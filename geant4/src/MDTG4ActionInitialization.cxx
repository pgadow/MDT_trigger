#include "MDTG4ActionInitialization.hh"
#include "MDTG4PrimaryGeneratorAction.hh"

#include "B1RunAction.hh"
#include "B1EventAction.hh"
#include "B1SteppingAction.hh"

MDTG4ActionInitialization::MDTG4ActionInitialization()
 : G4VUserActionInitialization()
{}

MDTG4ActionInitialization::~MDTG4ActionInitialization()
{}

void MDTG4ActionInitialization::BuildForMaster() const
{
  MDTG4RunAction* runAction = new MDTG4RunAction;
  SetUserAction(runAction);
}


void MDTG4ActionInitialization::Build() const
{
  SetUserAction(new MDTG4PrimaryGeneratorAction);
  MDTG4RunAction* runAction = new MDTG4RunAction;
  SetUserAction(runAction);

  MDTG4EventAction* eventAction = new MDTG4EventAction(runAction);
  SetUserAction(eventAction);

  SetUserAction(new MDTG4SteppingAction(eventAction));
}
