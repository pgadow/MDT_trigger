#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4GeometryManager.hh"
#include "G4SystemOfUnits.hh"


G4VPhysicalVolume* MDTG4DetectorConstruction::Construct(){
  // Define Materials
  DefineMaterials();

  // Define Volumes
  return DefineVolumes();
}

////////////////////////////////////////////////////////////////////////////////
// Materials                                                                  //
////////////////////////////////////////////////////////////////////////////////

void MDTG4DetectorConstruction::DefineMaterials(){
  G4String name, symbol;
  G4int ncomponents, natoms;
  G4double z, a, density, fractionmass, temperature, pressure;

// get geant material database
  G4NistManager* man = G4NistManager::Instance();

// Air //
  m_Air = man->FindOrBuildMaterial("G4_AIR");

// Tube Alumninum //
  density = 2.73*kg/dm3;
  a=26.98*g/mole;
  m_Al = new G4Material(name="Aluman100", z=13, a, density);

// Tube Wire //
  a = 183.84*g/mole;
  G4Element* eW  = new G4Element(name="Tungsten",symbol="H" , z= 74., a);

  a = 186.207*g/mole;
  G4Element* eRe  = new G4Element(name="Rhenium",symbol="Re" , z= 75., a);

  a = 196.967*g/mole;
  G4Element* eAu  = new G4Element(name="Gold",symbol="au" , z= 79., a);

  density = 19.34365*g/cm3;
  m_WireAlloy = new G4Material(name="WireAlloy",density,ncomponents=3);
  m_WireAlloy->AddElement(eW, fractionmass=94.09*perCent);
  m_WireAlloy->AddElement(eRe, fractionmass=2.91*perCent);
  m_WireAlloy->AddElement(eAu, fractionmass=3*perCent);

// Gas Mixture //
  a = 39.948*g/mole;
  G4Element* eAr  = new G4Element(name="Argon",symbol="Ar" , z= 18., a);
  G4Material* CO2  = man->FindOrBuildMaterial("G4_CARBON_DIOXIDE");

  density = 1.79586*g/cm3;
  temperature = STP_Temperature;
  pressure    = 3.*bar;
  m_GasMixture = new G4Material(name="GasMixture",density,ncomponents=2,
                                        kStateGas,temperature,pressure);
  m_GasMixture->AddElement(eAr, fractionmass=93*perCent);
  m_GasMixture->AddMaterial(CO2, fractionmass=7*perCent);

  // Print materials
  G4cout << *(G4Material::GetMaterialTable()) << G4endl;

  return;
}


G4VPhysicalVolume* MDTG4DetectorConstruction::DefineVolumes(){

////////////////////////////////////////////////////////////////////////////////
// Solids                                                                     //
////////////////////////////////////////////////////////////////////////////////

// World Volume //
  G4double world_hx = 4.0*m;
  G4double world_hy = 4.0*m;
  G4double world_hz = 4.0*m;

  G4Box* sdWorld
    = new G4Box("World", world_hx, world_hy, world_hz);


// MDT Tube //
  G4double tube_ri = 14.585*mm;
  G4double tube_ro = 14.985*mm;
  G4double tube_l  = 1186.5*mm;

  G4Tubs* sdMDTTube \
    = new G4Tubs("MDTTube",  // name
                  tube_ri,   // inner radius
                  tube_ro,   // outer radius
                  tube_l/2., // half length in z
                  0.*deg,    // starting phi angle in radians
                  360.*deg   // spanning phi angle in radians
                  );

// MDT Wire //
  G4double wire_ri = 0*um;
  G4double wire_ro = 50*um;
  G4double wire_l  = 1186.5*mm;

  G4Tubs* sdMDTWire \
    = new G4Tubs("MDTWire",  // name
                  wire_ri,   // inner radius
                  wire_ro,   // outer radius
                  wire_l/2., // half length in z
                  0.*deg,    // starting phi angle in radians
                  360.*deg   // spanning phi angle in radians
                  );

// MDT Gas //
G4Tubs* sdMDTGas \
    = new G4Tubs("MDTGas",  // name
                 wire_ro,   // inner radius
                 tube_ri,   // outer radius
                 tube_l/2., // half length in z
                 0.*deg,    // starting phi angle in radians
                 360.*deg   // spanning phi angle in radians
                 );


////////////////////////////////////////////////////////////////////////////////
// Logical Volumes                                                            //
////////////////////////////////////////////////////////////////////////////////

  m_lvWorld = new G4LogicalVolume(sdWorld, Air, "World");

  m_lvMDTTube = new G4LogicalVolume(sdMDTTube, Al, "MDTTube");

  m_lvMDTWire = new G4LogicalVolume(sdMDTWire, W, "MDTWire");

  m_lvMDTGas = new G4LogicalVolume(sdMDTGas, Ar, "MDTGas");


////////////////////////////////////////////////////////////////////////////////
// Physical Volumes                                                           //
////////////////////////////////////////////////////////////////////////////////

  // world volume //
  G4VPhysicalVolume* pvWorld
    = new G4PVPlacement(
                 0,               // no rotation
                 G4ThreeVector(), // at (0,0,0)
                 lvWorld,         // its logical volume
                 "World",         // its name
                 0,               // its mother  volume
                 false,           // no boolean operations
                 0,               // copy number
                 fCheckOverlaps); // checking overlaps


  G4int n_multilayers_per_chamber = 2;
  G4double spacer_height = 170*mm;

  G4int n_layers_per_multilayer = 3;
  G4double layer_spacing_in_multilayer = 25.95478*mm;

  G4int n_tubes_per_layer = 64;
  G4double tube_spacing_in_layer = 29.97*mm;
  G4double tube_radius = 14.985*mm;

  for (G4int i_ml = 0; i_ml < n_multilayers_per_chamber; i_ml++) {
    for (G4int i_ly = 0; i_ly < n_layers_per_multilayer; i_ly++) {
      for (G4int i_tb = 0; i_tb < n_tubes_per_layer; i_tb++) {
        G4int copy_nb = i_tb + i_ly * n_tubes_per_layer\
                      + i_ml*n_layers_per_multilayer*n_tubes_per_layer;
        G4RotationMatrix rotm  = G4RotationMatrix();
        rotm.rotateY(0);
        rotm.rotateZ(0);
        G4ThreeVector position(\
            0,
            -n_tubes_per_layer/2*tube_spacing_in_layer+i_tb*tube_spacing_in_layer\
            +(i_ly%2)*tube_spacing_in_layer/2. + tube_radius,
            -spacer_height/2+i_ml*spacer_height\
            -n_layers_per_multilayer*layer_spacing_in_multilayer\
            +i_ly*layer_spacing_in_multilayer + tube_radius);
        G4Transform3D transform = G4Transform3D(rotm,position);


        new G4PVPlacement(0,                 //rotation
                      G4ThreeVector(),        //position
                      m_lvMDTWire,           //its logical volume
                      "MDTWire",             //its name
                      m_lvMDTTube,           //its mother  volume
                      false,                 //no boolean operation
                      copy_nb+1000,               //copy number
                      fCheckOverlaps);       // checking overlaps

        new G4PVPlacement(transform,         //rotation,position
                      m_lvMDTGas,            //its logical volume
                      "MDTGas",              //its name
                      m_lvMDTTube,           //its mother  volume
                      false,                 //no boolean operation
                      copy_nb+2000,               //copy number
                      fCheckOverlaps);       // checking overlaps
        new G4PVPlacement(transform,         //rotation,position
                      m_lvMDTTube,           //its logical volume
                      "MDTTube",             //its name
                      m_lvWorld,             //its mother  volume
                      false,                 //no boolean operation
                      copy_nb,               //copy number
                      fCheckOverlaps);       // checking overlaps
      }
    }
  }
  return pvWorld;
}
