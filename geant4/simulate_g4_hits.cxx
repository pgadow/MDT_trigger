#ifdef G4VIS_USE
 #include "G4VisExecutive.hh"
#endif

#include "G4RunManager.hh"
#include "G4UImanager.hh"

#include "MDTG4DetectorConstruction.hh"
#include "MDTG4PhysicsList.hh"
#include "MDTG4ActionInitialization.hh"

int main()
{
 // construct the default run manager
 G4RunManager* runManager = new G4RunManager;

 // set mandatory initialization classes
 runManager->SetUserInitialization(new MDTG4DetectorConstruction);
 runManager->SetUserInitialization(new MDTG4PhysicsList);
 runManager->SetUserInitialization(new MDTG4ActionInitialization);

 // initialize G4 kernel
 runManager->Initialize();

 // get the pointer to the UI manager and set verbosities
 G4UImanager* UI = G4UImanager::GetUIpointer();
 UI->ApplyCommand("/run/verbose 1");
 UI->ApplyCommand("/event/verbose 1");
 UI->ApplyCommand("/tracking/verbose 1");

// Instantiation and initialization of the Visualization Manager
 #ifdef G4VIS_USE
   // visualization manager
   G4VisManager* visManager = new G4VisExecutive;
   // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
   // G4VisManager* visManager = new G4VisExecutive("Quiet");
   visManager->Initialize();
 #endif

 // start a run
 int numberOfEvent = 3;
 runManager->BeamOn(numberOfEvent);

 // job termination
    // Job termination
 #ifdef G4VIS_USE
   delete visManager;
 #endif
 delete runManager;
 return 0;
}
