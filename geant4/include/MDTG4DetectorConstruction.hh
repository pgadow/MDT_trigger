#ifndef MDTG4DetectorConstruction_h
#define MDTG4DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Material;

// Here the entire detector setup is described with
// - its geometry
// - the materials used in its construction
// - definition of its sensitive regions
// - readout schemes of the sensitive regions




/// Detector construction class to define materials and geometry.

class MDTG4DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    MDTG4DetectorConstruction();
    virtual ~MDTG4DetectorConstruction();

    virtual G4VPhysicalVolume* Construct();

  private:
    // methods //
    void DefineMaterials();
    G4VPhysicalVolume* DefineVolumes();

    // materials //
    G4Material* m_Air;
    G4Material* m_Al;
    G4Material* m_WireAlloy;
    G4Material* m_GasMixture;

    // logical volume //
      G4LogicalVolume* m_lvWorld;
      G4LogicalVolume* m_lvMDTTube;
      G4LogicalVolume* m_lvMDTWire;
      G4LogicalVolume* m_lvMDTGas;
};
#endif
