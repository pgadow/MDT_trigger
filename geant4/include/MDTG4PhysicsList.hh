// Here it is defined
// - which particles are to be used in the simulation
// - the physics processes to be simulated

class MDTG4PhysicsList : G4VuserPhysicsList (){
public:
  void ConstructParticle(); // construction of particles
  void ConstructProcess();  // construct processes and register to particles

private:

}
