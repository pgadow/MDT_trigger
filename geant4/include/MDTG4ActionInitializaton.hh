#ifndef MDTG4ActionInitialization_h
#define MDTG4ActionInitialization_h 1

#include "G4VUserActionInitialization.hh"


/// Action initialization class.
//  Here
// - user action classes that are invoked during the simulation are defined
// - including definition of primary particles

class MDTG4ActionInitialization : public G4VUserActionInitialization
{
  public:
    B1ActionInitialization();
    virtual ~B1ActionInitialization();

    virtual void BuildForMaster() const;
    virtual void Build() const;
};

#endif
