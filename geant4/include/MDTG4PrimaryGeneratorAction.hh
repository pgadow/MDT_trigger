#ifndef MDTG4PrimaryGeneratorAction_h
#define MDTG4PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

class G4ParticleGun;
class G4Event;

class MDTG4PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    MDTG4PrimaryGeneratorAction(
      const G4String& particleName = "mu-",
      G4double energy = 10.*GeV,
      G4ThreeVector position= G4ThreeVector(0,0,0),
      G4ThreeVector momentumDirection = G4ThreeVector(0,0,1));
    ~MDTG4PrimaryGeneratorAction();

    // methods
    virtual void GeneratePrimaries(G4Event*);

  private:
    // data members
    G4ParticleGun*  fParticleGun; //pointer a to G4 service class
};

#endif
