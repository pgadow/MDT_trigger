#!/bin/env python
# -*- utf-8 -*-
"""Small analysis for dependency of algorithms from seed precision."""

# Modules
import os
import subprocess
import numpy as np

# Set working directory
base_path = "../exe/"
os.chdir(base_path)

# Create folders for output, if they don't already exist
input_base = os.path.expanduser(
    "/mnt/scratch/pgadow/trigger/algorithm_study/")
output_base = os.path.expanduser(
    "/mnt/scratch/pgadow/trigger/algorithm_study/dependency_study/")

# Define Parameters
parameters = {'BIL': 'BIL1080.parameters', 'BIS': 'BIS1080.parameters',
              'BML': 'BML1440.parameters', 'BMS': 'BMS1440.parameters',
              'BOL': 'BOL2160.parameters', 'BOS': 'BOS2160.parameters',
              'EEL': 'EEL1.parameters',    'EES': 'EES1.parameters',
              'EIL': 'EIL1.parameters',    'EIS': 'EIS1.parameters',
              'EML': 'EML1.parameters',    'EMS': 'EMS.parameters',
              'EOL': 'EOL1_to_2.parameters', 'EOS': 'EOS1_to_3.parameters'}


ntracks = 10000
L0_resolution = np.arange(0.000, 0.030, 0.002)
time_resolution = 12.5

# Start analysis
for chamber in sorted(os.listdir('conf_sim')):
    chamber = chamber.replace('.simulation', '')
    if not (chamber == 'EML1' or chamber == 'EML1_double'):
        continue
    if not os.path.exists(output_base+chamber):
        os.makedirs(output_base+chamber)
    output_path = output_base+chamber+'/'
    input_name = input_base + "sim_{}_L0_{}_time_res_{}.root".format(
        chamber, 0.005, time_resolution)
    for alg in sorted(os.listdir('conf_alg')):
        if 'alg1' in alg:
            continue
        for res in L0_resolution:
            output_name = output_path + "sim_{}_L0_{}_time_res_{}_{}".format(
                chamber, res,
                time_resolution, alg.replace('.algorithm', ''))
            logname = output_name + ".log"
            output_name = output_name + '.root'

            log = open(logname, "w")
            command = "./run_data.exe {} {} {} {} \
{} conf_alg/{} conf_par/{}".format(
                output_name, input_name, ntracks, res,
                time_resolution, alg, parameters[chamber[0:3]])
            print command
            subprocess.call(command, stdout=log, shell=True)
            log.close()
    # command = \
    #    "python ../analysis_macros/find_optimal_parameters.py {}/".format(
    #        chamber)
    # subprocess.call(command, shell=True)
    # command = 'rm {}*'.format(output_path)
    # subprocess.call(command, shell=True)
