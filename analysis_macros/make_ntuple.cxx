#include "TTree.h"
#include "TFile.h"
void make_ntuple(){

  TFile* file = new TFile("root_results_BOS6.root", "RECREATE");
  TTree* tree = new TTree("tree", "tree");
  tree->ReadFile("results_BOS6.csv", "n:threshold:bin_wdt:nb_binned_algs:distance_binned_algs:b_width:m_width:search_width:muons:fakes:hits:miss:accepted_muons:rejected_muons:accepted_fakes:rejected_fakes:muons_p:fakes_p:hits_p:miss_p:accepted_muons_p:rejected_muons_p:accepted_fakes_p:rejected_fakes_p", ',');
  tree->Write();
  file->Close();
  return;
}
