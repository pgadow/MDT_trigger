#!/bin/env python
# -*- utf-8 -*-
"""Read result csv file and create histograms."""

# Modules
import csv
import os
import sys
from numpy import array
from collections import defaultdict
import ROOT
from ROOT import gROOT, gStyle, TH2D, TFile, TCanvas, TGraph, TLegend
import AtlasStyle
import AtlasUtils


# define class for deepening dictionaries
class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""

    def __getitem__(self, item):
        """implementation of get method."""
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value


# Set input directory from input
# Laptop
# input_base = os.path.expanduser(
#    "/Users/philippgadow1190/MDT_trigger/results/")
# MPI
input_base = os.path.expanduser(
    "/mnt/scratch/pgadow/trigger/algorithm_study/thesis_results/")
result_output = os.path.expanduser(
    "/mnt/scratch/pgadow/trigger/algorithm_study/")
if len(sys.argv) > 1:
    dataset = str(sys.argv[1])
os.chdir(input_base)
result_file = 'results_{}.csv'.format(dataset.strip('/'))
print "Working directory set to {}. Opening file {} ...".format(input_base,
                                                                result_file)
output_base = dataset
# Create CSV Reader for opening results
csvfile = open(result_file, 'rt')
reader = csv.DictReader(csvfile)

# Create ROOT Output file
output = TFile(result_output + "root_results_{}.root".format(dataset),
               "RECREATE")
# ntuple = TNtuple("ntuple", "ntuple", 'n:threshold:bin_wdt:nb_binned_algs:\
# distance_binned_algs:b_width:m_width:search_width:muons:fakes:hits:miss:\
# accepted_muons:rejected_muons:accepted_fakes:rejected_fakes:muons_p:fakes_p:\
# hits_p:miss_p:accepted_muons_p:rejected_muons_p:\
# accepted_fakes_p:rejected_fakes_p')

# Prepare ROOT
gROOT.Reset()
gStyle.SetOptStat('0')
# style = AtlasStyle.atlasStyle

# Containers for storing results
a0_bin_widths = defaultdict(list)
a0_efficiencies = defaultdict(list)
a0_muons = defaultdict(list)
a0_muon_acceptances = defaultdict(list)
a0_fake_acceptances = defaultdict(list)
a0_fake_rejections = defaultdict(list)

# structure : dict["nb_algs"]["dist_algs"]["threshold"]["bin_width"]
a1_efficiencies = AutoVivification()
a1_muons = AutoVivification()
a1_muon_acceptances = AutoVivification()
a1_fake_acceptances = AutoVivification()
a1_fake_rejections = AutoVivification()

# structure : dict["b_width"]["threshold"]["m_width"]["search_width"]
a2_efficiencies = AutoVivification()
a2_muons = AutoVivification()
a2_muon_acceptances = AutoVivification()
a2_fake_acceptances = AutoVivification()
a2_fake_rejections = AutoVivification()

# find optimal values
a0_maximal_eff3s = 0.0
a0_maximal_poor = 0.0
a0_maximal_eff3s_thresh = 0
a0_maximal_eff3s_binwd = 0.0

a1_maximal_eff3s = 0.0
a1_maximal_poor = 0.0
a1_maximal_eff3s_thresh = 0
a1_maximal_eff3s_binwd = 0.0
a1_maximal_eff3s_nb = 0.0
a1_maximal_eff3s_dist = 0.0

a2_maximal_eff3s = 0.0
a2_maximal_poor = 0.0
a2_maximal_eff3s_thresh = 0
a2_maximal_eff3s_b_width = 0.0
a2_maximal_eff3s_m_width = 0.0
a2_maximal_eff3s_search_width = 0.0

a3_maximal_eff3s = 0.0
a3_maximal_poor = 0.0
a3_maximal_eff3s_thresh = 0
a3_maximal_eff3s_b_width = 0.0
a3_maximal_eff3s_m_width = 0.0

# Fill Data Containers
for r in reader:
    # ntuple.Fill(int(r["n"]), int(r["threshold"]), float(r["bin_wdt"]),
    #             int(r["nb_binned_algs"]), float(r["distance_binned_algs"]),
    #             float(r["b_width"]), float(r["m_width"]),
    #             float(r["search_width"]),
    #             float(r["muons"]), float(r["fakes"]),
    #             float(r["hits"]), float(r["miss"]),
    #             float(r["accepted_muons"]), float(r["accepted_fakes"]),
    #             float(r["rejected_muons"]), float(r["rejected_fakes"]),
    #             float(r["muons_p"]), float(r["fakes_p"]),
    #             float(r["hits_p"]), float(r["miss_p"]),
    #             float(r["accepted_muons_p"]), float(r["accepted_fakes_p"]),
    #             float(r["rejected_muons_p"]), float(r["rejected_fakes_p"]))

    # Algorithm 0: Histogram-based pattern recognition
    if int(r["n"]) == 0:
        print "bin width: {}, threshold {}: \t efficiency: {}\
               3-sigma efficiency: {} poor tracks: {}".format(
              float(r["bin_wdt"]), int(r["threshold"]),
              float(r["accepted_muons_p"]) + float(r["accepted_fakes_p"]),
              float(r["accepted_muons_p"]), float(r["accepted_fakes_p"]))

        a0_bin_widths[r["threshold"]].append(float(r["bin_wdt"]))
        a0_efficiencies[r["threshold"]].append(float(r["hits_p"]))
        a0_muons[r["threshold"]].append(float(r["muons_p"]))
        a0_muon_acceptances[r["threshold"]].append(
            float(r["accepted_muons_p"]))
        a0_fake_acceptances[r["threshold"]].append(
            float(r["accepted_fakes_p"]))
        a0_fake_rejections[r["threshold"]].append(float(r["rejected_fakes_p"]))

        if float(r["accepted_muons_p"]) >= a0_maximal_eff3s and \
           int(r["threshold"]) > 2:
            a0_maximal_eff3s = float(r["accepted_muons_p"])
            a0_maximal_poor = float(r["accepted_fakes_p"])
            a0_maximal_eff3s_thresh = int(r["threshold"])
            a0_maximal_eff3s_binwd = float(r["bin_wdt"])

    # Algorithm 1: 2D Hough Transform
    # structure : dict["nb_algs"]["dist_algs"]["threshold"]["bin_width"]
    if int(r["n"]) == 1:
        nb_binned_algs = int(float(r["nb_binned_algs"]))

        print "bin width: {}, threshold {}, nb_algs {}, dist_algs {}:\
\t efficiency: {} 3-sigma efficiency: {} poor tracks: {}".format(
              float(r["bin_wdt"]), int(r["threshold"]), nb_binned_algs,
              float(r["distance_binned_algs"]),
              float(r["accepted_muons_p"]) + float(r["accepted_fakes_p"]),
              float(r["accepted_muons_p"]), float(r["accepted_fakes_p"]))

        a1_efficiencies[nb_binned_algs][float(r["distance_binned_algs"])][int(
            r["threshold"])][float(r["bin_wdt"])] = \
            float(r["hits_p"])
        a1_muons[nb_binned_algs][float(r["distance_binned_algs"])][
            int(r["threshold"])][float(r["bin_wdt"])] = float(r["muons_p"])
        a1_muon_acceptances[nb_binned_algs][float(
            r["distance_binned_algs"])][int(r["threshold"])][
            float(r["bin_wdt"])] = float(r["accepted_muons_p"])
        a1_fake_acceptances[nb_binned_algs][float(
            r["distance_binned_algs"])][int(r["threshold"])][
            float(r["bin_wdt"])] = float(r["accepted_fakes_p"])
        a1_fake_rejections[nb_binned_algs][float(
            r["distance_binned_algs"])][int(r["threshold"])][
            float(r["bin_wdt"])] = float(r["rejected_fakes_p"])

        if float(r["accepted_muons_p"]) >= a1_maximal_eff3s and \
           int(r["threshold"]) > 2:
            a1_maximal_eff3s = float(r["accepted_muons_p"])
            a1_maximal_poor = float(r["accepted_fakes_p"])
            a1_maximal_eff3s_thresh = int(r["threshold"])
            a1_maximal_eff3s_binwd = float(r["bin_wdt"])
            a1_maximal_eff3s_nb = nb_binned_algs
            a1_maximal_eff3s_dist = float(r["distance_binned_algs"])

    # Algorithm 2: Fast Track Finder
    if int(r["n"]) == 2:
        a2_efficiencies[float(r["b_width"])][int(r["threshold"])][float(
            r["m_width"])][float(r["search_width"])] = float(r["hits_p"])
        a2_muons[float(r["b_width"])][int(r["threshold"])][float(
            r["m_width"])][float(r["search_width"])] = float(r["muons_p"])
        a2_muon_acceptances[float(r["b_width"])][int(r["threshold"])][float(
            r["m_width"])][float(r["search_width"])] = \
            float(r["accepted_muons_p"])
        a2_fake_acceptances[float(r["b_width"])][int(r["threshold"])][float(
            r["m_width"])][float(r["search_width"])] = \
            float(r["accepted_fakes_p"])
        a2_fake_rejections[float(r["b_width"])][int(r["threshold"])][float(
            r["m_width"])][float(r["search_width"])] = \
            float(r["rejected_fakes_p"])
        if float(r["b_width"]) > 15.0:
            print "search width: {}, threshold {}: \t muon efficiency: {}\
                   fake efficiency: {}".format(
                   r["search_width"], r["threshold"],
                   r["accepted_muons_p"], r["accepted_fakes_p"])

            if float(r["accepted_muons_p"]) >= a2_maximal_eff3s and \
               int(r["threshold"]) > 2:
                a2_maximal_eff3s = float(r["accepted_muons_p"])
                a2_maximal_poor = float(r["accepted_fakes_p"])
                a2_maximal_eff3s_thresh = int(r["threshold"])
                a2_maximal_eff3s_b_width = float(r["b_width"])
                a2_maximal_eff3s_m_width = float(r["m_width"])
                a2_maximal_eff3s_search_width = float(r["search_width"])

    if int(r["n"]) == 3:
        if float(r["b_width"]) > 15.0:
            print "m width: {}, threshold {}: \t muon efficiency: {}\
                fake efficiency: {}".format(
                r["m_width"], r["threshold"],
                r["accepted_muons_p"], r["accepted_fakes_p"])

            if float(r["accepted_muons_p"]) >= a3_maximal_eff3s:
                a3_maximal_eff3s = float(r["accepted_muons_p"])
                a3_maximal_poor = float(r["accepted_fakes_p"])
                a3_maximal_eff3s_thresh = int(r["threshold"])
                a3_maximal_eff3s_b_width = float(r["b_width"])
                a3_maximal_eff3s_m_width = float(r["m_width"])

# Process Results
os.chdir(result_output + dataset)
##############################################################################
# Algorithm 0
##############################################################################

bin_widths = array(a0_bin_widths['6']).astype(float)

g_a0_label_3s = TGraph()
g_a0_label_3s.SetMarkerColor(1)
g_a0_label_3s.SetMarkerStyle(20)
g_a0_label_np = TGraph()
g_a0_label_np.SetMarkerColor(1)
g_a0_label_np.SetMarkerStyle(24)

g_a0_nbhits_3_efficiency = TGraph(bin_widths.size, bin_widths,
                                  array(a0_efficiencies['3']).astype(float))
g_a0_nbhits_4_efficiency = TGraph(bin_widths.size, bin_widths,
                                  array(a0_efficiencies['4']).astype(float))
g_a0_nbhits_5_efficiency = TGraph(bin_widths.size, bin_widths,
                                  array(a0_efficiencies['5']).astype(float))
g_a0_nbhits_6_efficiency = TGraph(bin_widths.size, bin_widths,
                                  array(a0_efficiencies['6']).astype(float))

g_a0_3s_efficiency = TGraph(bin_widths.size, bin_widths,
                            array(a0_muons['0']).astype(float))
g_a0_nbhits_3_3s_efficiency = TGraph(
    bin_widths.size, bin_widths, array(a0_muon_acceptances['3']).astype(float))
g_a0_nbhits_4_3s_efficiency = TGraph(
    bin_widths.size, bin_widths, array(a0_muon_acceptances['4']).astype(float))
g_a0_nbhits_5_3s_efficiency = TGraph(
    bin_widths.size, bin_widths, array(a0_muon_acceptances['5']).astype(float))
g_a0_nbhits_6_3s_efficiency = TGraph(
    bin_widths.size, bin_widths, array(a0_muon_acceptances['6']).astype(float))


g_a0_nbhits_3_np_efficiency = TGraph(
    bin_widths.size, bin_widths, array(a0_fake_acceptances['3']).astype(float))
g_a0_nbhits_4_np_efficiency = TGraph(
    bin_widths.size, bin_widths, array(a0_fake_acceptances['4']).astype(float))
g_a0_nbhits_5_np_efficiency = TGraph(
    bin_widths.size, bin_widths, array(a0_fake_acceptances['5']).astype(float))
g_a0_nbhits_6_np_efficiency = TGraph(
    bin_widths.size, bin_widths, array(a0_fake_acceptances['6']).astype(float))

##############################################################################
# alg0 Efficiency Plot
##############################################################################

c_a0_eff = TCanvas('c_a0_eff', 'Results: Histogram-based PR - Efficiency',
                   200, 10, 700, 500)
c_a0_eff.cd()
coord_a0_eff = TH2D("coord_a0_eff", ";bin width [mm];efficiency [%]",
                    2, 0.5, 5.5, 2, 0, 105)
coord_a0_eff.Draw()
g_a0_nbhits_6_efficiency.SetMarkerColor(2)
g_a0_nbhits_6_efficiency.SetMarkerStyle(20)
g_a0_nbhits_6_efficiency.Draw("P")
g_a0_nbhits_5_efficiency.SetMarkerColor(4)
g_a0_nbhits_5_efficiency.SetMarkerStyle(21)
g_a0_nbhits_5_efficiency.Draw("P")
g_a0_nbhits_4_efficiency.SetMarkerColor(3)
g_a0_nbhits_4_efficiency.SetMarkerStyle(22)
g_a0_nbhits_4_efficiency.Draw("P")
g_a0_nbhits_3_efficiency.SetMarkerColor(8)
g_a0_nbhits_3_efficiency.SetMarkerStyle(23)
g_a0_nbhits_3_efficiency.Draw("P")

leg_a0_eff = TLegend(0.6, 0.20, 0.85, 0.5)
leg_a0_eff.SetFillColor(ROOT.kWhite)
leg_a0_eff.SetLineColor(ROOT.kWhite)
leg_a0_eff.SetBorderSize(0)
# leg_a0_eff.SetHeader("reconstruction efficiency")
leg_a0_eff.AddEntry(g_a0_nbhits_3_efficiency, "threshold: 3 hits", "P")
leg_a0_eff.AddEntry(g_a0_nbhits_4_efficiency, "threshold: 4 hits", "P")
leg_a0_eff.AddEntry(g_a0_nbhits_5_efficiency, "threshold: 5 hits", "P")
leg_a0_eff.AddEntry(g_a0_nbhits_6_efficiency, "threshold: 6 hits", "P")
leg_a0_eff.Draw()

c_a0_eff.SaveAs(dataset.strip('/')+"_a0_efficiency.pdf")
c_a0_eff.SaveAs(dataset.strip('/')+"_a0_efficiency.C")

##############################################################################
# alg0 3 Sigma Efficiency Plot
##############################################################################

c_a0_3seff = TCanvas('c_a0_3seff', 'Results: HbPR - 3SigmaEfficiency',
                     200, 10, 700, 500)
c_a0_3seff.cd()
coord_a0_3seff = TH2D(
    "coord_a0_3seff", ";bin width [mm];3#sigma efficiency [%]",
    2, 0.5, 5.5, 2, 0, 105)
coord_a0_3seff.Draw()

g_a0_nbhits_6_3s_efficiency.SetMarkerColor(2)
g_a0_nbhits_6_3s_efficiency.SetMarkerStyle(20)
g_a0_nbhits_6_3s_efficiency.Draw("P")
g_a0_nbhits_5_3s_efficiency.SetMarkerColor(4)
g_a0_nbhits_5_3s_efficiency.SetMarkerStyle(21)
g_a0_nbhits_5_3s_efficiency.Draw("P")
g_a0_nbhits_4_3s_efficiency.SetMarkerColor(3)
g_a0_nbhits_4_3s_efficiency.SetMarkerStyle(22)
g_a0_nbhits_4_3s_efficiency.Draw("P")
g_a0_nbhits_3_3s_efficiency.SetMarkerColor(8)
g_a0_nbhits_3_3s_efficiency.SetMarkerStyle(23)
g_a0_nbhits_3_3s_efficiency.Draw("P")

leg_a0_3s = TLegend(0.51, 0.25, 0.85, 0.6)
leg_a0_3s.SetFillColor(ROOT.kWhite)
leg_a0_3s.SetLineColor(ROOT.kWhite)
leg_a0_3s.SetBorderSize(0)
# leg_a0_3s.AddEntry(g_a0_label_3s, "3#sigma-efficiency", "P")
leg_a0_3s.AddEntry(g_a0_nbhits_3_3s_efficiency, "threshold: 3 hits", "P")
leg_a0_3s.AddEntry(g_a0_nbhits_4_3s_efficiency, "threshold: 4 hits", "P")
leg_a0_3s.AddEntry(g_a0_nbhits_5_3s_efficiency, "threshold: 5 hits", "P")
leg_a0_3s.AddEntry(g_a0_nbhits_6_3s_efficiency, "threshold: 6 hits", "P")
leg_a0_3s.Draw()

c_a0_3seff.SaveAs(dataset.strip('/')+"_a0_3_sigma_efficiency.pdf")
c_a0_3seff.SaveAs(dataset.strip('/')+"_a0_3_sigma_efficiency.C")

##############################################################################
# alg0 n_poor Plot
##############################################################################

c_a0_npoor = TCanvas('c_a0_npoor', 'Results: HbPR - n poor',
                     200, 10, 700, 500)
c_a0_npoor.cd()
coord_a0_npoor = TH2D(
    "coord_a0_npoor", ";bin width [mm];n_{poor} [%]",
    2, 0.5, 5.5, 2, 0, 35)
coord_a0_npoor.Draw()

g_a0_nbhits_3_np_efficiency.SetMarkerColor(8)
g_a0_nbhits_3_np_efficiency.SetMarkerStyle(32)
g_a0_nbhits_3_np_efficiency.Draw("P")
g_a0_nbhits_4_np_efficiency.SetMarkerColor(3)
g_a0_nbhits_4_np_efficiency.SetMarkerStyle(26)
g_a0_nbhits_4_np_efficiency.Draw("P")
g_a0_nbhits_5_np_efficiency.SetMarkerColor(4)
g_a0_nbhits_5_np_efficiency.SetMarkerStyle(25)
g_a0_nbhits_5_np_efficiency.Draw("P")
g_a0_nbhits_6_np_efficiency.SetMarkerColor(2)
g_a0_nbhits_6_np_efficiency.SetMarkerStyle(24)
g_a0_nbhits_6_np_efficiency.Draw("P")


leg_a0_np = TLegend(0.51, 0.6, 0.85, 0.9)
leg_a0_np.SetFillColor(ROOT.kWhite)
leg_a0_np.SetLineColor(ROOT.kWhite)
leg_a0_np.SetBorderSize(0)
# leg_a0_np.AddEntry(g_a0_label_np, "n_{poor}", "P")
leg_a0_np.AddEntry(g_a0_nbhits_3_np_efficiency, "threshold: 3 hits", "P")
leg_a0_np.AddEntry(g_a0_nbhits_4_np_efficiency, "threshold: 4 hits", "P")
leg_a0_np.AddEntry(g_a0_nbhits_5_np_efficiency, "threshold: 5 hits", "P")
leg_a0_np.AddEntry(g_a0_nbhits_6_np_efficiency, "threshold: 6 hits", "P")
leg_a0_np.Draw()

c_a0_npoor.SaveAs(dataset.strip('/')+"_a0_n_poor_efficiency.pdf")
c_a0_npoor.SaveAs(dataset.strip('/')+"_a0_n_poor_efficiency.C")

##############################################################################
# Algorithm 1
##############################################################################

# structure : dict["nb_algs"]["dist_algs"]["threshold"]["bin_width"]

# a1_efficiencies = AutoVivification()
# a1_muons = AutoVivification()
# a1_muon_acceptances = AutoVivification()
# a1_fake_acceptances = AutoVivification()
# a1_fake_rejections = AutoVivification()

# obtain parameters to loop over
nb_algs_list = a1_efficiencies.keys()
interval_covered_list = a1_efficiencies[nb_algs_list[0]].keys()

bin_widths = array(a1_efficiencies[nb_algs_list[0]][
    interval_covered_list[0]][6.].keys()).astype(float)

g_a1_label_3s = TGraph()
g_a1_label_3s.SetMarkerColor(1)
g_a1_label_3s.SetMarkerStyle(20)
g_a1_label_np = TGraph()
g_a1_label_np.SetMarkerColor(1)
g_a1_label_np.SetMarkerStyle(24)


##############################################################################
# alg1 Efficiency Plots
##############################################################################

for sigma in interval_covered_list:

    g_a1_nbhits_3_nb_algs_3_efficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_efficiencies[2][sigma][3.0].values()).astype(float))
    g_a1_nbhits_5_nb_algs_3_efficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_efficiencies[2][sigma][5.0].values()).astype(float))
    g_a1_nbhits_3_nb_algs_7_efficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_efficiencies[6][sigma][3.0].values()).astype(float))
    g_a1_nbhits_5_nb_algs_7_efficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_efficiencies[6][sigma][5.0].values()).astype(float))
    g_a1_nbhits_3_nb_algs_11_efficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_efficiencies[10][sigma][3.0].values()).astype(float))
    g_a1_nbhits_5_nb_algs_11_efficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_efficiencies[10][sigma][5.].values()).astype(float))

    c_a1_eff = TCanvas('c_a1_eff', 'Results: 2D-Hough PR - Efficiency',
                       200, 10, 700, 500)
    c_a1_eff.cd()
    coord_a1_eff = TH2D("coord_a1_eff", ";bin width [mm];efficiency [%]",
                        2, 0.5, 5.5, 2, 0, 105)
    coord_a1_eff.Draw()
    g_a1_nbhits_3_nb_algs_3_efficiency.SetMarkerColor(2)
    g_a1_nbhits_3_nb_algs_3_efficiency.SetMarkerStyle(24)
    g_a1_nbhits_3_nb_algs_3_efficiency.Draw("P")
    g_a1_nbhits_5_nb_algs_3_efficiency.SetMarkerColor(2)
    g_a1_nbhits_5_nb_algs_3_efficiency.SetMarkerStyle(20)
    g_a1_nbhits_5_nb_algs_3_efficiency.Draw("P")
    g_a1_nbhits_3_nb_algs_7_efficiency.SetMarkerColor(4)
    g_a1_nbhits_3_nb_algs_7_efficiency.SetMarkerStyle(25)
    g_a1_nbhits_3_nb_algs_7_efficiency.Draw("P")
    g_a1_nbhits_5_nb_algs_7_efficiency.SetMarkerColor(4)
    g_a1_nbhits_5_nb_algs_7_efficiency.SetMarkerStyle(21)
    g_a1_nbhits_5_nb_algs_7_efficiency.Draw("P")
    g_a1_nbhits_3_nb_algs_11_efficiency.SetMarkerColor(3)
    g_a1_nbhits_3_nb_algs_11_efficiency.SetMarkerStyle(26)
    g_a1_nbhits_3_nb_algs_11_efficiency.Draw("P")
    g_a1_nbhits_5_nb_algs_11_efficiency.SetMarkerColor(3)
    g_a1_nbhits_5_nb_algs_11_efficiency.SetMarkerStyle(22)
    g_a1_nbhits_5_nb_algs_11_efficiency.Draw("P")

    leg_a1_eff = TLegend(0.5, 0.20, 0.85, 0.55)
    leg_a1_eff.SetFillColor(ROOT.kWhite)
    leg_a1_eff.SetLineColor(ROOT.kWhite)
    leg_a1_eff.SetBorderSize(0)
    leg_a1_eff.SetHeader("parameter space covered: [m_{{seed}} - {}#sigma_{{\
m_{{seed}} }}, m_{{seed}} + {}#sigma_{{m_{{seed}} }}]".format(sigma, sigma))
    leg_a1_eff.AddEntry(g_a1_nbhits_3_nb_algs_3_efficiency,
                        "threshold: 3 hits, 3 elementary algorithms", "P")
    leg_a1_eff.AddEntry(g_a1_nbhits_5_nb_algs_3_efficiency,
                        "threshold: 5 hits, 3 elementary algorithms", "P")
    leg_a1_eff.AddEntry(g_a1_nbhits_3_nb_algs_7_efficiency,
                        "threshold: 3 hits, 7 elementary algorithms", "P")
    leg_a1_eff.AddEntry(g_a1_nbhits_5_nb_algs_7_efficiency,
                        "threshold: 5 hits, 7 elementary algorithms", "P")
    leg_a1_eff.AddEntry(g_a1_nbhits_3_nb_algs_11_efficiency,
                        "threshold: 3 hits, 11 elementary algorithms", "P")
    leg_a1_eff.AddEntry(g_a1_nbhits_5_nb_algs_11_efficiency,
                        "threshold: 5 hits, 11 elementary algorithms", "P")
    leg_a1_eff.Draw()

    c_a1_eff.SaveAs(dataset.strip('/')+"_a1_{}_eff.pdf".format(int(sigma)))
    c_a1_eff.SaveAs(dataset.strip('/')+"_a1_{}_eff.C".format(int(sigma)))

##############################################################################
# alg1 3 Sigma Efficiency Plots
##############################################################################

for sig in interval_covered_list:

    g_a1_nbhits_3_nb_algs_3_3sefficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_muon_acceptances[2][sig][3.0].values()).astype(float))
    g_a1_nbhits_5_nb_algs_3_3sefficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_muon_acceptances[2][sig][5.0].values()).astype(float))
    g_a1_nbhits_3_nb_algs_7_3sefficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_muon_acceptances[6][sig][3.0].values()).astype(float))
    g_a1_nbhits_5_nb_algs_7_3sefficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_muon_acceptances[6][sig][5.0].values()).astype(float))
    g_a1_nbhits_3_nb_algs_11_3sefficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_muon_acceptances[10][sig][3.0].values()).astype(float))
    g_a1_nbhits_5_nb_algs_11_3sefficiency = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_muon_acceptances[10][sig][5.].values()).astype(float))

    c_a1_3seff = TCanvas('c_a1_3seff', 'Results: 2D-Hough PR - 3S Efficiency',
                         200, 10, 700, 500)
    c_a1_3seff.cd()
    coord_a1_3seff = TH2D("coord_a1_3seff", ";bin width [mm];\
3#sigma efficiency [%]", 2, 0.5, 5.5, 2, 0, 105)
    coord_a1_3seff.Draw()
    g_a1_nbhits_3_nb_algs_3_3sefficiency.SetMarkerColor(2)
    g_a1_nbhits_3_nb_algs_3_3sefficiency.SetMarkerStyle(24)
    g_a1_nbhits_3_nb_algs_3_3sefficiency.Draw("P")
    # g_a1_nbhits_5_nb_algs_3_3sefficiency.SetMarkerColor(2)
    # g_a1_nbhits_5_nb_algs_3_3sefficiency.SetMarkerStyle(20)
    # g_a1_nbhits_5_nb_algs_3_3sefficiency.Draw("P")
    g_a1_nbhits_3_nb_algs_7_3sefficiency.SetMarkerColor(4)
    g_a1_nbhits_3_nb_algs_7_3sefficiency.SetMarkerStyle(25)
    g_a1_nbhits_3_nb_algs_7_3sefficiency.Draw("P")
    g_a1_nbhits_5_nb_algs_7_3sefficiency.SetMarkerColor(4)
    g_a1_nbhits_5_nb_algs_7_3sefficiency.SetMarkerStyle(21)
    g_a1_nbhits_5_nb_algs_7_3sefficiency.Draw("P")
    g_a1_nbhits_3_nb_algs_11_3sefficiency.SetMarkerColor(3)
    g_a1_nbhits_3_nb_algs_11_3sefficiency.SetMarkerStyle(26)
    g_a1_nbhits_3_nb_algs_11_3sefficiency.Draw("P")
    g_a1_nbhits_5_nb_algs_11_3sefficiency.SetMarkerColor(3)
    g_a1_nbhits_5_nb_algs_11_3sefficiency.SetMarkerStyle(22)
    g_a1_nbhits_5_nb_algs_11_3sefficiency.Draw("P")

    leg_a1_3seff = TLegend(0.5, 0.20, 0.85, 0.55)
    leg_a1_3seff.SetFillColor(ROOT.kWhite)
    leg_a1_3seff.SetLineColor(ROOT.kWhite)
    leg_a1_3seff.SetBorderSize(0)
    leg_a1_3seff.SetHeader("parameter space covered: [m_{{seed}} - {}#sigma_{{\
m_{{seed}} }}, m_{{seed}} + {}#sigma_{{m_{{seed}} }}]".format(sig, sig))
    leg_a1_3seff.AddEntry(g_a1_nbhits_3_nb_algs_3_3sefficiency,
                          "threshold: 3 hits, 3 elementary algorithms", "P")
    # leg_a1_3seff.AddEntry(g_a1_nbhits_5_nb_algs_3_3sefficiency,
    #                       "threshold: 5 hits, 3 elementary algorithms", "P")
    leg_a1_3seff.AddEntry(g_a1_nbhits_3_nb_algs_7_3sefficiency,
                          "threshold: 3 hits, 7 elementary algorithms", "P")
    leg_a1_3seff.AddEntry(g_a1_nbhits_5_nb_algs_7_3sefficiency,
                          "threshold: 5 hits, 7 elementary algorithms", "P")
    leg_a1_3seff.AddEntry(g_a1_nbhits_3_nb_algs_11_3sefficiency,
                          "threshold: 3 hits, 11 elementary algorithms", "P")
    leg_a1_3seff.AddEntry(g_a1_nbhits_5_nb_algs_11_3sefficiency,
                          "threshold: 5 hits, 11 elementary algorithms", "P")
    leg_a1_3seff.Draw()

    c_a1_3seff.SaveAs(dataset.strip('/') +
                      "_a1_{}_3sigma_efficiency.pdf".format(int(sig)))
    c_a1_3seff.SaveAs(dataset.strip('/') +
                      "_a1_{}_3sigma_efficiency.C".format(int(sig)))


##############################################################################
# alg1  n_poor Plot
##############################################################################
for sig in interval_covered_list:

    g_a1_nbhits_3_nb_algs_3_npoor = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_fake_acceptances[2][sig][3.0].values()).astype(float))
    g_a1_nbhits_5_nb_algs_3_npoor = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_fake_acceptances[2][sig][5.0].values()).astype(float))
    g_a1_nbhits_3_nb_algs_7_npoor = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_fake_acceptances[6][sig][3.0].values()).astype(float))
    g_a1_nbhits_5_nb_algs_7_npoor = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_fake_acceptances[6][sig][5.0].values()).astype(float))
    g_a1_nbhits_3_nb_algs_11_npoor = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_fake_acceptances[10][sig][3.0].values()).astype(float))
    g_a1_nbhits_5_nb_algs_11_npoor = \
        TGraph(bin_widths.size, bin_widths,
               array(a1_fake_acceptances[10][sig][5.].values()).astype(float))

    c_a1_npoor = TCanvas('c_a1_npoor', 'Results: 2D-Hough PR - n poor',
                         200, 10, 700, 500)
    c_a1_npoor.cd()
    coord_a1_npoor = TH2D("coord_a1_npoor", ";bin width [mm]; n_{poor} [%]",
                          2, 0.5, 5.5, 2, 0, 105)
    coord_a1_npoor.Draw()
    g_a1_nbhits_3_nb_algs_3_npoor.SetMarkerColor(2)
    g_a1_nbhits_3_nb_algs_3_npoor.SetMarkerStyle(24)
    g_a1_nbhits_3_nb_algs_3_npoor.Draw("P")
    # g_a1_nbhits_5_nb_algs_3_npoor.SetMarkerColor(2)
    # g_a1_nbhits_5_nb_algs_3_npoor.SetMarkerStyle(20)
    # g_a1_nbhits_5_nb_algs_3_npoor.Draw("P")
    g_a1_nbhits_3_nb_algs_7_npoor.SetMarkerColor(4)
    g_a1_nbhits_3_nb_algs_7_npoor.SetMarkerStyle(25)
    g_a1_nbhits_3_nb_algs_7_npoor.Draw("P")
    g_a1_nbhits_5_nb_algs_7_npoor.SetMarkerColor(4)
    g_a1_nbhits_5_nb_algs_7_npoor.SetMarkerStyle(21)
    g_a1_nbhits_5_nb_algs_7_npoor.Draw("P")
    g_a1_nbhits_3_nb_algs_11_npoor.SetMarkerColor(3)
    g_a1_nbhits_3_nb_algs_11_npoor.SetMarkerStyle(26)
    g_a1_nbhits_3_nb_algs_11_npoor.Draw("P")
    g_a1_nbhits_5_nb_algs_11_npoor.SetMarkerColor(3)
    g_a1_nbhits_5_nb_algs_11_npoor.SetMarkerStyle(22)
    g_a1_nbhits_5_nb_algs_11_npoor.Draw("P")

    leg_a1_npoor = TLegend(0.5, 0.20, 0.85, 0.55)
    leg_a1_npoor.SetFillColor(ROOT.kWhite)
    leg_a1_npoor.SetLineColor(ROOT.kWhite)
    leg_a1_npoor.SetBorderSize(0)
    leg_a1_npoor.SetHeader("parameter space covered: [m_{{seed}} - {}#sigma_{{\
m_{{seed}} }}, m_{{seed}} + {}#sigma_{{m_{{seed}} }}]".format(sig, sig))
    leg_a1_npoor.AddEntry(g_a1_nbhits_3_nb_algs_3_npoor,
                          "threshold: 3 hits, 3 elementary algorithms", "P")
    # leg_a1_npoor.AddEntry(g_a1_nbhits_5_nb_algs_3_npoor,
    #                       "threshold: 5 hits, 3 elementary algorithms", "P")
    leg_a1_npoor.AddEntry(g_a1_nbhits_3_nb_algs_7_npoor,
                          "threshold: 3 hits, 7 elementary algorithms", "P")
    leg_a1_npoor.AddEntry(g_a1_nbhits_5_nb_algs_7_npoor,
                          "threshold: 5 hits, 7 elementary algorithms", "P")
    leg_a1_npoor.AddEntry(g_a1_nbhits_3_nb_algs_11_npoor,
                          "threshold: 3 hits, 11 elementary algorithms", "P")
    leg_a1_npoor.AddEntry(g_a1_nbhits_5_nb_algs_11_npoor,
                          "threshold: 5 hits, 11 elementary algorithms", "P")
    leg_a1_npoor.Draw()

    c_a1_npoor.SaveAs(dataset.strip('/') +
                      "_a1_{}_npoor_efficiency.pdf".format(int(sig)))
    c_a1_npoor.SaveAs(dataset.strip('/') +
                      "_a1_{}_npoor_efficiency.C".format(int(sig)))

##############################################################################
# Algorithm 2
##############################################################################
# structure : dict["b_width"]["threshold"]["m_width"]["search_width"]
m_widths = array(sorted(a2_efficiencies[100000.0][6].keys())).astype(float)
m_width_value = a2_maximal_eff3s_m_width

search_widths = array(
    a2_efficiencies[100000.0][6][m_width_value].keys()).astype(float)
search_width_value = a2_maximal_eff3s_search_width

g_a2_label_3s = TGraph()
g_a2_label_3s.SetMarkerColor(1)
g_a2_label_3s.SetMarkerStyle(20)
g_a2_label_np = TGraph()
g_a2_label_np.SetMarkerColor(1)
g_a2_label_np.SetMarkerStyle(24)

g_a2_nbhits_3_np_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_fake_acceptances[100000.0][3][m_width_value].values()).astype(
            float))
g_a2_nbhits_4_np_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_fake_acceptances[100000.0][4][m_width_value].values()).astype(
            float))
g_a2_nbhits_5_np_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_fake_acceptances[100000.0][5][m_width_value].values()).astype(
            float))
g_a2_nbhits_6_np_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_fake_acceptances[100000.0][6][m_width_value].values()).astype(
            float))

##############################################################################
# alg2 Efficiency Plot
##############################################################################

# efficiency vs search width
g_a2_nbhits_3_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_efficiencies[100000.0][3][m_width_value].values()).astype(float))
g_a2_nbhits_4_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_efficiencies[100000.0][4][m_width_value].values()).astype(float))
g_a2_nbhits_5_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_efficiencies[100000.0][5][m_width_value].values()).astype(float))
g_a2_nbhits_6_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_efficiencies[100000.0][6][m_width_value].values()).astype(float))


c_a2_eff = TCanvas('c_a2_eff', 'Results: Histogram-based PR - Efficiency',
                   200, 10, 700, 500)
c_a2_eff.cd()
coord_a2_eff = TH2D("coord_a2_eff", ";search width [mm];efficiency [%]",
                    2, 0.5, 5.5, 2, 0, 105)
coord_a2_eff.Draw()
g_a2_nbhits_6_efficiency.SetMarkerColor(2)
g_a2_nbhits_6_efficiency.SetMarkerStyle(20)
g_a2_nbhits_6_efficiency.Draw("P")
g_a2_nbhits_5_efficiency.SetMarkerColor(4)
g_a2_nbhits_5_efficiency.SetMarkerStyle(21)
g_a2_nbhits_5_efficiency.Draw("P")
g_a2_nbhits_4_efficiency.SetMarkerColor(3)
g_a2_nbhits_4_efficiency.SetMarkerStyle(22)
g_a2_nbhits_4_efficiency.Draw("P")
g_a2_nbhits_3_efficiency.SetMarkerColor(8)
g_a2_nbhits_3_efficiency.SetMarkerStyle(23)
g_a2_nbhits_3_efficiency.Draw("P")

leg_a2_eff = TLegend(0.55, 0.20, 0.85, 0.5)
leg_a2_eff.SetFillColor(ROOT.kWhite)
leg_a2_eff.SetLineColor(ROOT.kWhite)
leg_a2_eff.SetBorderSize(0)
# leg_a2_eff.SetHeader("reconstruction efficiency")
leg_a2_eff.AddEntry(g_a2_nbhits_3_efficiency, "threshold: 3 hits", "P")
leg_a2_eff.AddEntry(g_a2_nbhits_4_efficiency, "threshold: 4 hits", "P")
leg_a2_eff.AddEntry(g_a2_nbhits_5_efficiency, "threshold: 5 hits", "P")
leg_a2_eff.AddEntry(g_a2_nbhits_6_efficiency, "threshold: 6 hits", "P")
leg_a2_eff.Draw()

AtlasUtils.myText(0.22, 0.25, 1, 'm width: {}'.format(m_width_value))

c_a2_eff.SaveAs(dataset.strip('/')+"_a2_efficiency.pdf")
c_a2_eff.SaveAs(dataset.strip('/')+"_a2_efficiency.C")

# efficiency vs m width
efficiency_m_width_optimal_3 = []
efficiency_m_width_optimal_4 = []
efficiency_m_width_optimal_5 = []
efficiency_m_width_optimal_6 = []

for m_width in m_widths:
    efficiency_m_width_optimal_3.append(
        a2_efficiencies[100000.0][3][m_width][search_width_value])
    efficiency_m_width_optimal_4.append(
        a2_efficiencies[100000.0][4][m_width][search_width_value])
    efficiency_m_width_optimal_5.append(
        a2_efficiencies[100000.0][5][m_width][search_width_value])
    efficiency_m_width_optimal_6.append(
        a2_efficiencies[100000.0][6][m_width][search_width_value])

g_a2_nbhits_3_efficiency_m = TGraph(
    m_widths.size, m_widths, array(efficiency_m_width_optimal_3).astype(float))
g_a2_nbhits_4_efficiency_m = TGraph(
    m_widths.size, m_widths, array(efficiency_m_width_optimal_4).astype(float))
g_a2_nbhits_5_efficiency_m = TGraph(
    m_widths.size, m_widths, array(efficiency_m_width_optimal_5).astype(float))
g_a2_nbhits_6_efficiency_m = TGraph(
    m_widths.size, m_widths, array(efficiency_m_width_optimal_6).astype(float))

c_a2_eff_m = TCanvas('c_a2_eff_m', 'Results: Histogram-based PR - Efficiency',
                     200, 10, 700, 500)
c_a2_eff_m.cd()

if dataset[0] == 'E':
    m_hist_end = 0.03
else:
    m_hist_end = 1.5

coord_a2_eff_m = TH2D("coord_a2_eff_m", ";m width ;efficiency [%]",
                      2, 0.0, m_hist_end, 2, 0, 105)

coord_a2_eff_m.Draw()
g_a2_nbhits_6_efficiency_m.SetMarkerColor(2)
g_a2_nbhits_6_efficiency_m.SetMarkerStyle(20)
g_a2_nbhits_6_efficiency_m.Draw("P")
g_a2_nbhits_5_efficiency_m.SetMarkerColor(4)
g_a2_nbhits_5_efficiency_m.SetMarkerStyle(21)
g_a2_nbhits_5_efficiency_m.Draw("P")
g_a2_nbhits_4_efficiency_m.SetMarkerColor(3)
g_a2_nbhits_4_efficiency_m.SetMarkerStyle(22)
g_a2_nbhits_4_efficiency_m.Draw("P")
g_a2_nbhits_3_efficiency_m.SetMarkerColor(8)
g_a2_nbhits_3_efficiency_m.SetMarkerStyle(23)
g_a2_nbhits_3_efficiency_m.Draw("P")

leg_a2_eff_m = TLegend(0.55, 0.20, 0.85, 0.5)
leg_a2_eff_m.SetFillColor(ROOT.kWhite)
leg_a2_eff_m.SetLineColor(ROOT.kWhite)
leg_a2_eff_m.SetBorderSize(0)
# leg_a2_eff_m.SetHeader("reconstruction efficiency")
leg_a2_eff_m.AddEntry(g_a2_nbhits_3_efficiency_m, "threshold: 3 hits", "P")
leg_a2_eff_m.AddEntry(g_a2_nbhits_4_efficiency_m, "threshold: 4 hits", "P")
leg_a2_eff_m.AddEntry(g_a2_nbhits_5_efficiency_m, "threshold: 5 hits", "P")
leg_a2_eff_m.AddEntry(g_a2_nbhits_6_efficiency_m, "threshold: 6 hits", "P")
leg_a2_eff_m.Draw()

AtlasUtils.myText(0.22, 0.25, 1, 'search width: {} mm'.format(
    search_width_value))

c_a2_eff_m.SaveAs(dataset.strip('/')+"_a2_efficiency_m.pdf")
c_a2_eff_m.SaveAs(dataset.strip('/')+"_a2_efficiency_m.C")


##############################################################################
# alg2 3 Sigma Efficiency Plot
##############################################################################

# efficiency vs search width
g_a2_nbhits_3_3s_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_muon_acceptances[100000.0][3][m_width_value].values()).astype(
            float))
g_a2_nbhits_4_3s_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_muon_acceptances[100000.0][4][m_width_value].values()).astype(
            float))
g_a2_nbhits_5_3s_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_muon_acceptances[100000.0][5][m_width_value].values()).astype(
            float))
g_a2_nbhits_6_3s_efficiency = TGraph(
    search_widths.size, search_widths, array(
        a2_muon_acceptances[100000.0][6][m_width_value].values()).astype(
            float))


c_a2_3seff = TCanvas('c_a2_3seff', 'Results: HbPR - 3SigmaEfficiency',
                     200, 10, 700, 500)
c_a2_3seff.cd()
coord_a2_3seff = TH2D(
    "coord_a2_3seff", ";search width [mm];3#sigma efficiency [%]",
    2, 0.5, 5.5, 2, 0, 105)
coord_a2_3seff.Draw()

g_a2_nbhits_6_3s_efficiency.SetMarkerColor(2)
g_a2_nbhits_6_3s_efficiency.SetMarkerStyle(20)
g_a2_nbhits_6_3s_efficiency.Draw("P")
g_a2_nbhits_5_3s_efficiency.SetMarkerColor(4)
g_a2_nbhits_5_3s_efficiency.SetMarkerStyle(21)
g_a2_nbhits_5_3s_efficiency.Draw("P")
g_a2_nbhits_4_3s_efficiency.SetMarkerColor(3)
g_a2_nbhits_4_3s_efficiency.SetMarkerStyle(22)
g_a2_nbhits_4_3s_efficiency.Draw("P")
g_a2_nbhits_3_3s_efficiency.SetMarkerColor(8)
g_a2_nbhits_3_3s_efficiency.SetMarkerStyle(23)
g_a2_nbhits_3_3s_efficiency.Draw("P")

leg_a2_3s = TLegend(0.60, 0.22, 0.85, 0.5)
leg_a2_3s.SetFillColor(ROOT.kWhite)
leg_a2_3s.SetLineColor(ROOT.kWhite)
leg_a2_3s.SetBorderSize(0)
# leg_a2_3s.AddEntry(g_a2_label_3s, "3#sigma-efficiency", "P")
leg_a2_3s.AddEntry(g_a2_nbhits_3_3s_efficiency, "threshold: 3 hits", "P")
leg_a2_3s.AddEntry(g_a2_nbhits_4_3s_efficiency, "threshold: 4 hits", "P")
leg_a2_3s.AddEntry(g_a2_nbhits_5_3s_efficiency, "threshold: 5 hits", "P")
leg_a2_3s.AddEntry(g_a2_nbhits_6_3s_efficiency, "threshold: 6 hits", "P")
leg_a2_3s.Draw()

AtlasUtils.myText(0.22, 0.25, 1, 'm width: {}'.format(m_width_value))


c_a2_3seff.SaveAs(dataset.strip('/')+"_a2_3_sigma_efficiency.pdf")
c_a2_3seff.SaveAs(dataset.strip('/')+"_a2_3_sigma_efficiency.C")


# efficiency vs m width
efficiency3s_m_width_optimal_3 = []
efficiency3s_m_width_optimal_4 = []
efficiency3s_m_width_optimal_5 = []
efficiency3s_m_width_optimal_6 = []

for m_width in m_widths:
    efficiency3s_m_width_optimal_3.append(
        a2_muon_acceptances[100000.0][3][m_width][search_width_value])
    efficiency3s_m_width_optimal_4.append(
        a2_muon_acceptances[100000.0][4][m_width][search_width_value])
    efficiency3s_m_width_optimal_5.append(
        a2_muon_acceptances[100000.0][5][m_width][search_width_value])
    efficiency3s_m_width_optimal_6.append(
        a2_muon_acceptances[100000.0][6][m_width][search_width_value])

g_a2_nbhits_3_efficiency3s_m = TGraph(
    m_widths.size, m_widths, array(
        efficiency3s_m_width_optimal_3).astype(float))
g_a2_nbhits_4_efficiency3s_m = TGraph(
    m_widths.size, m_widths, array(
        efficiency3s_m_width_optimal_4).astype(float))
g_a2_nbhits_5_efficiency3s_m = TGraph(
    m_widths.size, m_widths, array(
        efficiency3s_m_width_optimal_5).astype(float))
g_a2_nbhits_6_efficiency3s_m = TGraph(
    m_widths.size, m_widths, array(
        efficiency3s_m_width_optimal_6).astype(float))

c_a2_3seff_m = TCanvas('c_a2_3seff_m', 'Results: HbPR - 3SigmaEfficiency',
                       200, 10, 700, 500)
c_a2_3seff_m.cd()
coord_a2_3seff_m = TH2D(
    "coord_a2_3seff_m", ";m width;3#sigma efficiency [%]",
    2, 0.0, m_hist_end, 2, 0, 105)
coord_a2_3seff_m.Draw()

g_a2_nbhits_6_efficiency3s_m.SetMarkerColor(2)
g_a2_nbhits_6_efficiency3s_m.SetMarkerStyle(20)
g_a2_nbhits_6_efficiency3s_m.Draw("P")
g_a2_nbhits_5_efficiency3s_m.SetMarkerColor(4)
g_a2_nbhits_5_efficiency3s_m.SetMarkerStyle(21)
g_a2_nbhits_5_efficiency3s_m.Draw("P")
g_a2_nbhits_4_efficiency3s_m.SetMarkerColor(3)
g_a2_nbhits_4_efficiency3s_m.SetMarkerStyle(22)
g_a2_nbhits_4_efficiency3s_m.Draw("P")
g_a2_nbhits_3_efficiency3s_m.SetMarkerColor(8)
g_a2_nbhits_3_efficiency3s_m.SetMarkerStyle(23)
g_a2_nbhits_3_efficiency3s_m.Draw("P")

leg_a2_3s_m = TLegend(0.60, 0.22, 0.85, 0.5)
leg_a2_3s_m.SetFillColor(ROOT.kWhite)
leg_a2_3s_m.SetLineColor(ROOT.kWhite)
leg_a2_3s_m.SetBorderSize(0)
# leg_a2_3s_m.AddEntry(g_a2_label_3s, "3#sigma-efficiency", "P")
leg_a2_3s_m.AddEntry(g_a2_nbhits_3_efficiency3s_m, "threshold: 3 hits", "P")
leg_a2_3s_m.AddEntry(g_a2_nbhits_4_efficiency3s_m, "threshold: 4 hits", "P")
leg_a2_3s_m.AddEntry(g_a2_nbhits_5_efficiency3s_m, "threshold: 5 hits", "P")
leg_a2_3s_m.AddEntry(g_a2_nbhits_6_efficiency3s_m, "threshold: 6 hits", "P")
leg_a2_3s_m.Draw()

AtlasUtils.myText(0.22, 0.25, 1, 'search width: {} mm'.format(
    search_width_value))

c_a2_3seff_m.SaveAs(dataset.strip('/')+"_a2_3_sigma_efficiency_m.pdf")
c_a2_3seff_m.SaveAs(dataset.strip('/')+"_a2_3_sigma_efficiency_m.C")

##############################################################################
# alg2 npoor plot
##############################################################################

# efficiency vs search width
g_a2_nbhits_3_npoor = TGraph(
    search_widths.size, search_widths, array(
        a2_fake_acceptances[100000.0][3][m_width_value].values()).astype(
            float))
g_a2_nbhits_4_npoor = TGraph(
    search_widths.size, search_widths, array(
        a2_fake_acceptances[100000.0][4][m_width_value].values()).astype(
            float))
g_a2_nbhits_5_npoor = TGraph(
    search_widths.size, search_widths, array(
        a2_fake_acceptances[100000.0][5][m_width_value].values()).astype(
            float))
g_a2_nbhits_6_npoor = TGraph(
    search_widths.size, search_widths, array(
        a2_fake_acceptances[100000.0][6][m_width_value].values()).astype(
            float))


c_a2_npoor = TCanvas('c_a2_npoor', 'Results: HbPR - npoor',
                     200, 10, 700, 500)
c_a2_npoor.cd()
coord_a2_npoor = TH2D(
    "coord_a2_npoor", ";search width [mm];n_{poor} [%]",
    2, 0.5, 5.5, 2, 0, 35)
coord_a2_npoor.Draw()

g_a2_nbhits_6_npoor.SetMarkerColor(2)
g_a2_nbhits_6_npoor.SetMarkerStyle(20)
g_a2_nbhits_6_npoor.Draw("P")
g_a2_nbhits_5_npoor.SetMarkerColor(4)
g_a2_nbhits_5_npoor.SetMarkerStyle(21)
g_a2_nbhits_5_npoor.Draw("P")
g_a2_nbhits_4_npoor.SetMarkerColor(3)
g_a2_nbhits_4_npoor.SetMarkerStyle(22)
g_a2_nbhits_4_npoor.Draw("P")
g_a2_nbhits_3_npoor.SetMarkerColor(8)
g_a2_nbhits_3_npoor.SetMarkerStyle(23)
g_a2_nbhits_3_npoor.Draw("P")

leg_a2_npoor = TLegend(0.51, 0.6, 0.85, 0.9)
leg_a2_npoor.SetFillColor(ROOT.kWhite)
leg_a2_npoor.SetLineColor(ROOT.kWhite)
leg_a2_npoor.SetBorderSize(0)
# leg_a2_npoor.AddEntry(g_a2_label_npoor, "3#sigma-efficiency", "P")
leg_a2_npoor.AddEntry(g_a2_nbhits_3_npoor, "threshold: 3 hits", "P")
leg_a2_npoor.AddEntry(g_a2_nbhits_4_npoor, "threshold: 4 hits", "P")
leg_a2_npoor.AddEntry(g_a2_nbhits_5_npoor, "threshold: 5 hits", "P")
leg_a2_npoor.AddEntry(g_a2_nbhits_6_npoor, "threshold: 6 hits", "P")
leg_a2_npoor.Draw()

AtlasUtils.myText(0.22, 0.75, 1, 'm width: {}'.format(m_width_value))


c_a2_npoor.SaveAs(dataset.strip('/')+"_a2_n_poor_efficiency.pdf")
c_a2_npoor.SaveAs(dataset.strip('/')+"_a2_n_poor_efficiency.C")


# efficiency vs m width
efficiencynp_m_width_optimal_3 = []
efficiencynp_m_width_optimal_4 = []
efficiencynp_m_width_optimal_5 = []
efficiencynp_m_width_optimal_6 = []

for m_width in m_widths:
    efficiencynp_m_width_optimal_3.append(
        a2_fake_acceptances[100000.0][3][m_width][search_width_value])
    efficiencynp_m_width_optimal_4.append(
        a2_fake_acceptances[100000.0][4][m_width][search_width_value])
    efficiencynp_m_width_optimal_5.append(
        a2_fake_acceptances[100000.0][5][m_width][search_width_value])
    efficiencynp_m_width_optimal_6.append(
        a2_fake_acceptances[100000.0][6][m_width][search_width_value])

g_a2_nbhits_3_efficiencynp_m = TGraph(
    m_widths.size, m_widths, array(
        efficiencynp_m_width_optimal_3).astype(float))
g_a2_nbhits_4_efficiencynp_m = TGraph(
    m_widths.size, m_widths, array(
        efficiencynp_m_width_optimal_4).astype(float))
g_a2_nbhits_5_efficiencynp_m = TGraph(
    m_widths.size, m_widths, array(
        efficiencynp_m_width_optimal_5).astype(float))
g_a2_nbhits_6_efficiencynp_m = TGraph(
    m_widths.size, m_widths, array(
        efficiencynp_m_width_optimal_6).astype(float))

c_a2_npeff_m = TCanvas('c_a2_npeff_m', 'Results: HbPR - 3SigmaEfficiency',
                       200, 10, 700, 500)
c_a2_npeff_m.cd()
coord_a2_npeff_m = TH2D(
    "coord_a2_npeff_m", ";m width;3#sigma efficiency [%]",
    2, 0.0, m_hist_end, 2, 0, 35)
coord_a2_npeff_m.Draw()

g_a2_nbhits_6_efficiencynp_m.SetMarkerColor(2)
g_a2_nbhits_6_efficiencynp_m.SetMarkerStyle(20)
g_a2_nbhits_6_efficiencynp_m.Draw("P")
g_a2_nbhits_5_efficiencynp_m.SetMarkerColor(4)
g_a2_nbhits_5_efficiencynp_m.SetMarkerStyle(21)
g_a2_nbhits_5_efficiencynp_m.Draw("P")
g_a2_nbhits_4_efficiencynp_m.SetMarkerColor(3)
g_a2_nbhits_4_efficiencynp_m.SetMarkerStyle(22)
g_a2_nbhits_4_efficiencynp_m.Draw("P")
g_a2_nbhits_3_efficiencynp_m.SetMarkerColor(8)
g_a2_nbhits_3_efficiencynp_m.SetMarkerStyle(23)
g_a2_nbhits_3_efficiencynp_m.Draw("P")

leg_a2_np_m = TLegend(0.51, 0.6, 0.85, 0.9)
leg_a2_np_m.SetFillColor(ROOT.kWhite)
leg_a2_np_m.SetLineColor(ROOT.kWhite)
leg_a2_np_m.SetBorderSize(0)
# leg_a2_np_m.AddEntry(g_a2_label_np, "3#sigma-efficiency", "P")
leg_a2_np_m.AddEntry(g_a2_nbhits_3_efficiencynp_m, "threshold: 3 hits", "P")
leg_a2_np_m.AddEntry(g_a2_nbhits_4_efficiencynp_m, "threshold: 4 hits", "P")
leg_a2_np_m.AddEntry(g_a2_nbhits_5_efficiencynp_m, "threshold: 5 hits", "P")
leg_a2_np_m.AddEntry(g_a2_nbhits_6_efficiencynp_m, "threshold: 6 hits", "P")
leg_a2_np_m.Draw()

AtlasUtils.myText(0.22, 0.75, 1, 'search width: {} mm'.format(
    search_width_value))

c_a2_npeff_m.SaveAs(dataset.strip('/')+"_a2_n_poor_efficiency_m.pdf")
c_a2_npeff_m.SaveAs(dataset.strip('/')+"_a2_n_poor_efficiency_m.C")


##############################################################################
# Save Output and Print Optimal Values
##############################################################################

output.Write()
output.Close()

# Print optimal values
print "Optimal values algorithm 0"
print a0_maximal_eff3s, a0_maximal_poor, a0_maximal_eff3s_thresh, \
    a0_maximal_eff3s_binwd

print "Optimal values algorithm 1"
print a1_maximal_eff3s, a1_maximal_poor, a1_maximal_eff3s_thresh, \
    a1_maximal_eff3s_binwd, a1_maximal_eff3s_nb, a1_maximal_eff3s_dist

print "Optimal values algorithm 2"
print a2_maximal_eff3s, a2_maximal_poor, a2_maximal_eff3s_thresh, \
    a2_maximal_eff3s_b_width, a2_maximal_eff3s_m_width, \
    a2_maximal_eff3s_search_width

print "Optimal values algorithm 3"
print a3_maximal_eff3s, a3_maximal_poor, a3_maximal_eff3s_thresh, \
    a3_maximal_eff3s_b_width, a3_maximal_eff3s_m_width
