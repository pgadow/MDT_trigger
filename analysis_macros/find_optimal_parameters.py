#!/bin/env python
# -*- utf-8 -*-
"""Loop through root files to find optimum parameters."""

# Modules
import os
import sys
import csv
import math
from collections import namedtuple
from ROOT import gROOT, gDirectory, TFile

# Set input directory from input
input_base = os.path.expanduser(
    "/mnt/scratch/pgadow/trigger/algorithm_study/")
if len(sys.argv) > 1:
    dataset = str(sys.argv[1])
    input_base += dataset
print "input path: {}".format(input_base)
os.chdir(input_base)

output_base = os.path.expanduser(
    "/mnt/scratch/pgadow/trigger/algorithm_study/")

# Create CSV Writer for storing results
csvfile_name = output_base + 'results_{}.csv'.format(dataset.strip('/'))
csvfile = open(csvfile_name, 'wb')
fieldnames = ['n', 'threshold', 'bin_wdt', 'nb_binned_algs',
              'distance_binned_algs', 'b_width', 'm_width', 'search_width',
              'muons', 'fakes', 'hits', 'miss', 'accepted_muons',
              'rejected_muons', 'accepted_fakes',
              'rejected_fakes', 'muons_p', 'fakes_p',
              'hits_p', 'miss_p', 'accepted_muons_p',
              'rejected_muons_p', 'accepted_fakes_p',
              'rejected_fakes_p']
writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
writer.writeheader()

# Create container to store the results
result = namedtuple('result', 'muons, fakes, hits, miss, accepted_muons,\
                               rejected_muons, accepted_fakes, rejected_fakes,\
                               muons_p, fakes_p, hits_p, miss_p,\
                               accepted_muons_p, rejected_muons_p,\
                               accepted_fakes_p, rejected_fakes_p')

a0_parameter = namedtuple('a0_parameter', 'threshold, bin_width')
a1_parameter = namedtuple('a1_parameter', 'threshold, bin_width, \
                           nb_algorithms, algorithm_distance')
a2_parameter = namedtuple('a2_parameter', 'threshold, \
                           m_width, b_width, s_width')
a3_parameter = namedtuple('a3_parameter', 'threshold, m_width, b_width')

result_container = dict()

# Set number of required hits as threshold
tresholds = [0, 1, 2, 3, 4, 5, 6]

# define most common angles for each chamber
theta = {'BOL1': 0.17, 'BOL2': 0.36, 'BOL3': 0.52, 'BOL4': 0.67,
         'BOL5': 0.79, 'BOL6': 0.87,
         'BOS1': 0.13, 'BOS2': 0.31, 'BOS3': 0.50, 'BOS4': 0.63,
         'BOS5': 0.76, 'BOS6': 0.84,
         'BML1': 0.17, 'BML2': 0.37, 'BML3': 0.55, 'BML4': 0.70,
         'BML5': 0.80, 'BML6': 0.86,
         'BMS1': 0.13, 'BMS2': 0.31, 'BMS3': 0.51, 'BMS4': 0.63,
         'BMS5': 0.75, 'BMS6': 0.82,
         'BIL1': 0.17, 'BIL2': 0.36, 'BIL3': 0.52, 'BIL4': 0.68,
         'BIL5': 0.80, 'BIL6': 0.86,
         'BIS1': 0.13, 'BIS2': 0.31, 'BIS3': 0.51, 'BIS4': 0.64,
         'BIS5': 0.78, 'BIS6': 0.83, 'BIS7': 1.0, 'BIS8': 1.0,
         'EML1': 0.20, 'EML2': 0.31, 'EML3': 0.42,
         'EML4': 0.50, 'EML5': 0.63,
         'EMS1': 0.22, 'EMS2': 0.33, 'EMS3': 0.45,
         'EMS4': 0.49, 'EMS5': 0.62,
         'EOL1': 0.19, 'EOL2': 0.24, 'EOL3': 0.31,
         'EOL4': 0.38, 'EOL5': 0.43, 'EOL6': 0.49,
         'EOS1': 0.18, 'EOS2': 0.24, 'EOS3': 0.31,
         'EOS4': 0.37, 'EOS5': 0.43, 'EOS6': 0.48,
         'EEL1': 1.00, 'EEL2': 1.00, 'EES1': 1.00, 'EES2': 1.00
         }

# Set MDT resolution
s_MDT = 0.001


# Loop over all files satisfying the selection
for filename in sorted(os.listdir(input_base)):
    if(filename[-4:] != "root" or filename[:3] != "sim"):
        continue
    gROOT.Reset()
    print filename
    try:
        f = TFile(filename)
    except:
        continue
    tree = gDirectory.Get('tree')

    n = int(filename[filename.find('alg')+3:filename.find('alg')+4])
    an = "a{}".format(n)
    t = theta[dataset[:4]]
    s = s_MDT * (1. + math.tan(t) * math.tan(t))

    bin_wdt = 0.
    nb_binned_algs = 0
    distance_binned_algs = 0.
    b_width = 0.
    m_width = 0.
    search_width = 0.

    if '_binwd_' in filename:
        bin_wdt = \
            float(filename[filename.find('_binwd_')+7:
                  filename.find('_binwd_')+10])
    if '_nb_' in filename:
        nb_binned_algs = \
            float(filename[filename.find('_nb_')+4:filename.find('_d_')])
    if '_d_' in filename:
        distance_binned_algs = \
            float(filename[filename.find('_d_')+3:filename.find('.root')])
    if '_bw_' in filename:
        b_width = float(filename[filename.find('_bw_')+4:
                        filename.find('_mw_')])
    if '_mw_' in filename:
        m_width = float(filename[filename.find('_mw_')+4:
                        filename.find('_sw_')])
    if '_sw_' in filename:
        search_width = \
            float(filename[filename.find('_sw_')+4:
                           filename.find('_sw_')+7])
    for threshold in tresholds:
        muons = 0
        fakes = 0
        hits = 0
        miss = 0

        accepted_muons = 0
        rejected_muons = 0
        accepted_fakes = 0
        rejected_fakes = 0

        muon = False
        hit = False

        for event in tree:
            for candidate in xrange(
                            getattr(tree, "{}_nb_candidates".format(an))):
                if(abs(tree.m_gen -
                       getattr(tree, "{}_m_rec".format(an))[candidate]) < 3*s):
                    muons += 1
                    muon = True
                else:
                    fakes += 1
                    muon = False
                if(getattr(tree, "{}_nb_hits".format(an))[candidate] <
                        threshold):
                    miss += 1
                    hit = False
                else:
                    hits += 1
                    hit = True

                if(muon and hit):
                    accepted_muons += 1
                elif(muon and not hit):
                    rejected_muons += 1
                elif(not muon and hit):
                    accepted_fakes += 1
                elif(not muon and not hit):
                    rejected_fakes += 1

        muons_p = muons / float(muons + fakes) * 100
        fakes_p = fakes / float(muons + fakes) * 100
        hits_p = hits / float(hits + miss) * 100
        miss_p = miss / float(hits + miss) * 100

        accepted_muons_p = accepted_muons / float(muons + fakes) * 100
        rejected_muons_p = rejected_muons / float(muons + fakes) * 100
        accepted_fakes_p = accepted_fakes / float(muons + fakes) * 100
        rejected_fakes_p = rejected_fakes / float(muons + fakes) * 100

        print "Algorithm: {}".format(n)
        print "Threshold: {} hits".format(threshold)
        print "Bin Width: {} mm".format(bin_wdt)
        print "Number of algorithms: {}".format(nb_binned_algs)
        print "Distance between algorithms: {}".format(
            distance_binned_algs)
        print "b search width: {}".format(b_width)
        print "m search width: {} mm".format(m_width)
        print "search width: {} mm".format(search_width)
        print "Muons: {0} ({1:2.2f}%), Fakes: {2} ({3:2.2f}%)".format(
            muons, muons_p, fakes, fakes_p)
        print "Hits: {0} ({1:2.2f}%), Rejected: {2} ({3:2.2f}%)".format(
            hits, hits_p, miss, miss_p)
        print "Accepted Muons: {0} ({1:2.2f}%), \
        Rejected Muons: {2} ({3:2.2f}%)".format(
            accepted_muons, accepted_muons_p, rejected_muons, rejected_muons_p)
        print "Accepted Fakes: {0} ({1:2.2f}%), \
        Rejected Fakes: {2} ({3:2.2f}%)".format(
            accepted_fakes, accepted_fakes_p, rejected_fakes, rejected_fakes_p)
        print "\n"

        writer.writerow({'n': n, 'threshold': threshold, 'bin_wdt': bin_wdt,
                         'nb_binned_algs': nb_binned_algs,
                         'distance_binned_algs': distance_binned_algs,
                         'b_width': b_width, 'm_width': m_width,
                         'search_width': search_width,
                         'muons': muons, 'fakes': fakes, 'hits': hits,
                         'miss': miss, 'accepted_muons': accepted_muons,
                         'rejected_muons': rejected_muons,
                         'accepted_fakes': accepted_fakes,
                         'rejected_fakes': rejected_fakes,
                         'muons_p': muons_p, 'fakes_p': fakes_p,
                         'hits_p': hits_p, 'miss_p': miss_p,
                         'accepted_muons_p': accepted_muons_p,
                         'rejected_muons_p': rejected_muons_p,
                         'accepted_fakes_p': accepted_fakes_p,
                         'rejected_fakes_p': rejected_fakes_p
                         })

        par = a0_parameter(threshold, bin_wdt)
        res = result(muons, fakes, hits, miss, accepted_muons, rejected_muons,
                     accepted_fakes, rejected_fakes, muons_p, fakes_p, hits_p,
                     miss_p, accepted_muons_p, rejected_muons_p,
                     accepted_fakes_p, rejected_fakes_p)
        result_container[par] = res

csvfile.close()
