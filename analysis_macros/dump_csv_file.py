"""dump data in text file."""
import ROOT
import sys
import csv

# get r-t relation
ROOT.gSystem.Load('Rt_relation_cxx')
from ROOT import Rt_relation


# open ROOT file
if len(sys.argv) > 1:
    filename = str(sys.argv[1])
    output = str(sys.argv[2])

ROOT.gROOT.Reset()

print filename
f = ROOT.TFile(filename)
tree = ROOT.gDirectory.Get('FRO_analysis_tree')
tree.Print()

rt = Rt_relation()
rt.read_rt('../share/default.rt')

with open(output, 'w') as csvfile:
    fields = ['event', 'b_reference', 'm_reference', 'wp_y', 'wp_z', 'r', 't']
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow(fields)
    event_counter = 0
    for event in tree:
        event_counter += 1
        if tree.std_has_track == 0:
            continue
        b_reference = tree.std_b
        m_reference = tree.std_m

        for i in xrange(tree.FRO_wy.size()):
            y = tree.FRO_wy[i]
            z = tree.FRO_wz[i]
            t = tree.FRO_t[i]

            if tree.FRO_r[0] == 0:
                r = rt.r(t, 0)
            else:
                r = tree.FRO_r[i]
            data = [event_counter, b_reference, m_reference, y, z, r, t]
            writer.writerow(data)
