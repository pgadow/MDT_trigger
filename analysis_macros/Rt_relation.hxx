//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 31.03.1999, AUTHOR: OLIVER KORTNER
// Modified: 30.05.1999, removed dynamic memory management due to copy problems
//           02.03.2000, t(const double & r, const int & option) added.
//           24.04.2000, error calculation improved.
//           25.05.2001, correct interpolation in r(r, option) and
//                       t(r, option).
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

///////////////////////
// CLASS Rt_relation //
///////////////////////

///////////////////////////////////////////////////////////
// THIS CLASS CONTAINS THE r-t RELATION OF AN MDT TUBES. //
///////////////////////////////////////////////////////////

#ifndef RtrelationHXX
#define RtrelationHXX

//////////////////////////
// INCLUDE HEADER FILES //
//////////////////////////

// standard C++ libraries //
#include <iostream>
#include <fstream>
#include <cmath>

class Rt_relation {

private:
// units: [mm], [ns]
	int nb_pairs; // number of (r,t) pairs
	double r_drift[300], t_drift[300]; // pointer to the array of
					      // (r,t) pairs
	double r_err[300]; // pointer to the array of the errors of r
	void copy(const Rt_relation & rt); // copy routine

public:
// Constructors
	Rt_relation(void) {
		nb_pairs = 0;
		}				// default constructor
	Rt_relation(const Rt_relation & rt);
                        // copy constructor

// Methods
// get-methods //
	int number_of_pairs(void) const { return nb_pairs; }
						// get the number of (r,t) pairs
	double r(const int & k) const;
						// get r for the k-th pair,
						// 0 <= k < number of pairs
	double r(const double & t, const int & option) const;
						// get r(t),
						// t[0] <= t <= t[nb_pairs],
						// option = 1 means:
						// 	throw a warning if t is
						//	out of range.
						// option = 0 means:
						//	dont' throw a warning.
						// In both cases, edge values
						// are used in case of error.
	double t(const int & k) const;
						// get t for the k-th pair,
						// 0 <= k < number of pairs
	double t(const double & r, const int & option) const;
						// get t(r),
						// r[0] <= r <= r[nb_pairs],
						// option = 1 means:
						//      throw a warning if r is
						//	out of range.
						// option = 0 means:
						//	dont' throw a warning.
						// In both cases, edge values
						// are used in case of error.
	double error(const int & k) const;
						// get the error on r for the
						// k-th pair,
						// 0 <= k < number of pairs
	double error(const double & t, const int & option)const;
						// get error(r(t)),
						// t[0] <= t <= t[nb_pairs],
						// option = 1 means:
						// 	throw a warning if t is
						//	out of range.
						// option = 0 means:
						//	dont' throw a warning.
						// In both cases, edge values
						// are used in case of error.
	double t_error(const double & t,
						const int & option) const;
						// get error(t(r)),
						// t[0] <= t <= t[nb_pairs],
						// option = 1 means:
						// 	throw a warning if t is
						//	out of range.
						// option = 0 means:
						//	dont' throw a warning.
						// In both cases, edge values
						// are used in case of error.
	void write_out_rt(std::ofstream & rt_file) const;
						// write r-t relation to rt_file
// set-methods //
	void set_number_of_pairs(const int & n_pairs);
						// set the number of pairs to
						// n_pairs, a call to this
						// routine removes a stored
						// r-t relation,
						// n_pairs <= 300
	void set_r_t(const int & k, const double & r,
				const double & t, const double & error);
						// set the k-th (r,t) pair to
						// (r,t), set the error of r to
						// error,
						// 0 <= k < number of pairs
	void read_rt(std::string rt_file);
						// read r-t relation from file
						// rt_file

};

#endif
