// root macro to add branches with tube, layer and multilayer to testbeam data
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include <iostream>
#include <vector>

unsigned int getML(double z){
  // TANDEM chamber consists only of one multilayer
  return 0;
}
unsigned int getLY(double z){
  if        (fabs(z - (-1703.13)) < 2){
    return 0;
  } else if (fabs(z - (-1716.2 )) < 2){
    return 1;
  } else if (fabs(z - (-1729.28)) < 2){
    return 2;
  } else if (fabs(z - (-1742.36)) < 2){
    return 3;
  } else if (fabs(z - (-1755.43)) < 2){
    return 4;
  } else if (fabs(z - (-1768.51)) < 2){
    return 5;
  } else if (fabs(z - (-1781.59)) < 2){
    return 6;
  }

  return -1;

}
unsigned int getTB(double y, double z){
  if (getLY(z)%2 == 0) {
    if        (fabs(y - (349.8)) < 2){
      return 0;
    } else if (fabs(y - (364.9)) < 2){
      return 1;
    } else if (fabs(y - (380)) < 2){
      return 2;
    } else if (fabs(y - (395.1)) < 2){
      return 3;
    } else if (fabs(y - (410.2)) < 2){
      return 4;
    } else if (fabs(y - (425.3)) < 2){
      return 5;
    }
  } else {
    if        (fabs(y - (342.25)) < 2){
      return 0;
    } else if (fabs(y - (357.35)) < 2){
      return 1;
    } else if (fabs(y - (372.45)) < 2){
      return 2;
    } else if (fabs(y - (387.55)) < 2){
      return 3;
    } else if (fabs(y - (402.65)) < 2){
      return 4;
    } else if (fabs(y - (417.75)) < 2){
      return 5;
    }
  }

  return -1;
}

void testbeam_add_geometry_information(std::string file){
   TFile *f = new TFile(file.c_str(),"update");
   TTree *T = (TTree*)f->Get("FRO_analysis_tree");
   std::vector<unsigned int> multilayer;
   std::vector<unsigned int> layer;
   std::vector<unsigned int> tube;
   std::vector<double> *std_wy = new std::vector<double>;
   std::vector<double> *std_wz = new std::vector<double>;
   T->SetBranchAddress("std_wy",&std_wy);
   T->SetBranchAddress("std_wz",&std_wz);

   std::vector<unsigned int> tube;
   std::vector<unsigned int> layer;
   std::vector<unsigned int> multilayer;


   TBranch *bpt_tb = T->Branch("tube", &tube);
   TBranch *bpt_ly = T->Branch("layer", &layer);
   TBranch *bpt_ml = T->Branch("multilayer", &multilayer);
   Long64_t nentries = T->GetEntries();
   for (Long64_t i=0;i<nentries;i++) {
     std::cout << i << '/' << nentries << std::endl;
      T->GetEntry(i);
      tube.clear();
      layer.clear();
      multilayer.clear();

      for (unsigned int j = 0; j < std_wy->size(); ++j) {
          unsigned int t_tube = getTB(std_wy->at(j), std_wz->at(j));
          tube.push_back(t_tube);
          unsigned int t_layer = getLY(std_wz->at(j));
          layer.push_back(t_layer);
          unsigned int t_multilayer = getML(std_wz->at(j));
          multilayer.push_back(t_multilayer);
      }

      T->Fill();
   }
   T->Print();
   T->Write();
   f->Close();
}
