#!/bin/env python
# -*- utf-8 -*-
"""Mass production of config files for MDT_trigger."""

# Modules
import os
import subprocess
import numpy as np

# Define path variables
base_path = "../exe/"
alg_path = "conf_alg/"

# Create folders, if they don't already exist
for directory in [alg_path]:
    if not os.path.exists(base_path+directory):
        os.makedirs(base_path+directory)

# Define algorithm parameters
# 0.) Histogram based pattern recognition
bin_widths = np.arange(1.0, 4.6, 0.1)

# 1.) Binned 2-D Hough Transform
# nb_binned_algs = np.arange(2, 14, 4)
nb_binned_algs = np.arange(2,14, 4)
distance_binned_algs = np.arange(1., 7., 2.)

# 2.) (and 3.)) (Very) Fast Algorithm
b_width = [100000]
# m_width = [0.005, 0.015, 0.03, 0.05, 1000]
# m_width = np.arange(1.0, 3.5, 0.020)

# end-cap parameters
m_width = np.append(np.arange(0.001, 0.006, 0.001),
                    np.arange(0.006, 0.032, 0.002))

# barrel parameters coarse
# m_width = np.append(np.append(np.arange(0.080, 0.200, 0.020),
#                     np.arange(0.200, 0.800, 0.050)),
#                     np.arange(0.800, 1.500, 0.100))

# barrel parameters fine
# m_width = np.append(np.append(np.arange(0.002, 0.006, 0.001),
#                     np.arange(0.006, 0.020, 0.002)),
#                     np.arange(0.020, 0.080, 0.020))

search_width = np.arange(0.3, 5.2, 0.3)

# Change directory and clean up files
os.chdir(base_path + alg_path)
subprocess.call("rm alg*", shell=True)

# Create Algorithms steering files
# # 0.) Histogram based pattern recognition
# for binwidth in bin_widths:
#     file_name = "alg0_binwd_{}.algorithm".format(binwidth)
#     try:
#         f = open(file_name, 'w')
#         content = """\
# # Histogram based pattern recognition
# algorithm_Hist=1
# # Binwidth in mm
# algorithm_Hist_bin_width={}
# algorithm_Hist_single_track=1
#         """.format(binwidth)
#         f.write(content)
#         f.close()
#     except:
#         print "error with file {}\n".format(file_name)
#         continue
# 1.) Binned 2-D Hough Transform
# for b in bin_widths:
#     for nb in nb_binned_algs:
#         for d in distance_binned_algs:
#             file_name = "alg1_binwd_{}_nb_{}_d_{}.algorithm".format(b, nb, d)
#             try:
#                 f = open(file_name, 'w')
#                 content = """\
# # Binned 2-D Hough Transform
# algorithm_2DHough=1
# algorithm_2DHough_bin_width={}
# algorithm_2DHough_nb_binned_algs={}
# algorithm_2DHough_distance_binned_algs={}
#         """.format(b, nb, d)
#                 f.write(content)
#                 f.close()
#             except:
#                 print "error with file {}\n".format(file_name)
#                 continue

# 2.) Fast Algorithm
for b in b_width:
    for m in m_width:
        for s in search_width:
            file_name = "alg2_bw_{}_mw_{}_sw_{}.algorithm".format(b, m, s)
            try:
                f = open(file_name, 'w')
                content = """\
# Fast Algorithm
algorithm_Fast=1
algorithm_Fast_b_width={}
algorithm_Fast_m_width={}
algorithm_Fast_search_width={}
        """.format(b, m, s)
                f.write(content)
                f.close()
            except:
                print "error with file {}\n".format(file_name)
                continue

# 3.) Very Fast Algorithm
# for b in b_width:
#     for m in m_width:
#         file_name = "alg3_bw_{}_mw_{}_sw_{}.algorithm".format(b, m, 0.0)
#         try:
#             f = open(file_name, 'w')
#             content = """\
# # Very Fast Algorithm
# algorithm_VeryFast=1
# algorithm_VeryFast_b_width={}
# algorithm_VeryFast_m_width={}
#         """.format(b, m)
#             f.write(content)
#             f.close()
#         except:
#             print "error with file {}\n".format(file_name)
#             continue
