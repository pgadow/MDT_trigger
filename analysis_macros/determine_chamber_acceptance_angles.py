#!/bin/env python
# -*- utf-8 -*-
"""Determine slope and polar angle acceptance of MDT chambers."""

import sys
import time
import ROOT
import AtlasStyle
import AtlasUtils


def get_boundaries(histogram, desired_fraction):
    """determine boundaries for percentage area to histogram."""
    # find max interval (lower and higher bin) with 0.95 area
    nentries = histogram.GetSize()
    content = histogram.GetEntries()
    first_bin = -1
    last_bin = -1
    minimal_length = nentries
    for lower_bin in xrange(nentries):
        for upper_bin in xrange(lower_bin, nentries, 1):
            integral = histogram.Integral(lower_bin, upper_bin)
            fraction = float(integral) / float(content)
            if fraction > float(desired_fraction):
                interval_length = upper_bin - lower_bin
                if interval_length <= minimal_length:
                    minimal_length = interval_length
                    first_bin = lower_bin
                    last_bin = upper_bin
    return first_bin, last_bin


def add_boundaries(histogram, desired_fraction):
    """paint boundaries to histogram."""
    boundaries = get_boundaries(histogram, desired_fraction)
    lower_bound = histogram.GetXaxis().GetBinCenter(boundaries[0])
    upper_bound = histogram.GetXaxis().GetBinCenter(boundaries[1])
    y_max = histogram.GetMaximum()

    print lower_bound, upper_bound

    line1 = ROOT.TLine(lower_bound, 0, lower_bound, y_max)
    line1.SetLineColor(2)
    line2 = ROOT.TLine(upper_bound, 0, upper_bound, y_max)
    line2.SetLineColor(2)

    return [lower_bound, line1, upper_bound, line2]


if len(sys.argv) != 2:
    print "usage: specify output path as argument"
    sys.exit()

path = sys.argv[1]
if not path.endswith('/'):
    path += '/'

# get file
# f = ROOT.TFile("/mnt/scratch/pgadow/trigger/master_thesis/user.pgadow.data12_\
# 8TeV.MERGE.physics_EnhancedBias.recon.ESD.f507.160517.L1MU20.minitupel.root")
f = ROOT.TFile("/mnt/scratch/pgadow/trigger/master_thesis/user.pgadow.data12_8TeV.periodB.physics_Muons.PhysCont.AOD.repro14_v01.160517.MDT_standalone.v1_EXT0.MERGE.MDT-standalone.minitupel.root")
t = f.Get("L1_MDT_trigger_tree")

# create output file
output = open(path + 'acceptance.txt', 'w')
output.write('chamber,station,layer,slope_min,slope_max,slope_mean,slope_rms,\
theta_min,theta_max,theta_mean,theta_rms\n')

# define chambers and eta stations
chambers = [1, 2, 3, 4, 19, 20, 5, 6, 22, 23]
name = {1: "BIL", 2: "BIS", 3: "BML", 4: "BMS", 5: "BOL", 6: "BOS",
        19: "EML", 20: "EMS", 22: "EOL", 23: "EOS"}
eta_stations = [1, 2, 3, 4, 5, 6, 7, 8]

barrel = ['slope', '1./slope']  # barrel: 1./slope, end-cap: slope
lp = [0.2, 0.5]
lp2 = [0.2, 0.7]

percentage = 0.99

# create canvas
c1 = ROOT.TCanvas()

for chamber in chambers:
    for eta in eta_stations:
        for layer in ['inner', 'middle', 'outer']:
            # if chamber != 2 or eta != 7 or layer != 'inner':
            #     continue

            # slope
            command = "{}_{} >> h_slope_{}_{}(180,0,1.8)".format(
                    barrel[int(chamber) < 14], layer, chamber, eta)
            cut = " chamber_type_{} == {} && eta_index_{} == {} && pt > 10. \
&& fitChi2/fitNumberDoF < 3.".format(layer, chamber, layer, eta)
            t.Draw(command, cut)
            h_slope = ROOT.gDirectory.Get('h_slope_{}_{}'.format(chamber, eta))
            if h_slope.GetEntries() < 50:
                continue
            result = add_boundaries(h_slope, percentage)
            h_slope.SetTitle(';slope m; entries / 0.01')
            h_slope.Draw()
            result[1].Draw()
            result[3].Draw()
            AtlasUtils.myText(lp[result[0] < 0.7], 0.8, 1,
                              "{}% in [{}, {}]".format(
                    int(100*percentage), max(0, result[0]), result[2]))
            mean = h_slope.GetMean()
            rms = h_slope.GetRMS()
            AtlasUtils.myText(lp[result[0] < 0.7], 0.7, 1,
                              "m_{{mean}} = {:.2f} #pm {:.2f}".format(mean,
                                                                      rms))
            AtlasUtils.myText(lp2[result[0] < 0.7], 0.3, 1,
                              "{}{}".format(name[chamber], eta))
            c1.SaveAs(path + "h_slope_{}_{}_{}.png".format(chamber, eta, layer))
            c1.SaveAs(path + "h_slope_{}_{}_{}.C".format(chamber, eta, layer))
            output.write('{},{},{},'.format(name[chamber], eta, layer))
            output.write('{},{},{:.4f},{:.4f},'.format(
                max(0, result[0]), result[2], mean, rms))

            # theta
            command = "atan({}_{}) >> h_theta_{}_{}(180,0,3.6)".format(
                    barrel[int(chamber) < 14], layer, chamber, eta)
            t.Draw(command, cut)
            h_theta = ROOT.gDirectory.Get('h_theta_{}_{}'.format(chamber, eta))
            result = add_boundaries(h_theta, percentage)
            if h_theta.GetEntries() < 50:
                continue
            h_theta.SetTitle(';#theta [rad]; entries / 0.02 rad')
            result[1].Draw()
            result[3].Draw()
            AtlasUtils.myText(lp[result[0] < 1.2], 0.8, 1,
                              "{}% in [{}, {}]".format(
                    int(100*percentage), max(0, result[0]), result[2]))
            mean = h_theta.GetMean()
            rms = h_theta.GetRMS()
            AtlasUtils.myText(lp[result[0] < 1.2], 0.7, 1,
                              "#theta_{{mean}} = {:.2f} #pm {:.2f}".format(
                              mean, rms))
            AtlasUtils.myText(lp2[result[0] < 1.2], 0.3, 1,
                              "{}{}".format(name[chamber], eta))
            c1.SaveAs(path + "h_theta_{}_{}_{}.png".format(chamber, eta, layer))
            c1.SaveAs(path + "h_theta_{}_{}_{}.C".format(chamber, eta, layer))
            output.write('{},{},{:.4f},{:.4f}\n'.format(
                max(0, result[0]), result[2], mean, rms))
            time.sleep(1)
output.close()
