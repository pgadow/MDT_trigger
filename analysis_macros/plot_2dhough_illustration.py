#! /bin/env python
# coding: utf-8
import matplotlib.pyplot as plt
import numpy as np


def d(x,y, alpha):
 return x * np.cos(alpha) + y * np.sin(alpha)


pi = np.pi
plt.close()
# use xkcd layout for plot
plt.xkcd()

# muon straight line
x = np.arange(0,5,1)
y = np.arange(0,5,1)

x1 = np.arange(0,5,1)
y1 = np.arange(0,5,1)

x2 = np.arange(0,5,1)
y2 = np.arange(0,5,1)

# drift radii
for i in xrange(len(x2)):
    if i%2 == 0:
        x2[i] -= 1. *(i/(i%2+2))
    else:
        x2[i] += 1. *(0.5+i*i/(i%3+6))

# figure 0
plt.figure(0)
plt.xlim([-0.5, 4.5])
plt.ylim([-0.5, 4.5])
plt.xlabel('z [mm]')
plt.ylabel('y [mm]')
plt.plot(x1,y1, 'ro', x2, y2, 'ro', x, y, 'b')
plt.show()

plt.figure(1)
alpha = np.arange(0,pi,0.1)

plt.xlabel(r'$\alpha$ [rad]')
plt.ylabel('d [mm]')
for i in xrange(len(x)):
    plt.plot(alpha, d(x1[i],y1[i],alpha))
    plt.plot(alpha, d(x2[i],y2[i],alpha))
plt.show()
