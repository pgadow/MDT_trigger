"""event viewer for generated data."""
import ROOT
import sys
import matplotlib.pyplot as plt
# get r-t relation
ROOT.gSystem.Load('Rt_relation_cxx')
from ROOT import Rt_relation


class Point:
    """Point class represents and manipulates x,y coords."""

    def __init__(self, z=0, y=0):
        """Create a new point at the origin."""
        self.z = z
        self.y = y

    def __lt__(self, other):
        return self.z < other
    def ___le__(self, other):
        return self.z <= other
    def __gt__(self, other):
        return self.z > other
    def __ge__(self, other):
        return self.z >= other
    def __float__(self):
        return float(self.z)

# open ROOT file
if len(sys.argv) > 1:
    filename = str(sys.argv[1])
    recofilename = str(sys.argv[2])
    # tube_radius = float(sys.argv[4])
else:
    print("not enough arguments: filename, recofilename (0 if none)")
# if "BLR" in filename:
#     tube_radius = 7.5
# else:
#     tube_radius = 15.0
tube_radius = 7.5
ROOT.gROOT.Reset()

# tube parameters
wall_strength = 0.4
r2 = tube_radius
r1 = tube_radius - wall_strength


# get reconstruction file
print(recofilename)
if recofilename != '0':
    recofile = ROOT.TFile(recofilename)
    recotree = ROOT.gDirectory.Get('tree')
    recotree.Print()
    n = int(
        recofilename[recofilename.find('alg')+3:recofilename.find('alg')+4])

# get input file
print(filename)
f = ROOT.TFile(filename)
tree = ROOT.gDirectory.Get('MDTtriggerTree')
tree.Print()

rt = Rt_relation()
print(rt.number_of_pairs())
rt.read_rt('../share/default.rt')
print(rt.number_of_pairs())

# get all tube positions
wire_pos = set()

counter = 0
for event in tree:
    counter += 1
    if counter > 300:
        break
    for i in xrange(tree.wy.size()):
        y = tree.wy[i]
        z = tree.wz[i]
        wire_pos.add(Point(z, y))

event = 0
for jentry in xrange(tree.GetEntriesFast()):
    nb = tree.GetEntry(jentry)

    if tree.has_track == 0:
        continue

    if recofilename != '0':
        nc = recotree.GetEntry(event)
    event += 1

    m_gen = tree.m
    b_gen = tree.b

    min_z = sorted(list(wire_pos))[0].z-tube_radius
    max_z = sorted(list(wire_pos))[-1].z+tube_radius

    if recofilename != '0':
        m_rec = getattr(recotree, "a{}_m_rec".format(n))[0]
        b_rec = getattr(recotree, "a{}_b_rec".format(n))[0]
        if m_gen - m_rec < 0.003:
            continue
        print(m_gen - m_rec)
        print(m_gen, m_rec)
        print(b_gen, b_rec)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_aspect('equal')
    points_y = []
    points_z = []
    tube_walls = []
    tubes = []
    for p in wire_pos:
        tube_walls.append(plt.Circle((p.y, p.z), r2, color='silver'))
        tubes.append(plt.Circle((p.y, p.z), r1, color='whitesmoke'))
        fig.gca().add_artist(tube_walls[-1])
        fig.gca().add_artist(tubes[-1])

        points_y.append(p.y)
        points_z.append(p.z)

    drift_circles = []
    for i in xrange(tree.wy.size()):
        y = tree.wy[i]
        z = tree.wz[i]

        if tree.r[0] == 0:
            r = rt.r(tree.t[i], 0)
        else:
            r = tree.r[i]

        drift_circles.append(plt.Circle((y, z), r, color='red', fill=False))
        points_y.append(y)
        points_z.append(z)
        fig.gca().add_artist(drift_circles[i])

    plt.scatter(points_y, points_z, color='black')
    plt.plot([b_gen+m_gen*min_z, b_gen+m_gen*max_z], [min_z, max_z], 'k-',
             lw=1, color='blue')
    if recofilename != '0':
        plt.plot([b_rec+m_rec*min_z, b_rec+m_rec*max_z], [min_z, max_z], 'k-',
                 lw=1, color='red')
    plt.show()
