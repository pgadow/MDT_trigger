//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 31.03.1999, AUTHOR: OLIVER KORTNER
// Modified: 30.05.1999, removed dynamic memory management due to copy problems
//           02.03.2000, t(const double & r, const int & option) added.
//           25.05.2001, correct interpolation in r(r, option) and
//                       t(r, option).
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

////////////////////////////////////////////////////////////////////
// IMPLEMENTATION OF THE METHODS DEFINED IN THE CLASS Rt_relation //
////////////////////////////////////////////////////////////////////

#include "Rt_relation.hxx"
#include <fstream>

using namespace std;

//*****************************************************************************

//////////////////////
// COPY CONSTRUCTOR //
//////////////////////

Rt_relation::Rt_relation(const Rt_relation & rt) {

    copy(rt);

}

//*****************************************************************************

/////////////////
// METHOD copy //
/////////////////

void Rt_relation::copy(const Rt_relation & rt) {

//////////
// COPY //
//////////

	set_number_of_pairs(rt.number_of_pairs());

	for (int k=0; k<nb_pairs; k++) {
		r_drift[k] = rt.r(k);
		t_drift[k] = rt.t(k);
		r_err[k] = rt.error(k);
	}

	return;

}

//******************************************************************************

//////////////
// METHOD r //
//////////////

double Rt_relation::r(const int & k) const {

/////////////
// CHECK k //
/////////////

	if (k<0 || k>=nb_pairs) {
		cerr << endl << "Class Rt_relation, method r: "
			<< "illegal (r,t) pair number!" << endl;
		return 0.0;
	}

	return r_drift[k];

}

//*****************************************************************************

//////////////
// METHOD r //
//////////////

double Rt_relation::r(const double & t, const int & option)
									const {

/////////////////////////
// AUXILIARY VARIABLES //
/////////////////////////

	int out_of_range=0; // out-of-range flag

////////////////////////////////////
// CHECK IF THERE ARE (r,t) PAIRS //
////////////////////////////////////

	if (nb_pairs == 0) {
		cerr << endl << "Class Rt_relation, method r: "
			<< "there are no (r,t) pairs!" << endl;
		return 0.0;
	}

///////////////////
// CHECK t range //
///////////////////

	if (t < t_drift[0]) {
		out_of_range = -1;
	}
	if (t > t_drift[nb_pairs-1]) {
		out_of_range = 1;
	}

///////////////////////////////
// THROW WARNING IF REQUIRED //
///////////////////////////////

	if (out_of_range != 0 && option == 1) {
		cerr << endl << "Class Rt_relation, method r: "
			<< "warning: t out of range!" << endl;
	}

////////////////////
// CALCULATE r(t) //
////////////////////

	if (out_of_range == -1) {
		if (r_drift[0] >= 0.0) {
			return r_drift[0];
		}
		else {
			return 0.0;
		}
	}

	if (out_of_range == 1) {
		return r_drift[nb_pairs-1];
	}

	for (int k=1; k<nb_pairs; k++) {
		if (t>=t_drift[k-1] && t<t_drift[k]) {
			return (r_drift[k-1]+
				(r_drift[k]-r_drift[k-1])*(t-t_drift[k-1])/
					(t_drift[k]-t_drift[k-1]));
		}
	}

	return 0.0;

}

//*****************************************************************************

//////////////
// METHOD t //
//////////////

double Rt_relation::t(const int & k) const {

/////////////
// CHECK k //
/////////////

	if (k<0 || k>nb_pairs) {
		cerr << endl << "Class Rt_relation, method t: "
			<< "illegal (r,t) pair number "<< k << "!" << endl;
		return 0.0;
	}

	return t_drift[k];

}

//*****************************************************************************

//////////////
// METHOD t //
//////////////

double Rt_relation::t(const double & r, const int & option)
									const {

/////////////////////////
// AUXILIARY VARIABLES //
/////////////////////////

	int out_of_range=0; // out-of-range flag

////////////////////////////////////
// CHECK IF THERE ARE (r,t) PAIRS //
////////////////////////////////////

	if (nb_pairs == 0) {
		cerr << endl << "Class Rt_relation, method r: "
			<< "there are no (r,t) pairs!" << endl;
		return 0.0;
	}

///////////////////
// CHECK t range //
///////////////////

	if (r < r_drift[0]) {
		out_of_range = -1;
	}
	if (r > r_drift[nb_pairs-1]) {
		out_of_range = 1;
	}

///////////////////////////////
// THROW WARNING IF REQUIRED //
///////////////////////////////

	if (out_of_range != 0 && option == 1) {
		cerr << endl << "Class Rt_relation, method r: "
			<< "warning: t out of range!" << endl;
	}

////////////////////
// CALCULATE r(t) //
////////////////////

	if (out_of_range == -1) {
		return t_drift[0];
	}

	if (out_of_range == 1) {
		return t_drift[nb_pairs-1];
	}

	for (int k=1; k<nb_pairs; k++) {
		if (r>=r_drift[k-1] && r<r_drift[k]) {
			return (t_drift[k-1]+
				(t_drift[k]-t_drift[k-1])*(r-r_drift[k-1])/
					(r_drift[k]-r_drift[k-1]));
		}
	}

	return 0.0;

}

//*****************************************************************************

//////////////////
// METHOD error //
//////////////////

double Rt_relation::error(const int & k) const {

/////////////
// CHECK k //
/////////////

	if (k<0 || k>=nb_pairs) {
		cerr << endl << "Class Rt_relation, method error: "
			<< "illegal (r,t) pair number!" << endl;
		return 0.0;
	}

	return r_err[k];

}

//*****************************************************************************

//////////////////
// METHOD error //
//////////////////

double Rt_relation::error(const double & t,
						const int & option) const {

/////////////////////////
// AUXILIARY VARIABLES //
/////////////////////////

	int out_of_range=0; // out-of-range flag
//	double scal; // "time scaling factor"
	double err; // error

////////////////////////////////////
// CHECK IF THERE ARE (r,t) PAIRS //
////////////////////////////////////

	if (nb_pairs == 0) {
		cerr << endl << "Class Rt_relation, method r: "
			<< "there are no (r,t) pairs!" << endl;
		return 0.0;
	}

///////////////////
// CHECK t range //
///////////////////

	if (t < t_drift[0]) {
		out_of_range = -1;
	}
	if (t > t_drift[nb_pairs-1]) {
		out_of_range = 1;
	}

///////////////////////////////
// THROW WARNING IF REQUIRED //
///////////////////////////////

	if (out_of_range != 0 && option == 1) {
		cerr << endl << "Class Rt_relation, method error: "
			<< "warning: t out of range!" << endl;
	}

////////////////////////
// CALCULATE r_err(t) //
////////////////////////

	if (out_of_range == -1) {
		return r_err[0];
	}

	if (out_of_range == 1) {
		return r_err[nb_pairs-1];
	}

	for (int k=1; k<nb_pairs; k++) {
		if (t>=t_drift[k-1] && t<t_drift[k]) {
			err = r_err[k-1]+(r_err[k]-r_err[k-1])*(t-t_drift[k-1])
						/(t_drift[k]-t_drift[k-1]);
			return err;
		}
	}

	return 1.0;

}


//*****************************************************************************

////////////////////
// METHOD t_error //
////////////////////

double Rt_relation::t_error(const double & t,
						const int & option) const {

/////////////////////////
// AUXILIARY VARIABLES //
/////////////////////////

	int out_of_range=0; // out-of-range flag
	double scal; // "time scaling factor"
	double err; // error

////////////////////////////////////
// CHECK IF THERE ARE (r,t) PAIRS //
////////////////////////////////////

	if (nb_pairs == 0) {
		cerr << endl << "Class Rt_relation, method r: "
			<< "there are no (r,t) pairs!" << endl;
		return 0.0;
	}

///////////////////
// CHECK t range //
///////////////////

	if (t < t_drift[0]) {
		out_of_range = -1;
	}
	if (t > t_drift[nb_pairs-1]) {
		out_of_range = 1;
	}

///////////////////////////////
// THROW WARNING IF REQUIRED //
///////////////////////////////

	if (out_of_range != 0 && option == 1) {
		cerr << endl << "Class Rt_relation, method t_error: "
			<< "warning: t out of range!" << endl;
	}

////////////////////////
// CALCULATE r_err(t) //
////////////////////////

	if (out_of_range == -1) {
		return r_err[0]*fabs((t_drift[1]-t_drift[0])/
						(r_drift[1]-r_drift[0]));
	}

	if (out_of_range == 1) {
		return r_err[nb_pairs-1]*
			fabs((t_drift[nb_pairs-1]-t_drift[nb_pairs-2])/
				(r_drift[nb_pairs-2]-r_drift[nb_pairs-1]));
	}

	for (int k=1; k<nb_pairs; k++) {
		if (t>=t_drift[k-1] && t<t_drift[k]) {
			scal = fabs((t_drift[k]-t_drift[k-1])/
						(r_drift[k]-r_drift[k-1]));
			err = r_err[k-1]+(r_err[k]-r_err[k-1])*(t-t_drift[k-1])
						/(t_drift[k]-t_drift[k-1]);
			return scal*err;
		}
	}

	return 1.0;

}

//*****************************************************************************

/////////////////////////
// METHOD write_out_rt //
/////////////////////////

void Rt_relation::write_out_rt(ofstream & rt_file) const {

///////////////
// WRITE OUT //
///////////////

	rt_file << nb_pairs << endl;
//------------- number of (r,t) pairs

	for (int k=0; k<nb_pairs; k++) {
		rt_file << r_drift[k] << " " << t_drift[k] << " " << r_err[k]
			<< endl;
	}

	return;

}

//*****************************************************************************

////////////////////////////////
// METHOD set_number_of_pairs //
////////////////////////////////

void Rt_relation::set_number_of_pairs(const int & n_pairs) {

///////////////////////////////////////////////////
// CHECK IF THE NUMBER OF PAIRS IS LESS THAN 201 //
///////////////////////////////////////////////////

	if (n_pairs>300) {
		cerr << endl
			<< "Class Rt_relation, method set_number_of_pairs: "
			<< "too many (r,t) pairs!" << endl;
		return;
	}

//////////////////////////////////
// IF SO, RESET NUMBER OF PAIRS //
//////////////////////////////////

	nb_pairs = n_pairs;

	return;

}

//*****************************************************************************

////////////////////
// METHOD set_r_t //
////////////////////

void Rt_relation::set_r_t(const int & k, const double & r,
				const double & t, const double & error) {

/////////////
// CHECK k //
/////////////

	if (k<0 || k>=nb_pairs) {
		cerr << endl << "Class Rt_relation, method set_r_t: "
			<< "illegal (r,t) pair number!" << endl;
		return;
	}

//////////////////////////
// SET (r,t,error of r) //
//////////////////////////

	r_drift[k] = r;
	t_drift[k] = t;
	r_err[k] = error;

	return;

}

//*****************************************************************************

/////////////////////
// METHODD read_rt //
/////////////////////

void Rt_relation::read_rt(string rt_file) {

///////////////
// READ FILE //
///////////////
  ifstream rt_file_stream (rt_file.c_str(), ifstream::in);

	rt_file_stream >> nb_pairs;
//	cout << "number of pairs = " << nb_pairs << endl;
//--------- number of pairs

	set_number_of_pairs(nb_pairs);

// loop over the triples (r,t,error of r) //
	for (int k=0; k<nb_pairs; k++) {
		rt_file_stream >> r_drift[k];
// WARNING! QUICK BUG FIX //
//		r_drift[k] = r_drift[k]+0.284;
//		cout << "r_drift[" << k << "] = " << r_drift[k] << endl;
		rt_file_stream >> t_drift[k];
//		cout << "t_drift[" << k << "] = " << t_drift[k] << endl;
		rt_file_stream >> r_err[k];
//		cout << "r_err[" << k << "] = " << r_err[k] << endl;
	}

	return;

}
