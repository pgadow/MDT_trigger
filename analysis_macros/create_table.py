#!/bin/env python
# -*- utf-8 -*-
"""Create table for latex document with results."""

import os
import subprocess
import math

alg0_results = {}
alg1_results = {}
alg2_results = {}
alg3_results = {}

# Set working directory
base_path = "../exe/conf_sim"
for chamber in sorted(os.listdir(base_path)):
    chamber = chamber.replace('.simulation', '')

    if "E" not in chamber:
        continue
    if "EE" in chamber:
        continue
    command = "python"
    script = "plot_results.py"
    args = "{}".format(chamber)
    output = subprocess.check_output([command, script, args])
    # algorithm 0
    alg0_start_position = output.find("Optimal values algorithm 0")+27
    alg0_end_position = output.find("Optimal values algorithm 1")
    alg0_maximal_values = \
        output[alg0_start_position:alg0_end_position].split("\n")[0].split(" ")
    alg0_optimal_values = \
        output[alg0_start_position:alg0_end_position].split("\n")[1].split(" ")

    # algorithm 1
    alg1_start_position = output.find("Optimal values algorithm 1")+27
    alg1_end_position = output.find("Optimal values algorithm 2")
    alg1_maximal_values = \
        output[alg1_start_position:alg1_end_position].split("\n")[0].split(" ")
    alg1_optimal_values = \
        output[alg1_start_position:alg1_end_position].split("\n")[1].split(" ")

    # algorithm 2
    alg2_start_position = output.find("Optimal values algorithm 2")+27
    alg2_end_position = output.find("Optimal values algorithm 3")
    alg2_maximal_values = \
        output[alg2_start_position:alg2_end_position].split("\n")[0].split(" ")
    alg2_optimal_values = \
        output[alg2_start_position:alg2_end_position].split("\n")[1].split(" ")

    # algorithm 3
    alg3_start_position = output.find("Optimal values algorithm 3")+27
    alg3_maximal_values = \
        output[alg3_start_position:].split("\n")[0].split(" ")
    alg3_optimal_values = \
        output[alg3_start_position:].split("\n")[1].split(" ")

    result = chamber + ' & ' + alg0_maximal_values[0] + ' & ' + \
        alg0_maximal_values[1] + ' & ' + alg0_maximal_values[3] + ' & ' + \
        alg0_maximal_values[2] + '\\\\'
    alg0_results[chamber] = result

    result = chamber + ' & ' + alg1_maximal_values[0] + ' & ' + \
        alg1_maximal_values[1] + ' & ' + alg1_maximal_values[3] + ' & ' + \
        alg1_maximal_values[4] + ' & ' + alg1_maximal_values[5] + ' & ' + \
        str(int(math.floor(float(alg1_maximal_values[2])))) + '\\\\'
    alg1_results[chamber] = result

    result = chamber + ' & ' + alg2_maximal_values[0] + ' & ' + \
        alg2_maximal_values[1] + ' & ' + alg2_maximal_values[5] + ' & ' + \
        alg2_maximal_values[4] + ' & ' + alg2_maximal_values[2] + '\\\\'
    alg2_results[chamber] = result

    result = chamber + ' & ' + alg3_maximal_values[0] + ' & ' + \
        alg3_maximal_values[1] + ' & ' + \
        alg3_maximal_values[4] + ' & ' + alg3_maximal_values[2] + '\\\\'
    alg3_results[chamber] = result


for chamber in sorted(alg0_results):
    print alg0_results[chamber]

print '\n'

for chamber in sorted(alg1_results):
    print alg1_results[chamber]

print '\n'

for chamber in sorted(alg2_results):
    print alg2_results[chamber]

print '\n'

for chamber in sorted(alg3_results):
    print alg3_results[chamber]
