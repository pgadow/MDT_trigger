#! /bin/env python
"""produce residual plots from tree holding reconstructed track segments."""

import sys
import ROOT
import AtlasStyle
import AtlasUtils


def cosmetics(histogram, axis_name, limits, width, m_width, threshold, unit):
    """beautify histograms."""
    min_bin = histogram.GetXaxis().FindBin(-limits)
    max_bin = histogram.GetXaxis().FindBin(limits)
    integral = min(float(histogram.Integral(min_bin, max_bin)), 10000)
    efficiency = integral / 100.

    histogram_red = histogram.Clone()
    histogram_red.SetName('{}_red'.format(histogram))
    histogram_red.SetFillColor(2)

    histogram_green = histogram.Clone()
    histogram_green.SetName('{}_green'.format(histogram))
    histogram_green.SetFillColor(3)

    for b in xrange(histogram.GetSize()):
        if b > min_bin and b < max_bin:
            histogram_red.SetBinContent(b, 0.)
        else:
            histogram_green.SetBinContent(b, 0.)

    histogram.Rebin(1)
    histogram_red.Rebin(1)
    histogram_green.Rebin(1)

    if unit != '':
        unitbox = '[' + unit + ']'
    else:
        unitbox = ''

    bin_width = histogram.GetXaxis().GetBinWidth(1)
    histogram.GetXaxis().SetTitle('{}_{{rec}} - {}_{{gen}} {}'.format(
                                                        axis_name, axis_name,
                                                        unitbox))
    histogram.GetXaxis().SetNdivisions(505)
    histogram.GetYaxis().SetTitle('entries / {} {}'.format(float(bin_width),
                                                           unit))

    histogram.Draw()
    histogram_green.Draw('SAME')
    histogram_red.Draw('SAME')
    AtlasUtils.myText(0.22, 0.6, 1,
                      "#varepsilon = {} %".format(float(efficiency)))
    AtlasUtils.myText(0.2, 0.85, 1,
                      "{}".format(width))
    AtlasUtils.myText(0.2, 0.79, 1,
                      "threshold: {} hits".format(threshold))

    if(len(m_width) > 0):
        AtlasUtils.myText(0.2, 0.73, 1,
                          "m width: {}".format(m_width))

    leg = ROOT.TLegend(0.6, 0.7, 0.85, 0.9)
    leg.AddEntry(histogram_green,
                 "{}_{{rec}} - {}_{{gen}} < 3#sigma_{{MDT}}".format(
        axis_name, axis_name), "f")
    leg.AddEntry(histogram_red,
                 "{}_{{rec}} - {}_{{gen}} > 3#sigma_{{MDT}}".format(
        axis_name, axis_name), "f")
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)

    return leg

if len(sys.argv) < 5:
    print 'wrong usage: input path, alg, threshold, bin/search width [m width]'
    sys.exit()
input_path = sys.argv[1]
alg = sys.argv[2]
threshold = sys.argv[3]
bsw = sys.argv[4]
if len(sys.argv) > 5:
    mw = sys.argv[5]
else:
    mw = ''

# get file
f = ROOT.TFile(input_path)
t = f.Get('tree')

t.Draw("a{}_m_rec - m_gen>>h_m(300,-0.015, 0.015)".format(alg),
       "a{}_nb_hits >={}".format(alg, threshold))
t.Draw("a{}_b_rec - b_gen>>h_b(800,-4, 4)".format(alg),
       "a{}_nb_hits >={}".format(alg, threshold))

# get histograms
h_m = ROOT.gDirectory.Get('h_m')
h_b = ROOT.gDirectory.Get('h_b')

c1 = ROOT.TCanvas()
c1.SetLogy()

# draw histograms
leg = cosmetics(h_m, 'm', 0.003123, "{}".format(bsw), mw, threshold, '')
leg.Draw()
b = raw_input()

leg = cosmetics(h_b, 'b', 1.96, "{}".format(bsw), mw, threshold, 'mm')
leg.Draw()
b = raw_input()
