## MDT trigger software

### Introduction
This collection of software enables the study of different analysis algorithms.

### Setup

A prerequisite is the CLHEP library that provides severeal implementations of geometrical quantities and random number generators.

To install it you have to

```
source setup.sh
```

while in the project root directory. This also calls the build scripts that call the makefiles for the software, which are located in `exe`.

### Usage

The software provides three different executables: `run_simulation.exe`, `run_data.exe` and `run_testbeam.exe`


To quickly produce simulated data enter

```
cd exe
bash run_simulation.sh
```

