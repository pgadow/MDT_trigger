#!/bin/bash

# DEFINITIONS FOR FORMATTED OUTPUT
E1='\033[0;32m'
E2='\033[1;32m'
NC='\033[0m' # No Color


echo -e "${E1}------------------------------------------${NC}"
echo -e "${E1}MDT Trigger${NC}"
echo -e ""
echo -e "${E1}Authors:${E2} Oliver Kortner, Philipp Gadow${NC}"
echo -e "${E1}Contact:${E2} Paul.Philipp.Gadow@cern.ch${NC}"
echo -e "${E1}(c) 2017${NC}"
echo -e "${E1}------------------------------------------${NC}"

echo "Set Up MDTTrigger software."

# CLHEP
if [ ! -d "share/libCLHEP" ]; then
    echo "No CLHEP library was found in the required path: ${E2}share/libCLHEP${NC}."
    # get CLHEP
    echo "Downloading CLHEP library"
    wget http://proj-clhep.web.cern.ch/proj-clhep/DISTRIBUTION/tarFiles/clhep-2.3.4.4.tgz
    tar xvf clhep-2.3.4.4.tgz

    # make CLHEP
    echo "Make CLHEP library"
    mkdir -p share/libCLHEP 2.3.4.4/build
    cd 2.3.4.4/build
    cmake -DCMAKE_INSTALL_PREFIX=../../share/libCLHEP ../CLHEP
    cmake --build . --config RelWithDebInfo
    ctest
    cmake --build . --target install

    # clean up
    echo "Clean up CLHEP building prerequisites"
    cd ../..
    rm -r 2.3.4.4
    rm clhep-2.3.4.4.tgz
fi

# ROOT 5
if [ ! -d "share/root-v5.34.36" ]; then
    echo "No ROOT library was found in the required path: ${E2}share/root-v5.34.36${NC}."
    # get CLHEP
    echo "Downloading ROOT"
    wget https://root.cern.ch/download/root_v5.34.36.Linux-ubuntu14-x86_64-gcc4.8.tar.gz
    tar xvf root_v5.34.36.Linux-ubuntu14-x86_64-gcc4.8.tar.gz
    mv root share/root-v5.34.36
    rm root_v5.34.36.Linux-ubuntu14-x86_64-gcc4.8.tar.gz
    source share/root-v5.34.36/bin/thisroot.sh
fi

# call make files for MDTTrigger
echo "Make library for MDTTrigger"
cd exe
source build_src.sh
source build_exe.sh

echo -e "Finished installation. The executables can be found in the ${E2}exe${NC} directory."