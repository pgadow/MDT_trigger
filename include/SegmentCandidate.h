//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_SegmentCandidateH
#define MDTTrigger_SegmentCandidateH

//::::::::::::::::::::::::::::
//:: CLASS SegmentCandidate ::
//::::::::::::::::::::::::::::

/// \class SegmentCandidate
///
/// Class storing the data of a MDT segment candidate found by the MDT trigger
/// algorithm.
///
/// \date 24.10.2013
///
/// \author Oliver.Kortner@CERN.CH

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// mt-offline //
#include "straight_line.hxx"

namespace MDTTrigger {

class SegmentCandidate {

public:
// Constructors //
    SegmentCandidate(void);
    ///< Default constructor.
    ///< Empty. Only there to allow use in STL vectors.

    SegmentCandidate(const Straight_line & segment,
                     const unsigned int & nb_hits,
                     const double & chi2,
                     const double & m_err);
    ///< Constructor.
    ///< \param segment Straight track segment.
    ///< \param nb_hits Number of hits on the track segment.
    ///< \param chi2    chi^2 value of the reconstructed segment.
    ///< \param m_err   Estimated error of the track slope.

// Methods //
    const Straight_line & getSegment(void) const;
    ///< Get the straight track segment.

    unsigned int getNumberOfHits(void) const;
    ///< Get the number of hits on the track segment.

    double getChi2(void) const;
    ///< Get the chi^2 value of the reconstructed segment.

    double getSlopeError(void) const;
    ///< Get the estimate error of the reconstructed track slope.

    bool operator < (const SegmentCandidate& seg) const{
        if(m_nb_hits == seg.getNumberOfHits()){
          return (m_chi2 < seg.getChi2());
        }
        return (m_nb_hits < seg.getNumberOfHits());
    }
    ///< Override "<"-operator for sort algorithm

private:
    Straight_line m_segment;
    unsigned int m_nb_hits;
    double m_chi2;
    double m_m_err;

};

}

#endif
