//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 22.12.1998, AUTHOR: OLIVER KORTNER
// Modified: 03.02.1999, method dist_from_line for points added.
// 	     Wed May 17 13:56:11 CEST 2000 by Felix Rauscher, method
//					transform_into_coordinate_system
//	     Wed May 17 15:41:26 CEST 2000 by Felix Rauscher method
//					intersection_with_plane
//	     Wed May 24 17:54:34 CEST 2000 by felix Rauscher:
//			-Constructur gets const references
//			-Datamembers are protected
//			-set-Functions are virtual
//			-dist_from_line2 inserted
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/////////////////////////
// CLASS Straight_line //
/////////////////////////

/////////////////////////////////////////////////////
// THIS CLASS PROVIDES STRAIGHT LINES TO THE USER. //
/////////////////////////////////////////////////////

#ifndef StraightLineHXX
#define StraightLineHXX

//////////////////////////
// INCLUDE HEADER FILES //
//////////////////////////

// CLHEP //
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/Rotation.h"

// standard C++ libraries //

#include <iostream>
#include <cstdio>
#include <cmath>


class Straight_line {

protected:
	CLHEP::Hep3Vector position, direction; // position vector and direction vector


public:
// Constructors
	Straight_line(const CLHEP::Hep3Vector & position_ini,
					const CLHEP::Hep3Vector & direction_ini) {
		position = position_ini;
		direction = direction_ini;
		}			// vector based constructor
	Straight_line(const double & m_x, const double & b_x,
			const double & m_y, const double & b_y) {
			position = CLHEP::Hep3Vector(b_x, b_y, 0.0);
			direction = CLHEP::Hep3Vector(m_x, m_y, 1.0);
		}			// slope and offset based constructor
	Straight_line(void) { }		// declaration constructor
	Straight_line(const Straight_line & h) {
		position = h.position_vector();
		direction = h.direction_vector();
		}			// copy constructor
	virtual ~Straight_line(void) {
		}			// virtual destructor

// Methods
// get-methods
	inline CLHEP::Hep3Vector position_vector(void) const { return position; }
					// get position vector
	inline CLHEP::Hep3Vector direction_vector(void) const { return direction; }
					// get direction vector
	inline void print_parameters(void) const;
					// print line parameters
	inline CLHEP::Hep3Vector point_on_line(const double lambda) const;
					// point for given lambda,
					// point = position_vector +
					//             lambda*direction_vector
	inline double sign_dist_from(const Straight_line & h) const;
					// signed distance of two lines
					// (if both are parallel, dist > 0)
	inline double dist_from_line(const CLHEP::Hep3Vector & point) const;
					// distance of point point from straight
					// line
	inline double dist_from_line2(const CLHEP::Hep3Vector & point) const;
					// squared distance of point point from straight
					// line
	inline double mx(void) const;
					// get m_x
	inline double bx(void) const;
					// get b_x
	inline double my(void) const;
					// get m_y
	inline double by(void) const;
					// get b_y
// set-methods
	inline virtual  void set_position(const CLHEP::Hep3Vector & new_pos);
					// set position vector
	inline virtual  void set_direction(const CLHEP::Hep3Vector & new_dir);
					// set direction vector
// intersection of line with plane
	inline bool intersection_with_plane(const CLHEP::Hep3Vector &norm, const CLHEP::Hep3Vector &point,
			CLHEP::Hep3Vector & intersection) const;
					//norm: Normal vector on plane
					//point: Point in plane
					//intersection: return value - intersectio point
					//returns true line || plane

};


///////////////////////////////////////////
// INCLUDE IMPLEMENTATION OF THE METHODS //
///////////////////////////////////////////

#include "straight_line.ixx"

#endif
