//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 25.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_MDTChamberEventGeneratorH
#define MDTTrigger_MDTChamberEventGeneratorH

//::::::::::::::::::::::::::::::::::::
//:: CLASS MDTChamberEventGenerator ::
//::::::::::::::::::::::::::::::::::::

/// \class MDTChamberEventGenerator
///
/// This class makes it possible to shoot a muon through an MDT chamber to
/// create muon hits and to add delta electron and random background hits.
///
/// \date 25.10.2013
///
/// \author Oliver.Kortner@CERN.CH

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <vector>
#include <string>

// mt-offline //
#include "Rt_relation.hxx"
#include "straight_line.hxx"

// ROOT //
#include "TRandom3.h"

// MDTTrigger //
#include "BareMDTHit.h"

namespace MDTTrigger {

class MDTChamberEventGenerator {

public:
// Constructor //
    MDTChamberEventGenerator(void);
    ///< Default constructor.
    ///< Default parameters:
    ///< Tube radius: 15 mm.
    ///< Tube wall thickness: 0.4 mm.
    ///< Number of tubes per layer: 36.
    ///< Number of layers per multilayer: 3.
    ///< Space thickness: 121 mm.
    ///< r-t relationship file: share/default.rt

    MDTChamberEventGenerator(const double & R,
                             const double & w_wall,
                             const unsigned int & nb_tubes_per_layer,
                             const unsigned int & nb_layers_per_multilayer,
                             const unsigned int & nb_multilayers,
                             const double & y_offset,
                             const double & z_offset,
                             const double & spacer_thickness,
                             const std::string & rt_file_name);
    ///< Constructor.
    ///< \param R   Tube radius.
    ///< \param w_wall  Thickness of the tube wall.
    ///< \param nb_tubes_per_layer  Number of tubes per layer.
    ///< \param nb_layers_per_multilayer    Number of layers per multilayer.
    ///< \param nb_multilayers    Number of multilayers.
    ///< \param y_offset    Offset in y to match data
    ///< \param z_offset    Offset in y to match data
    ///< \param spacer_thickness    Thickness of the spacer.
    ///< \param Name of the file with the r-t relationship.

// Methods //
    const Rt_relation & rt(void) const;
    ///< Get the r-t relationship used internally.

    void kill_wire(unsigned int number);
    ///< Kill wire, no hit will be generated from this wire
    ///< \param number Wire number, start counting from 0

    const std::vector<BareMDTHit> & generate(const Straight_line & track,
                                             bool delta_electrons_on,
                                             const double & t_dead,
                                             const double & occupancy,
                                             const double & road_width,
                                             const double & t0_shift,
                                             const double & t_accuracy);
    ///< Method to generate hits in the given MDT chamber.
    ///< \param track   Straight track in the chamber coordinate frame.
    ///< \param delta_electrons_ons Flag to turn on/off the creation of delta
    ///<                            electron hits inside a tube traversed by
    ///<                            the muon track.
    ///< \param t_dead  Dead time of the electronics.
    ///< \param road_width  Road width around the muon in which hits should be
    ///<                    created.
    ///< \param t0_shift    Offset to be able to the measured drift times.
    ///< \param t_accuracy  Resolution of the time measurement.


private:
    double m_R; // tube radius
    double m_w_wall; // tube wall thickness
    unsigned int m_nb_tubes_per_ly; // number of tubes per layer
    unsigned int m_nb_ly_per_ml; // number of layers per multilayer
    unsigned int m_nb_ml; // number of multilayers
    double m_y_offset; // offset to match data
    double m_z_offest; // offset to match data
    Rt_relation m_rt; // r-t relationship

    TRandom3 *m_p_rnd;
    // Random number generator.

    std::vector< std::vector<Straight_line> > m_wire_position;
    // Positions of all wires of the chamber.
    // Indexing: [layer][tube]

    std::vector<bool> m_dead_wires;
    // vector with all tube numbers: 0 wire working, 1 wire dead
    std::vector<BareMDTHit> m_bare_hit;
    // Vector containing the bare hits created by the generate method.

    void init(const double & R,
              const double & w_wall,
              const unsigned int & nb_tubes_per_layer,
              const unsigned int & nb_layers_per_multilayer,
              const unsigned int & nb_multilayers,
              const double & y_offset,
              const double & z_offset,
              const double & spacer_thickness,
              const std::string & rt_file_name);

};

}

#endif
