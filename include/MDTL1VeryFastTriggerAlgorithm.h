//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 25.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_MDTL1VeryFastTriggerAlgorithmH
#define MDTTrigger_MDTL1VeryFastTriggerAlgorithmH

//:::::::::::::::::::::::::::::::::::::
//:: CLASS MDTL1VeryFastTriggerAlgorithm ::
//:::::::::::::::::::::::::::::::::::::

/// \class MDTL1VeryFastTriggerAlgorithm
///
/// This class holds an implementation of the Semi-Analytic L1 MDT
/// trigger algorithm, which is based on an idea of Oliver Kortner.
///
/// \date 19.02.2016
///
/// \author Paul.Philipp.Gadow@cern.ch

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// CLHEP //
#include "CLHEP/Vector/ThreeVector.h"

// standard C++ //
#include <vector>

// MDTTrigger //
#include "BareMDTHit.h"
#include "SegmentCandidate.h"
#include "straight_line.hxx"
#include "MDTChamberParameters.h"

// ROOT //
#include "TH1.h"
#include "TH2.h"

namespace MDTTrigger {

class MDTL1VeryFastTriggerAlgorithm {

public:
// Constructors //
    MDTL1VeryFastTriggerAlgorithm(\
                          const double & b_width,
                          const double & m_width
                          );
    ///< Constructor.
    ///< \param b_width        search width in b for selection of tangents
    ///< \param m_width        search width in m for selection of tangents


// Methods //
    const std::vector<SegmentCandidate> & getSegmentCandidates(
                        const std::vector<BareMDTHit> & hits,
                        const Straight_line & L0_track,
                        const double & t_accuracy,
                        const Rt_relation & rt);
    ///< Run the trigger algorithm.
    ///< \param hits          MDT hits to be used in the algorithm.
    ///< \param L0_track      Seed track found by RPC/TGC detector
    ///< \param t_accuracy    Time resolution of electronics
    ///< \param rt            R-t relation.


private:
    std::vector<double> calculateTangent(double r1, double r2, double L2,\
                                        unsigned int configuration);
    ///< Calculate the tangent from two drift radii and their radial distance
    ///< \param r1  drift radius of first hit
    ///< \param r2  drift radius of second hit
    ///< \param L2   distance squared between the wire positions
    ///< \param configuration which of 4 possible tangents



    void patternRecognition(std::vector<BareMDTHit> & hits);
    ///< Perform pattern recognition on bare MDT hits
    ///< \param hits MDT hits for pattern recognition


    double m_m_width; // search width for accepting tangents (slope)
    double m_b_width; // search width for accepting tangents in mm (intercept)

    std::vector<SegmentCandidate> m_segment; // reconstruced segments

};

}

#endif
