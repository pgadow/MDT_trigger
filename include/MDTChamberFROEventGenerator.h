//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 25.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_MDTChamberFROEventGeneratorH
#define MDTTrigger_MDTChamberFROEventGeneratorH

//::::::::::::::::::::::::::::::::::::
//:: CLASS MDTChamberFROEventGenerator ::
//::::::::::::::::::::::::::::::::::::

/// \class MDTChamberFROEventGenerator
///
/// This class makes it possible to shoot a muon through an MDT chamber to
/// create muon hits and to add delta electron and random background hits.
///
/// \date 25.10.2013
///
/// \author Oliver.Kortner@CERN.CH

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <vector>
#include <string>

// mt-offline //
#include "Rt_relation.hxx"
#include "straight_line.hxx"

// ROOT //
#include "TRandom3.h"

// MDTTrigger //
#include "BareMDTHit.h"

namespace MDTTrigger {

class MDTChamberFROEventGenerator {

public:
// Constructor //
    MDTChamberFROEventGenerator(void);
    ///< Default constructor.
    ///< Default parameters:
    ///< Tube radius: 15 mm.
    ///< Tube wall thickness: 0.4 mm.
    ///< Number of tubes per layer: 36.
    ///< Number of layers per multilayer: 3.
    ///< Space thickness: 121 mm.
    ///< r-t relationship file: share/default.rt

    MDTChamberFROEventGenerator(const double & R,
                             const double & w_wall,
                             const unsigned int & nb_tubes_per_layer,
                             const unsigned int & nb_layers_per_multilayer,
                             const double & spacer_thickness,
                             const std::string & rt_file_name);
    ///< Constructor.
    ///< \param R   Tube radius.
    ///< \param w_wall  Thickness of the tube wall.
    ///< \param nb_tubes_per_layer  Number of tubes per layer.
    ///< \param nb_layers_per_multilayer    Number of layers per multilayer.
    ///< \param spacer_thickness    Thickness of the spacer.
    ///< \param Name of the file with the r-t relationship.

// Methods //
    const Rt_relation & rt(void) const;
    ///< Get the r-t relationship used internally.

    const std::vector<BareMDTHit> & generate(const Straight_line & track,
                                             bool delta_electrons_on,
                                             const double & t_min,
                                             const double & occupancy,
                                             const double & road_width,
                                             const double & t0_shift,
                                             const double & t_accuracy,
                                             int & nb_muon_hits);
    ///< Method to generate hits in the given MDT chamber.
    ///< \param track   Straight track in the chamber coordinate frame.
    ///< \param delta_electrons_ons Flag to turn on/off the creation of delta
    ///<                            electron hits inside a tube traversed by
    ///<                            the muon track.
    ///< \param t_min   Lower edged of the drift-time window.
    ///< \param road_width  Road with around the muon in which hits should be
    ///<                    created.
    ///< \param t0_shift    Offset to be able to the measured drift times.
    ///< \param t_accuracy  Resolution of the time measurement.
    ///< \param nb_muon_hits Number of unmasked muon hits. It is a return value
    ///<                     of the method

private:
    double m_R; // tube radius
    double m_w_wall; // tube wall thickness
    unsigned int m_nb_tubes_per_ly; // number of tubes per layer
    unsigned int m_nb_ly_per_ml; // number of layers per multilayer
    Rt_relation m_rt; // r-t relationship

    TRandom3 *m_p_rnd;
    // Random number generator.

    std::vector< std::vector<Straight_line> > m_wire_position;
    // Positions of all wires of the chamber.
    // Indexing: [layer][tube]

    std::vector<BareMDTHit> m_bare_hit;
    // Vector containing the bare hits created by the generate method.

    void init(const double & R,
              const double & w_wall,
              const unsigned int & nb_tubes_per_layer,
              const unsigned int & nb_layers_per_multilayer,
              const double & spacer_thickness,
              const std::string & rt_file_name);

};

}

#endif
