//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 22.12.1998, AUTHOR: OLIVER KORTNER
// Modified: 03.02.1999, method dist_from_line for points added. 
// 	     Wed May 17 13:56:11 CEST 2000 by Felix Rauscher, method 
//					transform_into_coordinate_system
//	     Wed May 17 15:41:26 CEST 2000 by Felix Rauscher method
//					intersection_with_plane	
//	     Wed May 24 17:54:34 CEST 2000 by felix Rauscher:
//			-Constructur gets const references
//			-Datamembers are protected
//			-set-Functions are virtual
//			-dist_from_line2 inserted
//           06.06.2012 by O. Kortner, small restructuring of the code for
//                      MDT trigger studies.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/////////////////////////
// CLASS Straight_line //
/////////////////////////

/////////////////////////////////////////////////////
// THIS CLASS PROVIDES STRAIGHT LINES TO THE USER. //
/////////////////////////////////////////////////////

#ifndef StraightLineHXX
#define StraightLineHXX

//////////////////////////
// INCLUDE HEADER FILES //
//////////////////////////

// ROOT //
#include "TVector3.h"

// standard C++ libraries //
#include <iostream>
#include <cmath>


class Straight_line {

protected:
	TVector3 position, direction; // position vector and direction vector


public:
// Constructors
	Straight_line(const TVector3 & position_ini,
					const TVector3 & direction_ini) {
		position = position_ini;
		direction = direction_ini;
		}			// vector based constructor
	Straight_line(const double & m_x, const double & b_x,
			const double & m_y, const double & b_y) {
			position = TVector3(b_x, b_y, 0.0);
			direction = TVector3(m_x, m_y, 1.0);
		}			// slope and offset based constructor
	Straight_line(void) { }		// declaration constructor
	Straight_line(const Straight_line & h) {
		position = h.position_vector();
		direction = h.direction_vector();
		}			// copy constructor
	virtual ~Straight_line(void) {
		}			// virtual destructor

// Methods
// get-methods
	inline const TVector3 & position_vector(void) const { return position; }
					// get position vector
	inline const TVector3 & direction_vector(void) const { return direction; }
					// get direction vector
	void print_parameters(void) const;
					// print line parameters
	const TVector3 & point_on_line(const double lambda) const;
					// point for given lambda,
					// point = position_vector +
					//             lambda*direction_vector
	double sign_dist_from(const Straight_line & h) const;
					// signed distance of two lines
					// (if both are parallel, dist > 0)
	double dist_from_line(const TVector3 & point) const;
					// distance of point point from straight
					// line
	double dist_from_line2(const TVector3 & point) const;
					// squared distance of point point from straight
					// line
	double mx(void) const;
					// get m_x
	double bx(void) const;
					// get b_x
	double my(void) const;
					// get m_y
	double by(void) const;
					// get b_y
// set-methods
	virtual  void set_position(const TVector3 & new_pos);
					// set position vector
	virtual  void set_direction(const TVector3 & new_dir);
					// set direction vector
// intersection of line with plane
	bool intersection_with_plane(const TVector3 &norm, 
            const TVector3 &point,
			TVector3 & intersection) const;
					//norm: Normal vector on plane
					//point: Point in plane
					//intersection: return value - intersection point
					//returns true line || plane

};


///////////////////////////////////////////
// INCLUDE IMPLEMENTATION OF THE METHODS //
///////////////////////////////////////////

#include "straight_line.ixx"

#endif
