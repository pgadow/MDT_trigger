//++++++++++++++++++++++++++++++++++
#ifndef MDTTrigger_MDTTriggerChambersH
#define MDTTrigger_MDTTriggerChambersH
#include <vector>

struct MDTChamberParameters{
  double tube_radius;//15.0,
  double tube_wall;//0.4,
  unsigned int tubes_per_layer;//36,
  unsigned int layers_per_multilayer;//3,
  unsigned int multilayers;//2,
  double y_offset; //0.,
  double z_offset; //0.,
  double spacer_thickness;//121.0,
  double dead_time;//200.0,
  double ROI_width;//120.0
  unsigned int use_std; // 0
  unsigned int store_input; // 0

  std::vector<unsigned int> dead_wires;
};
#endif
