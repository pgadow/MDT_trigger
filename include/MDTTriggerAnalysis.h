//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_MDTTriggerAnalysisH
#define MDTTrigger_MDTTriggerAnalysisH

//::::::::::::::::::::::::::::::
//:: CLASS MDTTriggerAnalysis ::
//::::::::::::::::::::::::::::::

/// \class MDTTriggerAnalysis
///
/// This class is used to study the performance of the MDT L1 trigger algorithm.
///
/// \date 29.10.2013
///
/// \author Oliver.Kortner@CERN.CH

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <string>
#include <vector>
#include <map>

// ROOT //
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TRandom3.h"

// mt-offline //
#include "straight_line.hxx"
#include "Rt_relation.hxx"

// MDTTrigger //
#include "MDTChamberEventGenerator.h"
#include "MDTL1TriggerAlgorithm.h"
#include "MDTL1Hough2DTriggerAlgorithm.h"
#include "MDTL1FastTriggerAlgorithm.h"
#include "MDTL1VeryFastTriggerAlgorithm.h"
#include "MDTChamberParameters.h"
#include "MDTTriggerEvent.h"
#include "TreeReader.h"

namespace MDTTrigger {

class MDTTriggerAnalysis {

public:
// Constructors //
    MDTTriggerAnalysis(const std::string & ROOT_outfile_name,
                       const std::string & simulationfile,
                       const std::string & algorithmfile,
                       const std::string & parameterfile);
    ///< Monte Carlo Constructor.
    ///< \param ROOT_outfile_name   Name of the output ROOT file.
    ///< \param simulationfile Simulation file with parameters for MC generator
    ///< \param algorithmfile Algorithm file, specification of algorithm
    ///< \param parameterfile Parameter file with chamber geometry


    MDTTriggerAnalysis(const std::string & ROOT_outfile_name,
                       TTree* tree,
                       const std::string & algorithmfile,
                       const std::string & parameterfile
                       );
    ///< Data Constructor.
    ///< \param ROOT_outfile_name   Name of the output ROOT file.
    ///< \param tree        tree with testbeam data
    ///< \param algorithmfile Algorithm file, specification of algorithm
    ///< \param parameterfile Parameter file with chamber geometry

// Methods //
    void initialize(const std::string & ROOT_outfile_name,
                    const std::string & algorithmfile,
                    const std::string & parameterfile);
    ///< Initialize Method, used in both constructors.
    ///< \param ROOT_outfile_name   Name of the output ROOT file.
    ///< \param algorithmfile Algorithm file, specification of algorithm
    ///< \param parameterfile Parameter file with chamber geometry

    void store_simulation(const std::string & output_path);
    ///< Produce ROOT file with tree to store simulation results
    ///< \param output_path

    void read_simulationfile(const std::string & simulationfile);
    ///< Read the simulation file specifying the simulation parameters.
    ///< \param simulationfile   Path to the simulation file

    void read_algorithmfile(const std::string & algorithmfile);
    ///< Read the algorithm file specifying the algorithm.
    ///< \param algorithmfile   Path to the algorithm file

    MDTChamberParameters read_parameterfile(const std::string & parameterfile);
    ///< Read the parameter file specifying the chamber geometry.
    ///< \param parameterfile   Path to the parameter file

    MDTTriggerEvent generate_event(const double & ang_res,
                                   const double & t_resolution);
    ///< Generate event with monte carlo routine
    ///< \param ang_res     Angular resolution of the L0.
    ///< \param t_resolution    Time resolution of the drift time measurement.

    MDTTriggerEvent read_event(const unsigned int & entry,
                               const double & ang_res,
                               const double & ROI_width);
    ///< read event from tree
    ///< \param entry     Entry in tree, specifies event retrieved
    ///< \param ang_res     Angular resolution of the L0.
    ///< \param ROI_width   Region of interest width in mm.



    void analyse(const unsigned int & nb_tracks,
                 const double & ang_res,
                 const double & t_resolution);
    ///< Run the analysis based on the requested number of tracks.
    ///< \param nb_tracks   Number of tracks to be analysed.
    ///< \param ang_res     Angular resolution of the L0.
    ///< \param t_resolution    Time resolution of the drift time measurement.



private:
    // Helper functions //
    std::string trim(std::string const& source, char const* delims);
    ///< Helper function to trim a string at a certain delimiter
    ///< \param source    String that is to be trimmed
    ///< \param delims    List of delimiting characters (where to trim source)

    std::map<std::string, std::vector<std::string> > read_file(\
                                                  const std::string & filename);
    ///< General Function to read in a config file in a certain format
    ///< and store keys and values in a map.
    ///< Structure of map: key - vector[values], comments with #
    ///< \param filename  path to text file to be read in

    void initialize_ROOTObjects(const std::string & ROOT_outfile_name);
    ///< Helper function taking care of setting up ROOT stuff
    ///< \param ROOT_outfile_name   Name of the output ROOT file.


    std::vector<std::string> access_map(std::map<std::string, \
                       std::vector<std::string> > m, std::string key);
    ///< Helper function to easily get values from map in desired format
    ///< \param m     map holding parameters from file
    ///< \param key   key whose value should be retrieved from map


    // Event Generator //
    MDTChamberEventGenerator *m_p_evnt_generator;
    TRandom3 *m_p_rnd;

    // Store Simulation Output //
    bool m_store_simulation_output;
    TFile* m_simulation_output;
    TTree* m_simulation_tree;
    double        m_sim_std_m;
    double        m_sim_std_b;
    double        m_sim_std_L0;
    unsigned int m_sim_std_has_track;
    std::vector<double>  m_sim_FRO_time;
    std::vector<double>  m_sim_FRO_radius;
    std::vector<double>  m_sim_FRO_wy;
    std::vector<double>  m_sim_FRO_wz;
    std::vector<unsigned int>  m_sim_tube;
    std::vector<unsigned int>  m_sim_layer;
    std::vector<unsigned int>  m_sim_multilayer;
    unsigned int m_sim_dummy_int;
    double m_sim_dummy_double;
    std::vector<double> m_sim_dummy_vector_double;
    std::vector<unsigned int> m_sim_dummy_vector_int;

    // Store Data Input //
    double        m_data_std_m;
    double        m_data_std_b;
    unsigned int m_data_std_has_track;
    std::vector<double>  m_data_FRO_time;
    std::vector<double>  m_data_FRO_wy;
    std::vector<double>  m_data_FRO_wz;
    std::vector<unsigned int>  m_data_tube;
    std::vector<unsigned int>  m_data_layer;
    std::vector<unsigned int>  m_data_multilayer;
    unsigned int m_data_std_nb_hits;
    unsigned int m_data_FRO_nb_hits;
    std::vector<unsigned int>  m_data_std_channel;
    std::vector<unsigned int>  m_data_FRO_channel;
    std::vector<double>  m_data_std_t;
    std::vector<double>  m_data_std_r;
    std::vector<double>  m_data_std_res;
    std::vector<double>  m_data_std_wy;
    std::vector<double>  m_data_std_wz;
    unsigned int  m_data_std_nb_track_hits;
    double        m_data_std_CL;
    std::vector<double>  m_data_std_d;
    std::vector<double>  m_data_FRO_d;
    std::vector<double>  m_data_FRO_r;


    // Parameters for MC event generation //
    std::string m_rootfile_name;
    std::string m_hist_b_name;
    std::string m_hist_m_name;

    bool m_use_b_weight;
    bool m_use_m_weight;
    bool m_use_L0_weight;

    TH1F* m_b_distribution;
    TH1F* m_m_distribution;

    double m_b_gen_min;
    double m_b_gen_max;
    double m_m_gen_min;
    double m_m_gen_max;
    double m_occupancy;

    // Algorithms //
    // 0.) Histogram-based Pattern Recognition
    MDTL1TriggerAlgorithm *m_p_trigger_alg;
    bool m_algorithm_Hist;
    double m_algorithm_Hist_bin_width;
    double m_algorithm_Hist_time;
    bool m_algorithm_Hist_single_track;

    // 1.) 2-D Binned Hough Transform
    MDTL1Hough2DTriggerAlgorithm *m_p_trigger_alg_2D;
    bool m_algorithm_2DHough;
    double m_algorithm_2DHough_bin_width;
    unsigned int m_algorithm_2DHough_nb_binned_algs;
    double m_algorithm_2DHough_distance_binned_algs;
    double m_algorithm_2DHough_time;

    // 2.) Fast Track Finder (Fast Algorithm)
    MDTL1FastTriggerAlgorithm *m_p_trigger_alg_semi;
    bool m_algorithm_Fast;
    double m_algorithm_Fast_b_width;
    double m_algorithm_Fast_m_width;
    double m_algorithm_Fast_search_width;
    bool m_algorithm_Fast_time;

    // 3.) Very Fast Track Finder (Fast Algorithm without fitting)
    MDTL1VeryFastTriggerAlgorithm *m_p_trigger_alg_fast_semi;
    bool m_algorithm_VeryFast;
    double m_algorithm_VeryFast_b_width;
    double m_algorithm_VeryFast_m_width;
    bool m_algorithm_VeryFast_time;

    // Output File //
    TFile *m_p_tfile; // ROOT output file

    // Input Tree //
    TTree* m_tree;
    TreeReader* tr;

    // Chamber Parameters //
    MDTChamberParameters m_par;

    // Rt relation //
    std::string m_rt_file_name;
    Rt_relation m_rt;

    // Tree and Tree Variables //
    TTree *m_p_tree; // tree with analysis results

    double m_gen; // generated track slope
    double b_gen; // generated track intercept
    double m_L0; // L0 slope

    // 0.) Histogram-based Pattern Recognition
    int a0_nb_candidates; // number of segment candidates
    std::vector<int> a0_nb_hits; // number of hits on the segment
    std::vector<double> a0_chi2; // chi^2 values of the reconstructed tracks
    std::vector<double> a0_m_rec; // reconstructed track slopes
    std::vector<double> a0_b_rec; // reconstructed track slopes
    std::vector<double> a0_m_rec_err; // estimated track slope error
    double a0_reco_time; // time for algorithm in microseconds

    // 1.) 2-D Binned Hough Transform
    int a1_nb_candidates; // number of segment candidates
    std::vector<int> a1_nb_hits; // number of hits on the segment
    std::vector<double> a1_chi2; // chi^2 values of the reconstructed tracks
    std::vector<double> a1_m_rec; // reconstructed track slopes
    std::vector<double> a1_b_rec; // reconstructed track slopes
    std::vector<double> a1_m_rec_err; // estimated track slope error
    double a1_reco_time; // time for algorithm in microseconds

    // 2.) Fast Track Finder (Fast Algorithm)
    int a2_nb_candidates; // number of segment candidates
    std::vector<int> a2_nb_hits; // number of hits on the segment
    std::vector<double> a2_chi2; // chi^2 values of the reconstructed tracks
    std::vector<double> a2_m_rec; // reconstructed track slopes
    std::vector<double> a2_b_rec; // reconstructed track slopes
    std::vector<double> a2_m_rec_err; // estimated track slope error
    double a2_reco_time; // time for algorithm in microseconds

    // 3.) Very Fast Track Finder (Fast Algorithm without fitting)
    int a3_nb_candidates; // number of segment candidates
    std::vector<int> a3_nb_hits; // number of hits on the segment
    std::vector<double> a3_chi2; // chi^2 values of the reconstructed tracks
    std::vector<double> a3_m_rec; // reconstructed track slopes
    std::vector<double> a3_b_rec; // reconstructed track slopes
    std::vector<double> a3_m_rec_err; // estimated track slope error
    double a3_reco_time; // time for algorithm in microseconds

};

}

#endif
