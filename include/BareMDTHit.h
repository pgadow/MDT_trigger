//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_BareMDTHitH
#define MDTTrigger_BareMDTHitH

//::::::::::::::::::::::
//:: CLASS BareMDTHit ::
//::::::::::::::::::::::

/// \class BareMDTHit
///
/// MDT hit information, wire position, drift time
///
/// \date 24.10.2013
///
/// \author Oliver.Kortner@CERN.CH

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// CLHEP //
#include "CLHEP/Vector/ThreeVector.h"

// mt-offline //
#include "Rt_relation.hxx"

namespace MDTTrigger {

class BareMDTHit {

public:
// Constructors //
    BareMDTHit(void);
    ///< Default constructor.
    ///< Empty. Only there to allow use in STL vectors.

    BareMDTHit(const CLHEP::Hep3Vector & wire_pos, const double & t_drift);
    ///< Constructor.
    ///< \param wire_pos    Wire position.
    ///< \param t_drift     Drift time (after t0 subtraction).

    BareMDTHit(const CLHEP::Hep3Vector & wire_pos, const double & t_drift,\
               const int & multilayer, const int & layer, const int & tube);
    ///< Constructor.
    ///< \param wire_pos    Wire position.
    ///< \param t_drift     Drift time (after t0 subtraction).
    ///< \param multilayer  multilayer index
    ///< \param layer       layer index
    ///< \param tube        tube index


    BareMDTHit(const CLHEP::Hep3Vector & wire_pos, const double & t_drift,\
               const Rt_relation & rt);
    ///< Constructor.
    ///< \param wire_pos    Wire position.
    ///< \param t_drift     Drift time (after t0 subtraction).
    ///< \param rt          r-t relation

    BareMDTHit(const CLHEP::Hep3Vector & wire_pos, const double & t_drift,\
               const Rt_relation & rt,\
               const int & multilayer, const int & layer, const int & tube);
    ///< Constructor.
    ///< \param wire_pos    Wire position.
    ///< \param t_drift     Drift time (after t0 subtraction).
    ///< \param rt          r-t relation
    ///< \param multilayer  multilayer index
    ///< \param layer       layer index
    ///< \param tube        tube index


// Methods //
    const CLHEP::Hep3Vector & getWirePosition(void) const;
    ///< Get the wire position.

    double getDriftTime(void) const;
    ///< Get the drift time.

    double getDriftRadius(void) const;
    ///< Get the drift radius.

    int getMultiLayer(void) const;
    ///< Get the multilayer index the hit belongs to.

    int getLayer(void) const;
    ///< Get the layer index the hit belongs to.

    int getTube(void) const;
    ///< Get the tube index the hit belongs to.

    void setRtRelation(const Rt_relation & rt);
    ///< Calculate the drift radius with given Rt relation.
    ///< \param rt          r-t relation

    void setMultiLayer(int multilayer);
    ///< Set the multilayer index the hit belongs to.
    ///< \param multilayer  multilayer index

    void setLayer(int layer);
    ///< Set the layer index the hit belongs to.
    ///< \param layer  layer index

    void setTube(int tube);
    ///< Set the tube index the hit belongs to.
    ///< \param tube  tube index

private:
    CLHEP::Hep3Vector m_w_pos; // wire position
    double m_t_drift; // drift time
    double m_r_drift; // drift radius

    int m_multilayer; // index of multilayer the hit belongs to
    int m_layer; // index of layer in multilayer the hit belongs to
    int m_tube; // index of tube in layer the hit belongs to

};

}

#endif
