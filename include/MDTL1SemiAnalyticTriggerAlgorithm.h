//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 25.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_MDTL1SemiAnalyticTriggerAlgorithmH
#define MDTTrigger_MDTL1SemiAnalyticTriggerAlgorithmH

//:::::::::::::::::::::::::::::::::::::::::::::
//:: CLASS MDTL1SemiAnalyticTriggerAlgorithm ::
//:::::::::::::::::::::::::::::::::::::::::::::

/// \class MDTL1SemiAnalyticTriggerAlgorithm
///
/// This class holds an implementation of the Semi-Analytic L1 MDT
/// trigger algorithm, which is based on an idea of Oliver Kortner.
///
/// \date 19.02.2016
///
/// \author Paul.Philipp.Gadow@cern.ch

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// CLHEP //
#include "CLHEP/Vector/ThreeVector.h"

// standard C++ //
#include <vector>

// MDTTrigger //
#include "BareMDTHit.h"
#include "SegmentCandidate.h"
#include "straight_line.hxx"
#include "MDTChamberParameters.h"

// ROOT //
#include "TH1.h"
#include "TH2.h"

namespace MDTTrigger {

class MDTL1SemiAnalyticTriggerAlgorithm {

public:
// Constructors //
    MDTL1SemiAnalyticTriggerAlgorithm(\
                          const double & m_min,
                          const double & m_max,
                          const double & m_bin_width,
                          const double & b_min,
                          const double & b_max,
                          const double & b_bin_width);
    ///< Constructor.
    ///< \param m_min   Minimum muon slope in y-z plane.
    ///< \param m_max   Maximum muon slope in y-z plane.
    ///< \param m_bin_width   Bin size for slope hist.
    ///< \param b_min   Minimum hit position along the y axis.
    ///< \param b_max   Maximum hit position along the y axis.
    ///< \param b_bin_width   Bin size for hit position hist.

// Methods //
    const std::vector<SegmentCandidate> & getSegmentCandidates(
                        const std::vector<BareMDTHit> & hits,
                        const Straight_line & L0_track,
                        const double & L0_m_accuracy,
                        const double & L0_b_accuracy,
                        const double & t_accuracy,
                        const Rt_relation & rt);
    ///< Run the trigger algorithm.
    ///< \param hits          MDT hits to be used in the algorithm.
    ///< \param L0_track      Seed track found by RPC/TGC detector
    ///< \param L0_m_accuracy      Seed track slope resolution
    ///< \param L0_b_accuracy      Seed track position resolution
    ///< \param t_accuracy    Time resolution of electronics
    ///< \param rt            R-t relation.


private:
    std::vector<double> calculateTangent(double r1, double r2, double L2,\
                                        unsigned int configuration);
    ///< Calculate the tangent from two drift radii and their radial distance
    ///< \param r1  drift radius of first hit
    ///< \param r2  drift radius of second hit
    ///< \param L2   distance squared between the wire positions
    ///< \param configuration which of 4 possible tangents

    std::vector<CLHEP::Hep3Vector> calculateHits(double r1, double r2, \
                                  CLHEP::Hep3Vector y_prime,\
                                  CLHEP::Hep3Vector wp1, CLHEP::Hep3Vector wp2,\
                                  unsigned int configuration);
    ///< Calculate hit positions from
    ///< \param r1       drift radius of first hit
    ///< \param r2       drift radius of second hit
    ///< \param y_prime  axis of rotated coordinate system orthogonal to track
    ///< \param wp1      wire position of first wire
    ///< \param wp2      wire position of second wire
    ///< \param configuration which of 4 possible tangents


    std::vector<BareMDTHit> patternRecognition(\
                                          const std::vector<BareMDTHit> & hits);
    ///< Perform pattern recognition on bare MDT hits
    ///< \param hits MDT hits for pattern recognition


    double m_parameter;

    TH1D *m_track_hist_m; // histogram for collecting the slopes
                          // and finding the most common slope
    TH1D *m_track_hist_b; // histogram for collecting the hit positions
                          // along the y axis


    TH2D *m_track_hist_mb; // 2D histogram for finding maximum in m, b space

    std::vector<SegmentCandidate> m_segment; // reconstruced segments

};

}

#endif
