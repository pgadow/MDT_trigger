//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 01./06.04.1999, AUTHOR: OLIVER KORTNER
// Modified: 06.06.2012 by O. Kortner, major clean-up for MDT trigger studies.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

////////////////////
// CLASS MDT_tube //
////////////////////

//////////////////////////////////////////////
// THIS CLASS DEFINES AN MDT DETECTOR TUBE. //
//////////////////////////////////////////////

#ifndef MDTtubeHXX
#define MDTtubeHXX

//////////////////////////
// INCLUDE HEADER FILES //
//////////////////////////

// ROOT //
#include "TVector3.h"

// standard C++ libraries //
#include <iostream>
#include <fstream>

// STL //
#include <string>

// MDT trigger //
#include "Rt_relation.hxx"
#include "Straight_line.hxx"

//units: mm, ns

class MDT_tube {

public:
// Constructors
    MDT_tube(void) : propagation_direction(1.0)
        {
        r_inner = 14.6275;
        r_outer = 15.0175;
        }                // default constructor
    MDT_tube(const MDT_tube & tb) 
        {
        copy(tb);
        }                // copy constructor
        
// Methods
// MDT specific get-methods //
    void copy(const MDT_tube & tb); // copy routine
    TVector3 end_point_1(void) const { return left_end_point; }
                        // get left end point of the MDT
    TVector3 end_point_2(void) const { return right_end_point; }
                        // get right end point of the
                        // MDT
    double inner_radius(void) const { return r_inner; }
                        // get inner tube radius [mm]
    double outer_radius(void) const { return r_outer; }
                        // get outer tube radius [mm]
    TVector3 wire_end_1(void) const { return wire_left_end_point; }
                        // get left wire end point
    TVector3 wire_end_2(void) const {return wire_right_end_point;}
                        // get right wire end point
    Rt_relation & Rt_relation(void) { return rt; }
                        // get the r-t relation of the
                        // tube
    Rt_relation rt_rel(void) const { return rt; }
                        // get the r-t relation of the
                        // tube
    double t0(void) const { return t_0; }
                        // get t0 [ns]
    double t_cal(void) const { return t_conv; }
                        // get the conversion factor for
                        // TDC channel -> ns,
                        // t_cal < 0: COMMON STOP MODE,
                        // t_cal > 0: COMMON START MODE
    int hit_type(void) const { return hit;}
                        // get hit type, classification:
                        // see above
    double guessed_hit_dist(void) const { return guessed_dist; }
                        // get guessed distance of a
                        // hit from the readout edge
    int TDC_count(void) const { return TDC_value; }
                        // get the (raw) TDC count
    double r(void) const { return r_drift; }
                        // get the drift radius of the
                        // tube hit
    double t(void) const { return t_drift; }
                        // get the drift time of the
                        // tube hit
    double error(void) const { return r_err; }
                        // get the error on the drift
                        // radius
    double distance_from_wire(const Straight_line & track) const;
                        // get the distance of the
                        // straight line track from
                        // the signal wire
    double signed_distance_from_wire(
                    const Straight_line & track) const;
                        // get signed distance of the
                        // straight line track from
                        // the signal wire
    double distance_from_axis(const Straight_line & track) const;
                        // get the distance of the
                        // straight line track from the
                        // symmetry axis of the tube
// MDT specific set-methods //
    void set_propagation_direction(const double &dir);
                        //dir >0: signal propagation 
                        //      direction is +x
                        //dir<0: signal propagation
                        //    direction is -x
                        //dir=0: signal propagation is
                        //    ignored
    void set_end_point_1(const TVector3 & end_pnt);
                        // set left MDT end point =
                        // end_pnt
    void set_end_point_2(const TVector3 & end_pnt);
                        // set right MDT end point =
                        // end_pnt
    void set_inner_radius(const double & r);
                        // set inner tube radius = r[mm]
    void set_outer_radius(const double & r);
                        // set outer tube radius = r[mm]
    void set_wire_end_1(const TVector3 & end_pnt);
                        // set left wire end point =
                        // end_pnt
    void set_wire_end_2(const TVector3 & end_pnt);
                        // set right wire end point =
                        // end_pnt
    void set_t0(const double & t_zero);
                        // set t0 = t_zero [ns]
    void set_t_cal(const double & t_con);
                        // set t_cal = r_conv
                        // [ns/TDC channel],
                        // t_con < 0: COMMON STOP MODE,
                        // t_con > 0: COMMON START MODE
    void set_hit_type(const int & tp);
                        // set hit type = tp,
                        // classification: see above
    void set_guessed_hit_dist(const double & guess);
                        // set guessed hit distance =
                        // guess
    void set_hit_data(const MDT_tube & MDT);
                        // set hit data according to
                        // the hit data of the tube MDT
    void set_Rt_relation(const Rt_relation & new_rt);
                        // set the r-t relation of the
                        // tube = new_rt

private:
    TVector3 left_end_point, right_end_point; // left and right end points
                                              // of the cylindrical MDT
    double r_inner, r_outer; // inner and outer radius of the
                             // MDT
    TVector3 wire_left_end_point, wire_right_end_point; // end points of
                                                        // the signal wire
    Rt_relation rt; // r-t relation of the tube
    double t_0; // t0
    double propagation_direction;
            //gives the direction of the signal propagation
            //1 = +x; -1 = -x; 0 = signal propagation is ignored
    double t_conv; // conversion factor from TDC channel to ns;
                   // convention:
                   //   t_conv < 0: COMMON STOP MODE,
                   //   t_conv > 0: COMMON START MODE
    int hit; // hit = 0, if the tube has not been hit,
            //     = 2, if the tube has been hit and the hit was
            //          detected
    double guessed_dist; // guessed distance of a hit from the readout
                         // edge
    int TDC_value; // TDC count
    double r_drift; // hit drift radius
    double t_drift; // hit drift time
    double r_err;   // error on r(t)

};

#endif
