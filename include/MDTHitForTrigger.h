//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_MDTHitForTriggerH
#define MDTTrigger_MDTHitForTriggerH

//::::::::::::::::::::::::::::
//:: CLASS MDTHitForTrigger ::
//::::::::::::::::::::::::::::

/// \class MDTHitForTrigger
///
/// This class stores the MDT hit information for the MDT trigger algorithm.
///
/// \date 24.10.2013
///
/// \author Oliver.Kortner@CERN.CH

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// mt-offline //
#include "Rt_relation.hxx"
#include "straight_line.hxx"

// MDTTrigger //
#include "BareMDTHit.h"

namespace MDTTrigger {

class MDTHitForTrigger {

public:
// Constructors //
    MDTHitForTrigger(void);
    ///< Default constructor.
    ///< Empty. Only there to allow use in STL vectors.

    MDTHitForTrigger(const BareMDTHit & bare_hit,
                     const Rt_relation & rt,
                     const CLHEP::Hep3Vector & origin,
                     const CLHEP::Hep3Vector & y,
                     const CLHEP::Hep3Vector & z);
    ///< Constructor.
    ///< \param bare_hit    Bare MDT hit.
    ///< \param rt          r-t relationship.
    ///< \param origin      Origin of the track coordinate system.
    ///< \param y           y coordinate of the track coordinate system.
    ///< \param z           z coordinate of the track coordinate system (track
    ///<                    direction).

// Methods //
    const CLHEP::Hep3Vector & getHitPosition1(void) const;
    ///< Get the 1st possible hit position in the track coordinate frame.

    const CLHEP::Hep3Vector & getHitPosition2(void) const;
    ///< Get the 2nd possible hit position in the track coordinate frame.

private:
    CLHEP::Hep3Vector m_hit_1; // 1st possible hit position in the track coordinate
                        // system
    CLHEP::Hep3Vector m_hit_2; // 2nd possible hit position in the track coordinate
                        // system

};

}

#endif
