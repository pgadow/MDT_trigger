//++++++++++++++++++++++++++++++++++++++
// 08.02.2016, AUTHOR: PHILIPP GADOW
//++++++++++++++++++++++++++++++++++++++

////////////////////////////////////////////////////////////////////
// CLASS ConfigReader
////////////////////////////////////////////////////////////////////

// \class ConfigReader
// Class which can read configuration text files in which chamber
// geometry and background occupancy are specified and applies
// the configuration to event generator
////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>

namespace MDTTrigger {

class ConfigReader{
public:
// Constructor
  ConfigReader();

// Methods //
  void read_config();

}
}
