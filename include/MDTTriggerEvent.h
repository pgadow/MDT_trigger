// standard C++ libraries //
#include <vector>

// mt-offline //
#include "straight_line.hxx"
// MDTTrigger //
#include "BareMDTHit.h"

//++++++++++++++++++++++++++++++++++
#ifndef MDTTrigger_MDTTriggerEventH
#define MDTTrigger_MDTTriggerEventH
struct MDTTriggerEvent{
  Straight_line muon_track;
  Straight_line L0_track;
  std::vector<MDTTrigger::BareMDTHit> hit_pattern;
};
#endif
