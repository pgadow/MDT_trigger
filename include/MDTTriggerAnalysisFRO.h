//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_MDTTriggerAnalysisFROH
#define MDTTrigger_MDTTriggerAnalysisFROH

//::::::::::::::::::::::::::::::
//:: CLASS MDTTriggerAnalysisFRO ::
//::::::::::::::::::::::::::::::

/// \class MDTTriggerAnalysisFRO
///
/// This class is used to study the performance of the MDT L1 trigger algorithm.
///
/// \date 29.10.2013
///
/// \author Oliver.Kortner@CERN.CH

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <string>

// ROOT //
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TRandom3.h"

// mt-offline //
#include "straight_line.hxx"

// MDTTrigger //
#include "MDTChamberFROEventGenerator.h"
#include "MDTL1TriggerAlgorithm.h"

namespace MDTTrigger {

class MDTTriggerAnalysisFRO {

public:
// Constructors //
    MDTTriggerAnalysisFRO(const std::string & ROOT_outfile_name,
                       const double & bin_width);
    ///< Constructor.
    ///< \param ROOT_outfile_name   Name of the output ROOT file.
    ///< \param bin_width   Width of the bins used by the trigger algorithm.
    
// Methods //
    void analyse(const unsigned int & nb_tracks,
                 const double & b_gen_min,
                 const double & b_gen_max,
                 const double & m_gen_min,
                 const double & m_gen_max,
                 const double & occupancy,
                 const double & ang_res,
                 const double & t_resolution);
    ///< Run the analysis based on the requested number of tracks.
    ///< \param nb_tracks   Number of tracks to be generated.
    ///< \param b_gen_min   Minimum track intercept.
    ///< \param b_gen_max   Maximum track intercept.
    ///< \param m_gen_min   Minimum track slope.
    ///< \param m_gen_max   Maximum track slope.
    ///< \param occupancy   Occupancy.
    ///< \param ang_res     Angular resolution of the L0.
    ///< \param t_resolution    Time resolution of the drift time measurement.

private:
    MDTChamberFROEventGenerator *m_p_evnt_generator;

    MDTL1TriggerAlgorithm *m_p_trigger_alg;

    TRandom3 *m_p_rnd;

    TFile *m_p_tfile; // ROOT output file

// tree and tree variables //
    TTree *m_p_tree; // tree with analysis results

    double m_gen; // generated track slope
    double b_gen; // generated track intercept

    int nb_muon_hits_on_track; // number of muon hits in the standard read-out

    double m_L0; // L0 slope

    int nb_candidates; // number of segment candidates
    std::vector<int> nb_hits; // number of hits on the segment
    std::vector<double> chi2; // chi^2 values of the reconstructed tracks
    std::vector<double> m_rec; // reconstructed track slopes
    std::vector<double> b_rec; // reconstructed track slopes
    std::vector<double> m_rec_err; // estimated track slope error

};

}

#endif
