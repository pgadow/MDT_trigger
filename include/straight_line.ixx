//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 22.12.1998, AUTHOR: OLIVER KORTNER
// Modified: 11.01.1999: METHOD sign_dist_from corrected.
//           03.02.1999, method dist_from_line for points added. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//////////////////////////////////////////////////////////////////////
// IMPLEMENTATION OF THE METHODS DEFINED IN THE CLASS Straight_line //
//////////////////////////////////////////////////////////////////////

//*****************************************************************************

/////////////////////////////
// METHOD print_parameters //
/////////////////////////////

inline void Straight_line::print_parameters(void) const {

	std::cout << "position vector: " 
		<< "(" << position.x()
		<< "," << position.y()
		<< "," << position.z()
		<< ")" << std::endl;
	std::cout << "direction vector: " 
		<< "(" << direction.x()
		<< "," << direction.y()
		<< "," << direction.z()
		<< ")" << std::endl;

	return;

}

//*****************************************************************************

//////////////////////////
// METHOD point_on_line //
//////////////////////////

inline CLHEP::Hep3Vector Straight_line::point_on_line(const double lambda) const {

/////////////////////////
// AUXILIARY VARIABLES //
/////////////////////////

	CLHEP::Hep3Vector p;

/////////////
// PROGRAM //
/////////////

	p = position + lambda*direction;

	return p;

}

//*****************************************************************************

///////////////////////////
// METHOD sign_dist_from //
///////////////////////////

inline double Straight_line::sign_dist_from(const Straight_line & h) const {

/////////////////////////
// AUXILIARY VARIABLES //
/////////////////////////

	CLHEP::Hep3Vector a = position, u = direction;
					// position and direction vectors of g
	CLHEP::Hep3Vector b = h.position_vector(), v = h.direction_vector();
					// position and direction vectors of h
	CLHEP::Hep3Vector n; // normal vector of plane spanned by g and h
	CLHEP::Hep3Vector d; // distance vector

/////////////
// PROGRAM //
/////////////

////////////////////////
// collinearity check //
////////////////////////

	n = u.cross(v);
	d = a-b;
	if (n*n == 0.0) {
		// straight lines are parallel
		// no sign given
		return sqrt(d*d-(u.unit()*d)*(u.unit()*d));
	}
	
	return (d*(n.unit()));

}

//*****************************************************************************

///////////////////////////
// METHOD dist_from_line //
///////////////////////////

inline double Straight_line::dist_from_line(const CLHEP::Hep3Vector & point) const {

/////////////////////////
// AUXILIARY VARIABLES //
/////////////////////////

	CLHEP::Hep3Vector u = direction;

////////////////////////
// CALCULATE DISTANCE //
////////////////////////

	return sqrt((point-position)*(point-position) -
		((point-position)*u.unit())*((point-position)*u.unit()));

}

//*****************************************************************************

///////////////
// METHOD mx //
///////////////

inline double Straight_line::mx(void) const {

	if (direction.z() == 0.0) {
		return 0.0;
	}
	return direction.x()/direction.z();

}

//*****************************************************************************

///////////////
// METHOD bx //
///////////////

inline double Straight_line::bx(void) const {

	if (direction.z() == 0.0) {
		std::cerr << "b_x not uniquely defined." << std::endl;
		return position.x();
	}
	return (position.x() - position.z()*direction.x()/direction.z());
					
}

//*****************************************************************************

///////////////
// METHOD my //
///////////////

inline double Straight_line::my(void) const {

	if (direction.z() == 0.0) {
		return 0.0;
	}
	return direction.y()/direction.z();

}

//*****************************************************************************

///////////////
// METHOD by //
///////////////

inline double Straight_line::by(void) const {

	if (direction.z() == 0.0) {
		std::cerr << "b_y not uniquely defined." << std::endl;
		return position.y();
	}
	return (position.y() - position.z()*direction.y()/direction.z());

}

//*****************************************************************************

/////////////////////////
// METHOD set_position //
/////////////////////////

inline void Straight_line::set_position(const CLHEP::Hep3Vector & new_pos) {

	position = new_pos;
	return;

}

//*****************************************************************************

//////////////////////////
// METHOD set_direction //
//////////////////////////

inline void Straight_line::set_direction(const CLHEP::Hep3Vector & new_dir) {

	direction = new_dir;
	return;

}

//*****************************************************************************

////////////////////////////////////
// METHOD intersection_with_plane //
////////////////////////////////////

inline bool Straight_line::intersection_with_plane(const CLHEP::Hep3Vector &norm, const CLHEP::Hep3Vector &point,
			CLHEP::Hep3Vector & intersection) const
	{
//check if plane || line
	double nd;
	if((nd=norm*direction)==0) return false;
//calculate line parameter for intersection point
	double lambda=(norm*point-norm*position)/nd;
//Get intersection Point
	intersection=position+lambda*direction;		
	return true;
	}
