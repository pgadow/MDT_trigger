//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_ExtendedHistogramH
#define MDTTrigger_ExtendedHistogramH

//:::::::::::::::::::::::::::::
//:: CLASS ExtendedHistogram ::
//:::::::::::::::::::::::::::::

/// \class ExtendedHistogram
///
/// This class is an extension to a one-dimensional ROOT histogram which allows
/// the user to keep a pointer to the object creating a certain entry of the
/// ROOT histogram.
///
/// \date 24.10.2013
///
/// \author Oliver.Kortner@CERN.CH

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// ROOT //
#include "TH1F.h"

// standard C++ //
#include <vector>
#include <string>

//::::::::::::::::::::::::::::::::
//:: TEMPLATE CLASS DECLARATION ::
//::::::::::::::::::::::::::::::::

namespace MDTTrigger {

template <class EntryProvider>

class ExtendedHistogram : public TH1F {

public:
// Constructors //
    ExtendedHistogram(const std::string & hist_name,
                      const std::string & hist_title,
                      const unsigned int & nb_bins,
                      const double & x_min,
                      const double & x_max);
    ///< Constructor.
    ///< \param hist_name   Name of the histogram.
    ///< \param hist_title  Title of the histogram.
    ///< \param nb_bins     Number of bins.
    ///< \param x_min       Lower boundary of the histogram.
    ///< \param x_max       Upper boundary of the histogram.

// Methods //
// get-methods //
    const std::vector<const EntryProvider *> getEntryProvider(
                                                const int & bin_number) const;
    ///< Get a vector of pointers to the Hep3Vectors of the bin bin_number.

// set-methods //
    void Fill(const double & x, const double & w, const EntryProvider * ep);
    ///< Fill the ROOT histogram a position x with the weight w and store
    ///< a pointer to the corresponding entry provider.

    void Clear(void);
    ///< Clear the histogram.

private:
    std::vector< std::vector<const EntryProvider *> > m_ep;

};

}

#endif
