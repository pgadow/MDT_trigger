//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_DriftRadiusH
#define MDTTrigger_DriftRadiusH

//:::::::::::::::::::::::
//:: CLASS DriftRadius ::
//:::::::::::::::::::::::

/// \class DriftRadius
///
/// MDT hit information, i.e. wire position, drift time, drift radius.
///
/// \date 24.10.2013
///
/// \author Oliver.Kortner@CERN.CH

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::
