//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 25.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef MDTTrigger_MDTL1Hough2DTriggerAlgorithmH
#define MDTTrigger_MDTL1Hough2DTriggerAlgorithmH

//::::::::::::::::::::::::::::::::::::::::
//:: CLASS MDTL1Hough2DTriggerAlgorithm ::
//::::::::::::::::::::::::::::::::::::::::

/// \class MDTL1Hough2DTriggerAlgorithm
///
/// This class holds an implementation of the L1 MDT trigger algorithm.
///
/// \date 25.10.2013
///
/// \author Oliver.Kortner@CERN.CH

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// CLHEP //
#include "CLHEP/Vector/ThreeVector.h"

// standard C++ //
#include <vector>

// MDTTrigger //
#include "ExtendedHistogram.h"
#include "MDTHitForTrigger.h"
#include "SegmentCandidate.h"

namespace MDTTrigger {

class MDTL1Hough2DTriggerAlgorithm {

public:
// Constructors //
    MDTL1Hough2DTriggerAlgorithm(const double & y_min,
                          const double & y_max,
                          const double & bin_width);
    ///< Constructor.
    ///< \param y_min   Minimum hit position along the y axis.
    ///< \param y_max   Maximum hit position along the y axis.
    ///< \param bin_width   Bin size.

// Methods //
    const std::vector<SegmentCandidate> & getSegmentCandidates(
                        const std::vector<BareMDTHit> & hits,
                        const Straight_line & L0_track,
                        const double & t_accuracy,
                        const Rt_relation & rt);
    ///< Run the trigger algorithm.
    ///< \param hits    MDT hits to be used in the algorithm.
    ///< \param L0_track    Straight line containing the track incidence angle
    ///<                    determined at L0.
    ///< \param rt  r-t relationship.

private:
    ExtendedHistogram *m_track_hist_1; // first tracking histogram
    ExtendedHistogram *m_track_hist_2; // second tracking histogram shifted
                                       // by half a bin

    std::vector<SegmentCandidate> m_segment; // reconstruced segments

};

}

#endif
