//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                         MDTHitForTrigger                        ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>

// MDTTrigger //
#include "MDTHitForTrigger.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;
using namespace CLHEP;

//*****************************************************************************

//:::::::::::::::::::::::::
//:: DEFAULT CONSTRUCTOR ::
//:::::::::::::::::::::::::

MDTHitForTrigger::MDTHitForTrigger(void) {
}

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

MDTHitForTrigger::MDTHitForTrigger(const BareMDTHit & bare_hit,
                     const Rt_relation & rt,
                     const Hep3Vector & origin,
                     const Hep3Vector & y,
                     const Hep3Vector & z) {

// hit positions in the original coordinate system //
    Hep3Vector pos1 = bare_hit.getWirePosition()-
                                    rt.r(bare_hit.getDriftTime(), 0)*y.unit();
    Hep3Vector pos2 = bare_hit.getWirePosition()+
                                    rt.r(bare_hit.getDriftTime(), 0)*y.unit();

// hit positions in the track coordinate frame //
    m_hit_1 = Hep3Vector(pos1.x(),
                         (pos1-origin)*y.unit(),
                         (pos1-origin)*z.unit());
    m_hit_2 = Hep3Vector(pos2.x(),
                         (pos2-origin)*y.unit(),
                         (pos2-origin)*z.unit());

}

//*****************************************************************************

//::::::::::::::::::::::::::::
//:: METHOD getHitPosition1 ::
//::::::::::::::::::::::::::::

const Hep3Vector & MDTHitForTrigger::getHitPosition1(void) const {

    return m_hit_1;

}

//*****************************************************************************

//::::::::::::::::::::::::::::
//:: METHOD getHitPosition2 ::
//::::::::::::::::::::::::::::

const Hep3Vector & MDTHitForTrigger::getHitPosition2(void) const {

    return m_hit_2;

}
