//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 25.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                     MDTChamberFROEventGenerator                    ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>

// MDTTrigger //
#include "MDTChamberFROEventGenerator.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;
using namespace CLHEP;

//*****************************************************************************

//:::::::::::::::::::::::::
//:: DEFAULT CONSTRUCTOR ::
//:::::::::::::::::::::::::

MDTChamberFROEventGenerator::MDTChamberFROEventGenerator(void) {

    init(15.0, 0.4, 36, 3, 121.0, "share/default.rt");

}

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

MDTChamberFROEventGenerator::MDTChamberFROEventGenerator(
                            const double & R,
                            const double & w_wall,
                            const unsigned int & nb_tubes_per_layer,
                            const unsigned int & nb_layers_per_multilayer,
                            const double & spacer_thickness,
                            const string & rt_file_name) {

    init(R, w_wall, nb_tubes_per_layer, nb_layers_per_multilayer,
         spacer_thickness, rt_file_name);

}

//*****************************************************************************

//:::::::::::::::
//:: METHOD rt ::
//:::::::::::::::

const Rt_relation & MDTChamberFROEventGenerator::rt(void) const {

    return m_rt;

}

//*****************************************************************************

//:::::::::::::::::::::
//:: METHOD generate ::
//:::::::::::::::::::::

const std::vector<BareMDTHit> & MDTChamberFROEventGenerator::generate(
                                            const Straight_line & track,
                                            bool delta_electrons_on,
                                            const double & t_min,
                                            const double & occupancy,
                                            const double & road_width,
                                            const double & t0_shift,
                                            const double & t_accuracy,
                                            int & nb_muon_hits) {

///////////////
// VARIABLES //
///////////////

    double r_delta;
    double d;
    double t, t_digi;
    double t_dead(200.0); // auxiliary dead time to count the number of muon
                          // hits in the standard read-out chain
    double t_min2;
    double t_max(m_rt.t(m_R-m_w_wall, 0)); // maximum drift time
    BareMDTHit *bare_hit(0); // auxiliary bare hit
    BareMDTHit *bare_hit_2(0); // auxiliary bare hit
    int minus_muon_hits(0);

    if (fabs(t_min)<t_dead) {
        t_min2 = t_dead;
    } else {
        t_min2 = t_min;
    }

////////////
// RESETS //
////////////

    m_bare_hit.clear();
    nb_muon_hits = 0;

/////////////////////////
// LOOP OVER THE TUBES //
/////////////////////////

    for (unsigned int ly=0; ly<2*m_nb_ly_per_ml; ly++) {
    for (unsigned int tb=0; tb<m_nb_tubes_per_ly; tb++) {

        minus_muon_hits = 0;

// check whether tube is within the road width around the track //
        d = fabs(track.sign_dist_from(m_wire_position[ly][tb]));
        if (d-road_width<m_R) {

    // create a background hit //
            if (m_p_rnd->Uniform(1.0)<occupancy) {
                t = m_p_rnd->Uniform(t_min2, t_max);
                t_digi = t_accuracy*static_cast<int>(t/t_accuracy)
                         +0.5*t_accuracy;
                bare_hit = new BareMDTHit(
                                m_wire_position[ly][tb].position_vector(),
                                t_digi);
            }

    // create a muon hit //
            if (d<m_R-m_w_wall) {
                t = m_p_rnd->Gaus(m_rt.t(d, 0), m_rt.t_error(m_rt.t(d, 0), 0));
                t_digi = t_accuracy*static_cast<int>(t/t_accuracy)
                         +0.5*t_accuracy;
                bare_hit_2 = new BareMDTHit(
                                m_wire_position[ly][tb].position_vector(),
                                t_digi);
                
                nb_muon_hits++;
            }


    // create delta electron hit (highly simplified model) //
            if (bare_hit_2!=0 &&
                delta_electrons_on && m_p_rnd->Uniform(0.0, 1.0)<0.28) {
                r_delta = m_p_rnd->Uniform(0.0, 2.0*m_R);
                if (bare_hit!=0 && m_rt.t(r_delta, 0)<bare_hit->getDriftTime()){
                 if (r_delta<d &&
                    fabs(m_rt.t(r_delta, 0)-bare_hit->getDriftTime())>t_dead) {
                    delete bare_hit_2;
                    minus_muon_hits = -1;
                    t_digi = t_accuracy*
                             static_cast<int>(m_rt.t(r_delta, 0)/t_accuracy)
                             +0.5*t_accuracy;

                    bare_hit_2 = new BareMDTHit(
                                m_wire_position[ly][tb].position_vector(),
                                t_digi);   
                 }
                }
                if (bare_hit==0 ||
                    (bare_hit!=0 &&
                     m_rt.t(r_delta, 0)>=bare_hit->getDriftTime())) {
                 if (r_delta<d) {
                    delete bare_hit_2;
                    minus_muon_hits = -1;
                    t_digi = t_accuracy*
                             static_cast<int>(m_rt.t(r_delta, 0)/t_accuracy)
                             +0.5*t_accuracy;

                    bare_hit_2 = new BareMDTHit(
                                m_wire_position[ly][tb].position_vector(),
                                t_digi);   
                 }
                }
            }

    // check for dead time //
            if (bare_hit!=0 && bare_hit_2!=0) {
                if (bare_hit->getDriftTime()<bare_hit_2->getDriftTime()) {
                    delete bare_hit_2;
                } else {
                    delete bare_hit;
                }

            }
            if (bare_hit!=0) {
                m_bare_hit.push_back(*bare_hit);
            }
            if (bare_hit_2!=0) {
                m_bare_hit.push_back(*bare_hit_2);
            }
        }

        nb_muon_hits = nb_muon_hits+minus_muon_hits;

        bare_hit = 0;
        bare_hit_2 = 0;

    }
    }

////////////////////////////////
// DISPLAY FILE FOR DEBUGGING //
////////////////////////////////

//     static ofstream outfile("hit_display.cxx");
//     outfile << "display();" << endl;
// //     outfile << "TH2F *hist = new TH2F(\"hist\", \"\", 100, "
// //             << -1.1*m_nb_tubes_per_ly*m_R << ", "
// //             << 1.1*m_nb_tubes_per_ly*m_R<< ", 100, "
// //             << -1.1*m_nb_tubes_per_ly*m_R << ", "
// //             << 1.1*m_nb_tubes_per_ly*m_R << ");"
// //             << endl;
// //     outfile << "hist->Draw();" << endl;
//     for (unsigned int k=0; k<m_bare_hit.size(); k++) {
//         outfile << "TArc *arc = new TArc("
//                 << m_bare_hit[k].getWirePosition().y()
//                 << ", "
//                 << m_bare_hit[k].getWirePosition().z()
//                 << ", " << m_rt.r(m_bare_hit[k].getDriftTime(), 0)
//                 << "); arc->SetLineColor(2); "
//                 << "arc->Draw(\"SAME\");"
//                 << endl;
//     }
// 
//     outfile << "TLine *line=new TLine("
//             << track.my()*(-1.1*m_nb_tubes_per_ly*m_R)+track.by() << ", "
//             << (-1.1*m_nb_tubes_per_ly*m_R) << ", "
//             << track.my()*(1.1*m_nb_tubes_per_ly*m_R)+track.by() << ", "
//             << (1.1*m_nb_tubes_per_ly*m_R) << "); line->Draw(\"SAME\");"
//             << endl;
// 
//     outfile << "c1->Update(); double wait; cin >> wait;" << endl;

    static ofstream res_file("residuals.txt");
    for (unsigned int k=0; k<m_bare_hit.size(); k++) {
        Straight_line wire(m_bare_hit[k].getWirePosition(),
                           Hep3Vector(1.0, 0.0, 0.0));
        res_file << m_bare_hit[k].getWirePosition().y() << "\t"
                << track.sign_dist_from(wire) << "\t"
                << m_bare_hit[k].getDriftTime() << "\t"
                << m_rt.r(m_bare_hit[k].getDriftTime(), 0) << "\t"
                << m_rt.t_error(m_bare_hit[k].getDriftTime(), 0)
                << endl;
    }

    return m_bare_hit;

}

//*****************************************************************************

//:::::::::::::::::
//:: METHOD init ::
//:::::::::::::::::

void MDTChamberFROEventGenerator::init(const double & R,
              const double & w_wall,
              const unsigned int & nb_tubes_per_layer,
              const unsigned int & nb_layers_per_multilayer,
              const double & spacer_thickness,
              const string & rt_file_name) {

///////////////
// VARIABLES //
///////////////

    double pos_y, pos_z;
    double h;
    Hep3Vector dir(1.0, 0.0, 0.0);
    ifstream rt_file(rt_file_name.c_str());
    if (rt_file.fail()) {
        cerr << "Class MDTChamberFROEventGenerator, method init: ERROR!\n"
             << "Could not open file " << rt_file_name << ".\n";
        exit(1);
    }

/////////////////////////////
// STORE PRIVATE VARIABLES //
/////////////////////////////

    m_R = R;
    h = sqrt(3.0)*m_R;
    m_w_wall = w_wall;
    m_nb_tubes_per_ly = nb_tubes_per_layer;
    m_nb_ly_per_ml = nb_layers_per_multilayer;
    m_rt.read_rt(rt_file);
    m_wire_position = vector< vector< Straight_line > >(2*m_nb_ly_per_ml);
    for (unsigned int k=0; k<m_wire_position.size(); k++) {
        m_wire_position[k] = vector<Straight_line>(m_nb_tubes_per_ly);
    }

/////////////////////////////////
// CREATE THE CHAMBER GEOMETRY //
/////////////////////////////////

    for (unsigned int ly=0; ly<2*m_nb_ly_per_ml; ly++) {
    for (unsigned int tb=0; tb<m_nb_tubes_per_ly; tb++) {

// 1st multilayer //
        if (ly<m_nb_ly_per_ml) {
            pos_z = -0.5*spacer_thickness-m_R+
                    static_cast<int>(ly-m_nb_ly_per_ml+1)*h;
            pos_y = (tb-0.5*m_nb_tubes_per_ly)*2.0*m_R-m_R*(ly%2);
        }

// 2nd multilayer //
        if (ly>=m_nb_ly_per_ml) {
            pos_z = 0.5*spacer_thickness+m_R+(ly-m_nb_ly_per_ml)*h;
            pos_y = (tb-0.5*m_nb_tubes_per_ly)*2.0*m_R-m_R*((ly-1)%2);
        }

// store wire position //
        m_wire_position[ly][tb] = Straight_line(Hep3Vector(0.0, pos_y, pos_z),
                                              dir);

    }
    }

////////////////////////////////
// DISPLAY FILE FOR DEBUGGING //
////////////////////////////////

    ofstream outfile("display.cxx");
    outfile << "void display(void) {" << endl;
    outfile << "TH2F *hist = new TH2F(\"hist\", \"\", 100, "
            << -1.1*m_nb_tubes_per_ly*m_R << ", "
            << 1.1*m_nb_tubes_per_ly*m_R<< ", 100, "
            << -1.1*m_nb_tubes_per_ly*m_R << ", "
            << 1.1*m_nb_tubes_per_ly*m_R << ");"
            << endl;
    outfile << "hist->Draw();" << endl;
    for (unsigned int ly=0; ly<2*m_nb_ly_per_ml; ly++) {
    for (unsigned int tb=0; tb<m_nb_tubes_per_ly; tb++) {
        outfile << "TArc *arc = new TArc("
                << m_wire_position[ly][tb].position_vector().y()
                << ", "
                << m_wire_position[ly][tb].position_vector().z()
                << ", " << m_R << "); arc->Draw(\"SAME\");"
                << endl;
    }
    }
    outfile << "}" << endl;

/////////////////////////////
// RANDOM NUMBER GENERATOR //
/////////////////////////////

    m_p_rnd = new TRandom3(time(0));

    return;

}
