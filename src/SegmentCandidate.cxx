//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                         SegmentCandidate                        ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>

// MDTTrigger //
#include "SegmentCandidate.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;

//*****************************************************************************

//:::::::::::::::::::::::::
//:: DEFAULT CONSTRUCTOR ::
//:::::::::::::::::::::::::

SegmentCandidate::SegmentCandidate(void) {
}

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

SegmentCandidate::SegmentCandidate(const Straight_line & segment,
                     const unsigned int & nb_hits,
                     const double & chi2,
                     const double & m_err) {

    m_segment = segment;
    m_nb_hits = nb_hits;
    m_chi2 = chi2;
    m_m_err = m_err;

}

//*****************************************************************************

//:::::::::::::::::::::::
//:: METHOD getSegment ::
//:::::::::::::::::::::::

const Straight_line & SegmentCandidate::getSegment(void) const {

    return m_segment;

}

//*****************************************************************************

//::::::::::::::::::::::::::::
//:: METHOD getNumberOfHits ::
//::::::::::::::::::::::::::::

unsigned int SegmentCandidate::getNumberOfHits(void) const {

    return m_nb_hits;

}

//*****************************************************************************

//::::::::::::::::::::
//:: METHOD getChi2 ::
//::::::::::::::::::::

double SegmentCandidate::getChi2(void) const {

    return m_chi2;

}

//*****************************************************************************

//::::::::::::::::::::::::::
//:: METHOD getSlopeError ::
//::::::::::::::::::::::::::

double SegmentCandidate::getSlopeError(void) const {

    return m_m_err;

}
