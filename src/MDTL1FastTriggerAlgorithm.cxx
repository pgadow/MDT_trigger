//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 28.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                       MDTL1FastTriggerAlgorithm                 ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

#include "TFile.h"
#include "TCanvas.h"
#include "TImage.h"

// standard C++ //
#include <iostream>
#include <string>
#include <cmath>

// MDTTrigger //
#include "MDTL1FastTriggerAlgorithm.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;
using namespace CLHEP;

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

MDTL1FastTriggerAlgorithm::MDTL1FastTriggerAlgorithm(\
                          const double & b_width,
                          const double & m_width,
                          const double & search_width) {

    m_b_width = static_cast<double>(b_width);
    m_m_width = static_cast<double>(m_width);
    m_search_width = static_cast<double>(search_width);

}

//*****************************************************************************

//:::::::::::::::::::::::::::::::::
//:: METHOD getSegmentCandidates ::
//:::::::::::::::::::::::::::::::::

const std::vector<SegmentCandidate> &
                        MDTL1FastTriggerAlgorithm::getSegmentCandidates(
                                        const std::vector<BareMDTHit> & hits,
                                        const Straight_line & L0_track,
                                        const double & t_accuracy,
                                        const Rt_relation & rt) {

///////////
// RESET //
///////////

    m_segment.clear();

///////////////
// VARIABLES //
///////////////

    double chi2(0.0); // chi^2 of the reconstructed straight line
    double m(0.0), b(0.0);  // slope and intercept of a straight line
    double g_1 = 0.0; // fit calculation
    double g_2 = 0.0; // fit calculation
    double Lambda_11 = 0.0; // fit calculation
    double Lambda_12 = 0.0; // fit calculation
    double Lambda_22 = 0.0; // fit calculation
    double D = 0.0; // fit calculation

    double m_mean = 0; // arithmetic mean of tangent slopes
    double b_mean = 0; // arithmetic mean of tangent intercepts
    unsigned int n_tangents = 0; // helper variable for arithmetic mean

    vector<double> rs; // result of tangent calculation: [0]:slope [1]:intercept
    rs.reserve(2);

    vector<BareMDTHit> passed_hits; // hits passing pattern recognition
    passed_hits.reserve(hits.size());
    Hep3Vector wire_pos_1; // wire position of first hit
    Hep3Vector wire_pos_2; // wire position of second hit
    Hep3Vector L;          // vector between wire positions (wp2 - wp1)
    double r1;             // drift radius of first hit
    double r2;             // drift radius of second hit

    Hep3Vector wire_pos;   // wire position (needed for hit calculation)
    double r;              // drift radius (needed for hit calculation)

    vector<Hep3Vector> results_hits; // results of hit positions

    double t_max=rt.t(rt.number_of_pairs()-1);
    double r_max=rt.r(rt.number_of_pairs()-1);
    double sigma(0.0);
    double m_err(0.0);

/////////////////////////////////////
// COMPUTE MEAN SPATIAL RESOLUTION //
/////////////////////////////////////

    for (int k=0; k<rt.number_of_pairs(); k++) {
        sigma += rt.error(k)*rt.error(k);
    }
    sigma = sqrt(sigma/static_cast<double>(rt.number_of_pairs())
                 +pow(t_accuracy*r_max/(t_max*sqrt(12.0)), 2));


///////////////////////////////////////////////////////////
// CREATE A VECTOR OF MDT HITS FOR THE TRIGGER ALGORITHM //
///////////////////////////////////////////////////////////

    // only process hits with drift time within trigger signal
    // (extended by time accuracy). Illustration:
    // t[ns]: ----(-time_accuracy)--------|trigger signal|----

    passed_hits.clear();
    for (unsigned int k=0; k<hits.size(); k++) {
        if (hits[k].getDriftTime()<-t_accuracy) {
            continue;
        }

        passed_hits.push_back(BareMDTHit(hits[k].getWirePosition(),\
                                         hits[k].getDriftTime(),\
                                         rt,\
                                         hits[k].getMultiLayer(),\
                                         hits[k].getLayer(),\
                                         hits[k].getTube() ));
    }

    // perform pattern recognition to reduce number of hits to be processed
    // in algorithm

    //patternRecognition(passed_hits);


///////////////////////////////////////////////
// CALCULATE THE TANGENTS TO THE DRIFT RADII //
///////////////////////////////////////////////

    m_mean = 0;
    b_mean = 0;
    n_tangents = 0;

    for (unsigned int j=0; j<passed_hits.size(); j++) {
      wire_pos_1 = passed_hits[j].getWirePosition();
      r1 = passed_hits[j].getDriftRadius();
      for (unsigned int k=j+1; k<passed_hits.size(); k++) {

        // only calculate tangents of hits in the same multilayer
        // and in different layers
        if (passed_hits[j].getMultiLayer() != passed_hits[k].getMultiLayer() ||\
            passed_hits[j].getLayer() == passed_hits[k].getLayer()) {
              cout << "pass" << endl;
              continue;
        }

        wire_pos_2 = passed_hits[k].getWirePosition();
        r2 = passed_hits[k].getDriftRadius();

        L = wire_pos_2 - wire_pos_1;
        double L2 = L.mag2();
        double alpha_0 = atan(L.y()/L.z());

        // calculate tangents for 4 different combinations
        for (unsigned int i = 0; i < 4; i++) {
          rs = calculateTangent(r1,r2,L2,i); // m=result[0], b=result[1]
          double alpha = atan(rs[0]);
          double m_candidate = tan(alpha+alpha_0);
          double b_candidate = wire_pos_1.y() + cos(alpha_0)*rs[1]\
                   -m_candidate*wire_pos_1.z() + m_candidate*sin(alpha_0)*rs[1];

          // cout << "r1: " << j <<", r2: "<<k<<", combination: "<<i<<endl;
          // cout << "wp1: " << wire_pos_1.z() << " " << wire_pos_1.y() << endl;
          // cout << "m: " << rs[0] << " b: " << rs[1] << endl;
          // cout << "m_candidate: " << m_candidate
          //          << "\tL0_track.my: " << L0_track.my()<< endl;
          // cout << "b_candidate: " << b_candidate
          //         << "\tL0_track.by: " << L0_track.by()<< endl;
          // cout << endl;

          //check if result is compatible with TGC/RPC seed track
          if (fabs(L0_track.my()-m_candidate)>m_m_width ||\
              fabs(L0_track.by()-b_candidate)>m_b_width){
              continue;
          }

          ++n_tangents;
          m_mean += m_candidate;
          b_mean += b_candidate;
        }
      }
    }

    // Calculate average slope and intercept, if not possible, take L0 path
    if (n_tangents > 0) {
        m_mean /= static_cast<double>(n_tangents);
        b_mean /= static_cast<double>(n_tangents);
    }
    else{
        m_mean = L0_track.my();
        b_mean = L0_track.by();
    }


    // Create track seed from arithmetic mean of tangent parameters //
    Straight_line averaged_tangent(Hep3Vector(0.0, b_mean, 0.0),\
                                    Hep3Vector(0.0, m_mean, 1.0));

    // Find hit positions closest to averaged tangent //
    for (unsigned int j=0; j<passed_hits.size(); j++) {
      wire_pos = passed_hits[j].getWirePosition();
      r = passed_hits[j].getDriftRadius();

      Hep3Vector hit = calculateHit(r, wire_pos, averaged_tangent);
      if(abs(averaged_tangent.dist_from_line(hit)) < m_search_width){
        results_hits.push_back(hit);
      }
    }


    for (unsigned int i = 0; i < results_hits.size(); i++) {
      g_1 = g_1+results_hits[i].y();
      g_2 = g_2+results_hits[i].y()*results_hits[i].z();
      Lambda_11 = Lambda_11+1.0;
      Lambda_12 = Lambda_12+results_hits[i].z();
      Lambda_22 = Lambda_22+results_hits[i].z()*results_hits[i].z();
    }

    D = Lambda_11*Lambda_22-Lambda_12*Lambda_12;
    b = (g_1*Lambda_22-g_2*Lambda_12)/D;
    m = (-g_1*Lambda_12+g_2*Lambda_11)/D;
    m_err = sigma*sqrt(Lambda_11*Lambda_11*Lambda_22
                 -2.0*Lambda_11*Lambda_12*Lambda_12
                 +Lambda_12*Lambda_12*Lambda_11)/D;

    chi2 = 0.0;
    for(unsigned int j=0; j<results_hits.size(); j++) {
        chi2 = chi2+pow((results_hits[j].y()-b-m*results_hits[j].z())/sigma, 2);
    }

    // create track segment candidate //
    Straight_line track_candidate(Hep3Vector(0.0, b, 0.0),\
                                        Hep3Vector(0.0, m, 1.0));
    m_segment.push_back(SegmentCandidate(track_candidate,\
                  static_cast<unsigned int>(results_hits.size()), chi2, m_err));

    // cout << "number of found hits " << results_hits.size() << " of " << passed_hits.size() << endl;
    // cout << "number of tangents " << n_tangents << endl;
    // cout << "L0_track.my: " << L0_track.my() << "\tL0_track.by: " << L0_track.by()<< endl;
    // cout << "m_mean " << m_mean << ", b_mean " << b_mean <<endl;
    // cout << "m " << m << ", b " << b <<endl;

    return m_segment;
}

//*****************************************************************************

//:::::::::::::::::::::::::::::
//:: METHOD calculateTangent ::
//:::::::::::::::::::::::::::::

std::vector<double> MDTL1FastTriggerAlgorithm::calculateTangent(\
                                    double r1, double r2, double L2,\
                                    unsigned int configuration){
    std::vector<double> result;
    result.reserve(2);
    double m,b;
    double nominator;
    double denominator1 = sqrt(L2 - r1*r1 - r2*r2 + 2*r1*r2);
    double denominator2 = sqrt(L2 - r1*r1 - r2*r2 - 2*r1*r2);

    // check if denominator is zero
    if (denominator1 == 0 || denominator2 == 0) {
      result.push_back(-999999);
      result.push_back(-999999);
      return result;
    }

    switch (configuration) {
      case 0:
        nominator = r2 - r1;
        m = nominator/denominator1;
        b = r1*sqrt(1+m*m);
        break;
      case 1:
        nominator = r1 - r2;
        m = nominator/denominator1;
        b = -r1*sqrt(1+m*m);
        break;
      case 2:
        nominator = -r1 - r2;
        b = r1*sqrt(1+m*m);
        m = nominator/denominator2;
        break;
      case 3:
        nominator = r1 + r2;
        b = -r1*sqrt(1+m*m);
        m = nominator/denominator2;
        break;
      default:
        nominator = 0;
        break;
    }

    result.push_back(m);
    result.push_back(b);

    return result;
}

//*****************************************************************************

//::::::::::::::::::::::::::
//:: METHOD calculateHits ::
//::::::::::::::::::::::::::

Hep3Vector MDTL1FastTriggerAlgorithm::calculateHit(\
                                double r, Hep3Vector wp, Straight_line tangent){

    Hep3Vector hit1 = wp + r * tangent.direction_vector().orthogonal().unit();
    Hep3Vector hit2 = wp - r * tangent.direction_vector().orthogonal().unit();

    if (abs(tangent.dist_from_line(hit1)) < abs(tangent.dist_from_line(hit2))) {
      return hit1;
    }
    return hit2;

}

//*****************************************************************************

//:::::::::::::::::::::::::::::::::
//:: METHOD patternRecognition   ::
//:::::::::::::::::::::::::::::::::

void MDTL1FastTriggerAlgorithm::patternRecognition(\
                                      std::vector<BareMDTHit> & hits){

    // remove hits with a certain criterium
    return;

}
