//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                        ExtendedHistogram                       ::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>
#include <cstdlib>

// MDTTrigger //
#include "ExtendedHistogram.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

ExtendedHistogram::ExtendedHistogram(
                      const std::string & hist_name,
                      const std::string & hist_title,
                      const unsigned int & nb_bins,
                      const double & x_min,
                      const double & x_max) : TH1F(hist_name.c_str(),
                                                   hist_title.c_str(),
                                                   nb_bins,
                                                   x_min,
                                                   x_max) {

    m_ep = vector< vector<const CLHEP::Hep3Vector *> >(nb_bins+2);

}

//*****************************************************************************

//::::::::::::::::::::::::::::::
//:: METHOD getEntryProviders ::
//::::::::::::::::::::::::::::::

const std::vector<const CLHEP::Hep3Vector *>
    ExtendedHistogram::getEntryProvider(
                                                const int & bin_number) const {

//////////////////////
// CHECK BIN NUMBER //
//////////////////////

    if (bin_number<0 || bin_number>this->GetNbinsX()+1) {
        cerr << "Class ExtendedHistogram, method getEntryProviders: ERROR!\n"
             << "Illegal bin number " << bin_number << endl;
        exit(1);
    }

/////////////////////////////
// RETURN REQUESTED VECTOR //
/////////////////////////////

    return m_ep[bin_number];

}

//*****************************************************************************

//:::::::::::::::::
//:: METHOD Fill ::
//:::::::::::::::::

void ExtendedHistogram::Fill(const double & x, const double & w,
                             const CLHEP::Hep3Vector * ep) {

/////////////////////////////
// FILL THE ROOT HISTOGRAM //
/////////////////////////////

    TH1F::Fill(x, w);

///////////////////////////////////////////
// STORE A POINTER TO THE ENTRY PROVIDER //
///////////////////////////////////////////

    m_ep[FindBin(x)].push_back(ep);

    return;

}

//*****************************************************************************

//::::::::::::::::::
//:: METHOD Clear ::
//::::::::::::::::::

void ExtendedHistogram::Clear(void) {

    TH1F::Reset();
    for (unsigned int k=0; k<m_ep.size(); k++) {
        m_ep[k].clear();
    }

    return;

}
