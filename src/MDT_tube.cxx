//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 01./06.04.1999, AUTHOR: OLIVER KORTNER
// Modified: 06.06.2012 by O. Kortner, major clean-up for MDT trigger studies.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION OF THE INLINE METHODS DEFINED IN THE CLASS MDT_tube //
////////////////////////////////////////////////////////////////////////

//*****************************************************************************

/////////////////
// METHOD copy //
/////////////////

inline void MDT_tube::copy(const MDT_tube & tb) {

	left_end_point = tb.end_point_1();
	right_end_point = tb.end_point_2();
	r_inner = tb.inner_radius();
	r_outer = tb.outer_radius();
	wire_left_end_point = tb.wire_end_1();
	wire_right_end_point = tb.wire_end_2();
	rt = tb.rt_rel();
	t_0 = tb.t0();
	t_conv = tb.t_cal();
	hit = tb.hit_type();
	guessed_dist = tb.guessed_hit_dist();
	TDC_value = tb.TDC_count();
	r_drift = tb.r();
	t_drift = tb.t();
	r_err = tb.error();

	return;

}

//*****************************************************************************

///////////////////////////////
// METHOD distance_from_wire //
///////////////////////////////

inline HepDouble MDT_tube::distance_from_wire(const Straight_line & track)
									const {

///////////////
// VARIABLES //
///////////////

	Straight_line wire(wire_left_end_point,
                                    wire_right_end_point-wire_left_end_point);

/////////////////////
// RETURN DISTANCE //
/////////////////////

	return wire.dist_from_line(track);

}

//*****************************************************************************

//////////////////////////////////////
// METHOD signed_distance_from_wire //
//////////////////////////////////////

inline HepDouble MDT_tube::signed_distance_from_wire(
					const Straight_line & track) const {

///////////////
// VARIABLES //
///////////////

	Straight_line wire(wire_left_end_point, 
                                    wire_right_end_point-wire_left_end_point);

/////////////////////
// RETURN DISTANCE //
/////////////////////

	return wire.sign_dist_from(track);

}

//*****************************************************************************

///////////////////////////////
// METHOD distance_from_axis //
///////////////////////////////

inline HepDouble MDT_tube::distance_from_axis(const Straight_line & track)
									const {

///////////////
// VARIABLES //
///////////////

	Straight_line tube_axis(left_end_point, right_end_point-left_end_point);

/////////////////////
// RETURN DISTANCE //
/////////////////////

	return fabs(tube_axis.sign_dist_from(track));

}


//*****************************************************************************

/////////////////////////////////////
//METHOD set_propagation_direction //
/////////////////////////////////////

inline void MDT_tube :: set_propagation_direction(const HepDouble &dir)
	{
	if(dir == 0.0)
		{
		propagation_direction=0.0;
		}
	if(dir >0.0)
		{
		propagation_direction =1.0;
		}
	if(dir <0.0)
		{
		propagation_direction = -1.0;
		}
	}


//*****************************************************************************

////////////////////////////
// METHOD set_end_point_1 //
////////////////////////////

inline void MDT_tube::set_end_point_1(const Hep3Vector & end_pnt) {

	left_end_point = end_pnt;
	return;

}

//*****************************************************************************

////////////////////////////
// METHOD set_end_point_2 //
////////////////////////////

inline void MDT_tube::set_end_point_2(const Hep3Vector & end_pnt) {

	right_end_point = end_pnt;
	return;

}

//*****************************************************************************

/////////////////////////////
// METHOD set_inner_radius //
/////////////////////////////

inline void MDT_tube::set_inner_radius(const HepDouble & r) {

	r_inner = r;
	return;

}


//*****************************************************************************

/////////////////////////////
// METHOD set_outer_radius //
/////////////////////////////

inline void MDT_tube::set_outer_radius(const HepDouble & r) {

	r_outer = r;
	return;

}

//*****************************************************************************

///////////////////////////
// METHOD set_wire_end_1 //
///////////////////////////

inline void MDT_tube::set_wire_end_1(const Hep3Vector & end_pnt) {

	wire_left_end_point = end_pnt;
	return;

}

//*****************************************************************************

///////////////////////////
// METHOD set_wire_end_2 //
///////////////////////////

inline void MDT_tube::set_wire_end_2(const Hep3Vector & end_pnt) {

	wire_right_end_point = end_pnt;
	return;

}

//*****************************************************************************

/////////////////////////
// METHOD set_wire_sag //
/////////////////////////

inline void MDT_tube::set_wire_sag(const HepDouble & wire_sag) {

	sag = wire_sag;
	return;

}

//*****************************************************************************

///////////////////
// METHOD set_t0 //
///////////////////

inline void MDT_tube::set_t0(const HepDouble & t_zero) {

// correct drift time //
	t_drift = t_drift + t_0 - t_zero;

// reset t0
	t_0 = t_zero;

	return;

}

//*****************************************************************************

//////////////////////
// METHOD set_t_cal //
//////////////////////

inline void MDT_tube::set_t_cal(const HepDouble & t_con) {

	t_conv = t_con;
	return;

}

//*****************************************************************************

/////////////////////////
// METHOD set_hit_type //
/////////////////////////

inline void MDT_tube::set_hit_type(const HepInt & tp) {

	if (tp != 0 && tp != 2) {
		cerr << endl
			<< "Class MDT_tube, method set_hit_type: ERROR!"
			<< endl
			<< "Illegal hit type! Must be" << endl
			<< "       0 (no hit) or 2 (hit)." << endl;
		return;
	}

	hit = tp;
	return;

}

//*****************************************************************************

/////////////////////////////////
// METHOD set_guessed_hit_dist //
/////////////////////////////////

inline void MDT_tube::set_guessed_hit_dist(const HepDouble & guess) {

	guessed_dist = guess;
//	cout << endl << guessed_dist - offset << endl;
//	cout << guessed_dist << endl << offset << endl;
	return;

}

//*****************************************************************************

///////////////////////////////
// METHOD set_hodoscope_time //
///////////////////////////////


inline void MDT_tube::set_hodoscope_time(const HepDouble & time) {

	hodo_time = time;
	return;

}

//*****************************************************************************

//////////////////////////////////////////
// METHOD set_time_of_flight_correction //
//////////////////////////////////////////

inline void MDT_tube::set_time_of_flight_correction(const HepDouble & t_corr) {

	t_of_flight = t_corr;
	return;

}

//*****************************************************************************

/////////////////////////
// METHOD set_hit_data //
/////////////////////////

inline void MDT_tube::set_hit_data(const MDT_tube & MDT) {

////////////////////////////////////////////////
// COPY HIT DATA FROM MDT TO THE CURRENT TUBE //
////////////////////////////////////////////////

// t_0 //
	t_0 = MDT.t0();

// t_cal //
	t_conv = MDT.t_cal();

// hit type //
	hit = MDT.hit_type();

// guessed hit distance //
	guessed_dist = MDT.guessed_hit_dist();

// r //
	r_drift = MDT.r();

// t //
	t_drift = MDT.t();

// error of r //
	r_err = MDT.error();

	return;

}

//*****************************************************************************

////////////////////////////
// METHOD set_Rt_relation //
////////////////////////////

inline void MDT_tube::set_Rt_relation(const Rt_relation & new_rt) {

	rt = new_rt;
	return;

}
//*****************************************************************************


////////////////////////////////
// METHOD get_module_number() //
////////////////////////////////

inline unsigned int MDT_tube::get_module_number() const
	{
	return TDC_mod_nb;
	}
	
//*****************************************************************************


///////////////////////////////
// METHOD get_channel_number //
///////////////////////////////
	
inline unsigned int MDT_tube::get_channel_number() const
	{
	return channel_nb;
	}
