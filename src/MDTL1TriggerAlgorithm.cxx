//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 28.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                       MDTL1TriggerAlgorithm                     ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>
#include <cmath>
#include <algorithm>

// MDTTrigger //
#include "MDTL1TriggerAlgorithm.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;
using namespace CLHEP;


//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

MDTL1TriggerAlgorithm::MDTL1TriggerAlgorithm(
                          const double & y_min,
                          const double & y_max,
                          const double & bin_width,
                          const bool   & single_candidate) {

    m_track_hist_1 = new ExtendedHistogram("track_hist_1", "",
                         static_cast<int>((y_max-y_min)/bin_width),
                         y_min, y_max);
    m_track_hist_2 = new ExtendedHistogram("track_hist_2", "",
                         static_cast<int>((y_max-y_min)/bin_width),
                         y_min-0.5*bin_width, y_max-0.5*bin_width);
    m_single_candidate = single_candidate;

}

//*****************************************************************************

//:::::::::::::::::::::::::::::::::
//:: METHOD getSegmentCandidates ::
//:::::::::::::::::::::::::::::::::

const std::vector<SegmentCandidate> &
                        MDTL1TriggerAlgorithm::getSegmentCandidates(
                                        const std::vector<BareMDTHit> & hits,
                                        const Straight_line & L0_track,
                                        const double & t_accuracy,
                                        const Rt_relation & rt) {

///////////
// RESET //
///////////

    m_track_hist_1->Clear();
    m_track_hist_2->Clear();
    m_segment.clear();

///////////////
// VARIABLES //
///////////////

    double chi2(0.0); // chi^2 of the reconstructed straight line
    double m(0.0), b(0.0);  // slope and intercept of a straight line
    Hep3Vector y, z; // coordinate axes
    Hep3Vector origin(0.0, 0.0, 0.0); // origin of the coordinate system
    double max_1, max_2, max; // maximum numbers of entries in a bin of
                              // histograms
    vector<int> max_bins; // bins with maximum number of entries
    double g_1, g_2;
    double Lambda_11, Lambda_12, Lambda_22;
    double D;
    ExtendedHistogram *track_hist(0);
    double t_max=rt.t(rt.number_of_pairs()-1);
    double r_max=rt.r(rt.number_of_pairs()-1);
    double sigma(0.0);
    double m_err;

/////////////////////////////////////
// COMPUTE MEAN SPATIAL RESOLUTION //
/////////////////////////////////////

    for (int k=0; k<rt.number_of_pairs(); k++) {
        sigma = sigma+pow(rt.error(k), 2);
    }
    sigma = sqrt(sigma/static_cast<double>(rt.number_of_pairs())
                 +pow(t_accuracy*r_max/(t_max*sqrt(12.0)), 2));

/////////////////////////////
// COMPUTE COORDINATE AXES //
/////////////////////////////

    y = Hep3Vector(0.0, 1.0, -L0_track.my()); y = y.unit();
    z = Hep3Vector(0.0, L0_track.my(), 1.0); z = z.unit();

///////////////////////////////////////////////////////////
// CREATE A VECTOR OF MDT HITS FOR THE TRIGGER ALGORITHM //
///////////////////////////////////////////////////////////

    vector<MDTHitForTrigger> processed_hit;
    for (unsigned int k=0; k<hits.size(); k++) {
        if (hits[k].getDriftTime()<-t_accuracy) {
            continue;
        }
        processed_hit.push_back(MDTHitForTrigger(hits[k], rt, origin, y, z));
    }

/////////////////////////////////////////////
// FILL THE PATTERN RECOGNITION HISTOGRAMS //
/////////////////////////////////////////////

    for (unsigned int k=0; k<processed_hit.size(); k++) {
        m_track_hist_1->Fill(processed_hit[k].getHitPosition1().y(), 1.0,
                             &(processed_hit[k].getHitPosition1()));
        m_track_hist_1->Fill(processed_hit[k].getHitPosition2().y(), 1.0,
                             &(processed_hit[k].getHitPosition2()));
        m_track_hist_2->Fill(processed_hit[k].getHitPosition1().y(), 1.0,
                             &(processed_hit[k].getHitPosition1()));
        m_track_hist_2->Fill(processed_hit[k].getHitPosition2().y(), 1.0,
                             &(processed_hit[k].getHitPosition2()));
   }

/////////////////
// FIND MAXIMA //
/////////////////

    max_1 = m_track_hist_1->GetMaximum();
    max_2 = m_track_hist_2->GetMaximum();

//     static ofstream outfile("hist_debug.txt");
//     static unsigned int count(1);

    if (max_1>=max_2) {
        max = max_1;
        track_hist = m_track_hist_1;
        for (int bin=1; bin<=m_track_hist_1->GetNbinsX(); bin++) {
//             outfile << count << "\t"
//                     << bin << "\t"
//                     << m_track_hist_1->GetBinCenter(bin) << "\t"
//                     << m_track_hist_1->GetBinContent(bin)
//                     << endl;
            if (m_track_hist_1->GetBinContent(bin)==max_1) {
                max_bins.push_back(bin);
            }
        }
//         count++;
    } else {
        max = max_2;
        track_hist = m_track_hist_2;
        for (int bin=1; bin<=m_track_hist_2->GetNbinsX(); bin++) {
            if (m_track_hist_2->GetBinContent(bin)==max_2) {
                max_bins.push_back(bin);
            }
        }
    }

////////////////////
// EXPLORE MAXIMA //
////////////////////

// loop over the maxima and perform straigt line fits  //
    for (unsigned int k=0; k<max_bins.size(); k++) {
        g_1 = 0.0;
        g_2 = 0.0;
        Lambda_11 = 0.0;
        Lambda_12 = 0.0;
        Lambda_22 = 0.0;
        const vector<const CLHEP::Hep3Vector *> & entry(
                                    track_hist->getEntryProvider(max_bins[k]));
        for (unsigned int i=0; i<entry.size(); i++) {
            const Hep3Vector *pos(entry[i]);
            g_1 = g_1+pos->y();
            g_2 = g_2+pos->y()*pos->z();
            Lambda_11 = Lambda_11+1.0;
            Lambda_12 = Lambda_12+pos->z();
            Lambda_22 = Lambda_22+pos->z()*pos->z();
        }
        D = Lambda_11*Lambda_22-Lambda_12*Lambda_12;
        b = (g_1*Lambda_22-g_2*Lambda_12)/D;
        m = (-g_1*Lambda_12+g_2*Lambda_11)/D;
        m_err = sigma*sqrt(Lambda_11*Lambda_11*Lambda_22
                     -2.0*Lambda_11*Lambda_12*Lambda_12
                     +Lambda_12*Lambda_12*Lambda_11)/D;

        chi2 = 0.0;
        for(unsigned int i=0; i<entry.size(); i++) {
            const Hep3Vector *pos(entry[i]);
            chi2 = chi2+pow((pos->y()-b-m*pos->z())/sigma, 2);
        }

// track coordinate system //
//         Straight_line track_candidate(Hep3Vector(0.0, b, 0.0),
//                                       Hep3Vector(0.0, m, 1.0));
// chamber coordinate system //
        Straight_line track_candidate(b*y,
                                      Hep3Vector(0.0, L0_track.my()+m, 1.0));
        m_segment.push_back(SegmentCandidate(track_candidate,
                                static_cast<unsigned int>(max), chi2, m_err));

    }

    // return only 1 candidate with max. hits if flag is activated
    if (m_single_candidate) {
      if(m_segment.size()>1){
        sort(m_segment.begin(), m_segment.end());
        m_segment.erase(m_segment.begin(), m_segment.end()-1);
      }
    }

    return m_segment;
}

//*****************************************************************************
