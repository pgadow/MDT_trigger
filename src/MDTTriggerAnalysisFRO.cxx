//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                       MDTTriggerAnalysisFRO                        ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>
#include <fstream>
#include <ctime>

// MDTTrigger //
#include "MDTTriggerAnalysisFRO.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;
using namespace CLHEP;

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

MDTTriggerAnalysisFRO::MDTTriggerAnalysisFRO(const std::string & ROOT_outfile_name,
                                       const double & bin_width) {

    m_p_evnt_generator = new MDTChamberFROEventGenerator(15.0,
                                                      0.4,
                                                      36,
                                                      3,
                                                      121.0,
                                                      "../share/default.rt");

    m_p_trigger_alg = new MDTL1TriggerAlgorithm(-18.0*30.0, 18.0*30.0,
                                                bin_width);

    m_p_rnd = new TRandom3(time(0));

    m_p_tfile = new TFile(ROOT_outfile_name.c_str(), "RECREATE");

    m_p_tree = new TTree("tree", "tree");

    m_p_tree->Branch("m_gen", &m_gen);
    m_p_tree->Branch("b_gen", &b_gen);

    m_p_tree->Branch("nb_muon_hits_on_track", &nb_muon_hits_on_track);

    m_p_tree->Branch("m_L0", &m_L0);

    m_p_tree->Branch("nb_candidates", &nb_candidates);
    m_p_tree->Branch("nb_hits", &nb_hits);
    m_p_tree->Branch("chi2", &chi2);
    m_p_tree->Branch("m_rec", &m_rec);
    m_p_tree->Branch("b_rec", &b_rec);
    m_p_tree->Branch("m_rec_err", &m_rec_err);

}

//*****************************************************************************

//::::::::::::::::::::
//:: METHOD analyse ::
//::::::::::::::::::::

void MDTTriggerAnalysisFRO::analyse(const unsigned int & nb_tracks,
                                const double & b_gen_min,
                                const double & b_gen_max,
                                const double & m_gen_min,
                                const double & m_gen_max,
                                const double & occupancy,
                                const double & ang_res,
                                const double & t_resolution) {

///////////////
// VARIABLES //
///////////////

    vector<SegmentCandidate> rec_tracks;
    vector<SegmentCandidate> rec_tracks_plus;
    vector<SegmentCandidate> rec_tracks_minus;
    vector<SegmentCandidate> rec_final;

///////////////////////////
// EVENT GENERATION LOOP //
///////////////////////////

    for (unsigned int evt=0; evt<nb_tracks; evt++) {

// event information //
        if (evt%1000==0) {
            cout << "Number of generated tracks: " << evt << endl;
        }

// track generation //
        b_gen = m_p_rnd->Uniform(b_gen_min, b_gen_max);
        m_gen = m_p_rnd->Uniform(m_gen_min, m_gen_max);
        m_L0 = m_p_rnd->Gaus(m_gen, ang_res);
        Straight_line gen_track(Hep3Vector(0.0, b_gen, 0.0),
                                Hep3Vector(0.0, m_gen, 1.0));
        Straight_line L0_track(Hep3Vector(0.0, b_gen, 0.0),
                               Hep3Vector(0.0, m_L0, 1.0));
//         Straight_line L0_track_plus(Hep3Vector(0.0, b_gen, 0.0),
//                                Hep3Vector(0.0, m_L0+2.0*ang_res, 1.0));
//         Straight_line L0_track_minus(Hep3Vector(0.0, b_gen, 0.0),
//                                Hep3Vector(0.0, m_L0-2.0*ang_res, 1.0));

// MDT chamber event generation //
        vector<BareMDTHit> hit_pattern(
                            m_p_evnt_generator->generate(gen_track,
                                                         1,
                                                         -200.0,
                                                         occupancy,
                                                         30.0,
                                                         0.0,
                                                         t_resolution,
                                                         nb_muon_hits_on_track));

// MDT L1 trigger algorithm //
        rec_tracks = m_p_trigger_alg->getSegmentCandidates(
                                        hit_pattern,
                                        L0_track,
                                        t_resolution,
                                        m_p_evnt_generator->rt());
//         rec_tracks_plus = m_p_trigger_alg->getSegmentCandidates(
//                                         hit_pattern,
//                                         L0_track_plus,
//                                         t_resolution,
//                                         m_p_evnt_generator->rt());
//         rec_tracks_minus = m_p_trigger_alg->getSegmentCandidates(
//                                         hit_pattern,
//                                         L0_track_minus,
//                                         t_resolution,
//                                         m_p_evnt_generator->rt());

// take all results //
        rec_final.clear();
        for (unsigned int k=0; k<rec_tracks.size(); k++) {
            rec_final.push_back(rec_tracks[k]);
        }
//         for (unsigned int k=0; k<rec_tracks_plus.size(); k++) {
//             rec_final.push_back(rec_tracks_plus[k]);
//         }
//         for (unsigned int k=0; k<rec_tracks_minus.size(); k++) {
//             rec_final.push_back(rec_tracks_minus[k]);
//         }

// store results //
        nb_candidates = rec_final.size();
        nb_hits = vector<int>(nb_candidates);
        chi2 = vector<double>(nb_candidates);
        m_rec = vector<double>(nb_candidates);
        b_rec = vector<double>(nb_candidates);
        m_rec_err = vector<double>(nb_candidates);
        for (unsigned int k=0; k<rec_final.size(); k++) {
            nb_hits[k] = rec_final[k].getNumberOfHits();
            chi2[k] = rec_final[k].getChi2();
            m_rec[k] = rec_final[k].getSegment().my();
            b_rec[k] = rec_final[k].getSegment().by();
            m_rec_err[k] = rec_final[k].getSlopeError();
        }
        m_p_tree->Fill();

    }

///////////////////////////////
// WRITE AND CLOSE ROOT FILE //
///////////////////////////////

    m_p_tfile->Write();
    m_p_tfile->Close();

    return;    

}
