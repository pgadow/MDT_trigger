//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                        ExtendedHistogram                       ::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>
#include <cstdlib>

// MDTTrigger //
#include "ExtendedHistogram.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

template <class EntryProvider>

ExtendedHistogram<EntryProvider>::ExtendedHistogram(
                      const std::string & hist_name,
                      const std::string & hist_title,
                      const unsigned int & nb_bins,
                      const double & x_min,
                      const double & x_max) : TH1F(hist_name.c_str(),
                                                   hist_title.c_str(),
                                                   nb_bins,
                                                   x_min,
                                                   x_max) {

    m_ep = vector< vector<const EntryProvider *> >(nb_bins+2);

}

//*****************************************************************************

//::::::::::::::::::::::::::::::
//:: METHOD getEntryProviders ::
//::::::::::::::::::::::::::::::

template <class EntryProvider>

const std::vector<const EntryProvider *>
    ExtendedHistogram<EntryProvider>::getEntryProvider(
                                                const int & bin_number) const {

//////////////////////
// CHECK BIN NUMBER //
//////////////////////

    if (bin_number<0 || bin_number>this->GetNbinsX()+1) {
        cerr << "Class ExtendedHistogram, method getEntryProviders: ERROR!\n"
             << "Illegal bin number " << bin_number << endl;
        exit(1);
    }

/////////////////////////////
// RETURN REQUESTED VECTOR //
/////////////////////////////

    return m_ep[bin_number];

}

//*****************************************************************************

//:::::::::::::::::
//:: METHOD Fill ::
//:::::::::::::::::

template <class EntryProvider>

void ExtendedHistogram<EntryProvider>::Fill(
                const double & x, const double & w, const EntryProvider * ep) {

/////////////////////////////
// FILL THE ROOT HISTOGRAM //
/////////////////////////////

    Fill(x, w);

///////////////////////////////////////////
// STORE A POINTER TO THE ENTRY PROVIDER //
///////////////////////////////////////////

    m_ep[FindBin(x)]->push_back(ep);

    return;

}

//*****************************************************************************

//::::::::::::::::::
//:: METHOD Clear ::
//::::::::::::::::::

template <class EntryProvider>

void ExtendedHistogram<EntryProvider>::Clear(void) {

    Reset();
    for (unsigned int k=0; k<m_ep.size(); k++) {
        m_ep[k].clear();
    }

    return;

}
