//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                       MDTTriggerAnalysis                        ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <ctime>
#include <chrono>


// MDTTrigger //
#include "MDTTriggerAnalysis.h"
#include "MDTChamberParameters.h"

// ROOT //
#include "TFile.h"
#include "TH1F.h"
#include "TTree.h"
#include "TreeReader.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;
using namespace CLHEP;

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

// Simulation Constructor //
MDTTriggerAnalysis::MDTTriggerAnalysis(const std::string & ROOT_outfile_name,
                                       const std::string & simulationfile,
                                       const std::string & algorithmfile,
                                       const std::string & parameterfile) {

    initialize(ROOT_outfile_name, algorithmfile, parameterfile);

    m_p_evnt_generator = new MDTChamberEventGenerator(m_par.tube_radius,//15.0,
                                                      m_par.tube_wall,//0.4,
                                                      m_par.tubes_per_layer,//36,
                                                      m_par.layers_per_multilayer,//3,
                                                      m_par.multilayers, //2,
                                                      m_par.y_offset, //0.,
                                                      m_par.z_offset, //0.,
                                                      m_par.spacer_thickness,//121.0,
                                                      m_rt_file_name);//"../share/default.rt"

    read_simulationfile(simulationfile);

    // kill dead wires //
    for (unsigned int wire = 0; wire < m_par.dead_wires.size(); ++wire) {
        m_p_evnt_generator->kill_wire(m_par.dead_wires[wire]);
    }

    // set tree pointer to NULL for MC mode
    m_tree = NULL;
    tr = NULL;
}

// Data Constructor //
MDTTriggerAnalysis::MDTTriggerAnalysis(const std::string & ROOT_outfile_name,
                                       TTree* tree,
                                       const std::string & algorithmfile,
                                       const std::string & parameterfile){

    initialize(ROOT_outfile_name, algorithmfile, parameterfile);

    // use tree to obtain events
    m_tree = tree;
    tr = new TreeReader(m_tree);
}


//*****************************************************************************

//:::::::::::::::::::::::
//:: METHOD initialize ::
//:::::::::::::::::::::::

void MDTTriggerAnalysis::initialize(const std::string & ROOT_outfile_name,
                               const std::string & algorithmfile,
                               const std::string & parameterfile) {

    // Read parameters from file //
    read_algorithmfile(algorithmfile);
    m_par = read_parameterfile(parameterfile);

    // Initialize Random Number Generator //
    m_p_rnd = new TRandom3(time(0));

    // Rt Relation //
    m_rt_file_name = "../share/default.rt";
    std::cout << "Rt relation is read from " << m_rt_file_name << std::endl;
    std::ifstream rt_file(m_rt_file_name.c_str());
    if (rt_file.fail()) {
        cerr << "Rt relation in method initialize: ERROR!\n"
             << "Could not open file " << m_rt_file_name << ".\n";
        exit(1);
    }
    m_rt.read_rt(rt_file);

    // Set up ROOT stuff (e.g. Output tree) //
    initialize_ROOTObjects(ROOT_outfile_name);

    // Set Simulation Output //
    m_store_simulation_output = false;
    m_simulation_output = NULL;
    m_simulation_tree = NULL;


    // Set up Algorithms //
    m_p_trigger_alg = new MDTL1TriggerAlgorithm(\
                  -static_cast<float>(m_par.tubes_per_layer)*m_par.tube_radius+\
                  m_par.y_offset,\
                   static_cast<float>(m_par.tubes_per_layer)*m_par.tube_radius+\
                  m_par.y_offset,\
                  m_algorithm_Hist_bin_width, m_algorithm_Hist_single_track);


    m_p_trigger_alg_2D = new MDTL1Hough2DTriggerAlgorithm(\
                 -static_cast<float>(m_par.tubes_per_layer)*m_par.tube_radius+\
                 m_par.y_offset,\
                  static_cast<float>(m_par.tubes_per_layer)*m_par.tube_radius+\
                 m_par.y_offset,\
                      m_algorithm_2DHough_bin_width);

    m_p_trigger_alg_semi = new MDTL1FastTriggerAlgorithm(\
                      m_algorithm_Fast_m_width,
                      m_algorithm_Fast_b_width,
                      m_algorithm_Fast_search_width
                      );

    m_p_trigger_alg_fast_semi = new MDTL1VeryFastTriggerAlgorithm(\
                      m_algorithm_VeryFast_m_width,
                      m_algorithm_VeryFast_b_width
                      );

    return;
   }

//*****************************************************************************


//:::::::::::::::::::::::::::::::::::
//:: METHOD initialize_ROOTObjects ::
//:::::::::::::::::::::::::::::::::::

void MDTTriggerAnalysis::initialize_ROOTObjects(\
                                         const std::string & ROOT_outfile_name){

    // Create Output File //
    m_p_tfile = new TFile(ROOT_outfile_name.c_str(), "RECREATE");

    // Create Output Tree //
    m_p_tree = new TTree("tree", "tree");

    m_p_tree->Branch("m_gen", &m_gen);
    m_p_tree->Branch("b_gen", &b_gen);
    m_p_tree->Branch("m_L0", &m_L0);


    // 0.) Histogram-based Pattern Recognition
    if(m_algorithm_Hist){
      m_p_tree->Branch("a0_nb_candidates", &a0_nb_candidates);
      m_p_tree->Branch("a0_nb_hits", &a0_nb_hits);
      m_p_tree->Branch("a0_chi2", &a0_chi2);
      m_p_tree->Branch("a0_m_rec", &a0_m_rec);
      m_p_tree->Branch("a0_b_rec", &a0_b_rec);
      m_p_tree->Branch("a0_m_rec_err", &a0_m_rec_err);
      m_p_tree->Branch("a0_reco_time", &a0_reco_time);
    }

    // 1.) 2-D Binned Hough Transform
    if(m_algorithm_2DHough){
      m_p_tree->Branch("a1_nb_candidates", &a1_nb_candidates);
      m_p_tree->Branch("a1_nb_hits", &a1_nb_hits);
      m_p_tree->Branch("a1_chi2", &a1_chi2);
      m_p_tree->Branch("a1_m_rec", &a1_m_rec);
      m_p_tree->Branch("a1_b_rec", &a1_b_rec);
      m_p_tree->Branch("a1_m_rec_err", &a1_m_rec_err);
      m_p_tree->Branch("a1_reco_time", &a1_reco_time);
    }

    // 2.) Fast Track Finder (Fast Algorithm)
    if(m_algorithm_Fast){
      m_p_tree->Branch("a2_nb_candidates", &a2_nb_candidates);
      m_p_tree->Branch("a2_nb_hits", &a2_nb_hits);
      m_p_tree->Branch("a2_chi2", &a2_chi2);
      m_p_tree->Branch("a2_m_rec", &a2_m_rec);
      m_p_tree->Branch("a2_b_rec", &a2_b_rec);
      m_p_tree->Branch("a2_m_rec_err", &a2_m_rec_err);
      m_p_tree->Branch("a2_reco_time", &a2_reco_time);
    }

    // 3.) Very Fast Track Finder (Fast Algorithm without fitting)
    if(m_algorithm_VeryFast){
      m_p_tree->Branch("a3_nb_candidates", &a3_nb_candidates);
      m_p_tree->Branch("a3_nb_hits", &a3_nb_hits);
      m_p_tree->Branch("a3_chi2", &a3_chi2);
      m_p_tree->Branch("a3_m_rec", &a3_m_rec);
      m_p_tree->Branch("a3_b_rec", &a3_b_rec);
      m_p_tree->Branch("a3_m_rec_err", &a3_m_rec_err);
      m_p_tree->Branch("a3_reco_time", &a3_reco_time);
    }

    if(m_par.store_input){
      m_p_tree->Branch("std_b", &m_data_std_b);
      m_p_tree->Branch("std_m", &m_data_std_m);
      m_p_tree->Branch("std_L0", &m_sim_std_L0);
      m_p_tree->Branch("std_has_track", &m_data_std_has_track);
      m_p_tree->Branch("FRO_t", &m_data_FRO_time);
      m_p_tree->Branch("FRO_wy", &m_data_FRO_wy);
      m_p_tree->Branch("FRO_wz", &m_data_FRO_wz);
      m_p_tree->Branch("tube", &m_data_tube);
      m_p_tree->Branch("layer", &m_data_layer);
      m_p_tree->Branch("multilayer", &m_data_multilayer);

      m_p_tree->Branch("std_nb_hits", &m_data_std_nb_hits);
      m_p_tree->Branch("std_channel", &m_data_std_channel);
      m_p_tree->Branch("std_t", &m_data_std_t);
      m_p_tree->Branch("std_r", &m_data_std_r);
      m_p_tree->Branch("std_res", &m_data_std_res);
      m_p_tree->Branch("std_wy", &m_data_std_wy);
      m_p_tree->Branch("std_wz", &m_data_std_wz);
      m_p_tree->Branch("std_nb_track_hits", &m_data_std_nb_track_hits);
      m_p_tree->Branch("std_CL", &m_data_std_CL);
      m_p_tree->Branch("std_d", &m_data_std_d);
      m_p_tree->Branch("FRO_nb_hits", &m_data_FRO_nb_hits);
      m_p_tree->Branch("FRO_channel", &m_data_FRO_channel);
      m_p_tree->Branch("FRO_d", &m_data_FRO_d);
      m_p_tree->Branch("FRO_r", &m_data_FRO_r);
    }

    return;
}


//*****************************************************************************

//:::::::::::::::::::::::::::::
//:: METHOD store_simulation ::
//:::::::::::::::::::::::::::::

void MDTTriggerAnalysis::store_simulation(const std::string & output_path){

  m_store_simulation_output = true;
  m_simulation_output = new TFile(output_path.c_str(), "RECREATE");
  m_simulation_tree = new TTree("MDTtriggerTree", "MDTtriggerTree");


  m_simulation_tree->Branch("b", &m_sim_std_b);
  m_simulation_tree->Branch("m", &m_sim_std_m);
  m_simulation_tree->Branch("L0", &m_sim_std_L0);
  m_simulation_tree->Branch("has_track", &m_sim_std_has_track);
  m_simulation_tree->Branch("t", &m_sim_FRO_time);
  m_simulation_tree->Branch("r", &m_sim_FRO_radius);
  m_simulation_tree->Branch("wy", &m_sim_FRO_wy);
  m_simulation_tree->Branch("wz", &m_sim_FRO_wz);
  m_simulation_tree->Branch("tube", &m_sim_tube);
  m_simulation_tree->Branch("layer", &m_sim_layer);
  m_simulation_tree->Branch("multilayer", &m_sim_multilayer);

  // m_simulation_tree = new TTree("FRO_analysis_tree", "FRO_analysis_tree");
  // m_simulation_tree->Branch("std_b", &m_sim_std_b);
  // m_simulation_tree->Branch("std_m", &m_sim_std_m);
  // m_simulation_tree->Branch("std_L0", &m_sim_std_L0);
  // m_simulation_tree->Branch("std_has_track", &m_sim_std_has_track);
  // m_simulation_tree->Branch("FRO_t", &m_sim_FRO_time);
  // m_simulation_tree->Branch("FRO_r", &m_sim_FRO_radius);
  // m_simulation_tree->Branch("FRO_wy", &m_sim_FRO_wy);
  // m_simulation_tree->Branch("FRO_wz", &m_sim_FRO_wz);
  // m_simulation_tree->Branch("tube", &m_sim_tube);
  // m_simulation_tree->Branch("layer", &m_sim_layer);
  // m_simulation_tree->Branch("multilayer", &m_sim_multilayer);


  // m_sim_dummy_int = 0;
  // m_sim_dummy_double = 0.;
  // m_sim_dummy_vector_int.push_back(0);
  // m_sim_dummy_vector_double.push_back(0.);
  // m_simulation_tree->Branch("std_nb_hits", &m_sim_dummy_int);
  // m_simulation_tree->Branch("std_channel", &m_sim_dummy_vector_int);
  // m_simulation_tree->Branch("std_t", &m_sim_dummy_vector_double);
  // m_simulation_tree->Branch("std_r", &m_sim_dummy_vector_double);
  // m_simulation_tree->Branch("std_res", &m_sim_dummy_vector_double);
  // m_simulation_tree->Branch("std_wy", &m_sim_dummy_vector_double);
  // m_simulation_tree->Branch("std_wz", &m_sim_dummy_vector_double);
  // m_simulation_tree->Branch("std_nb_track_hits", &m_sim_dummy_int);
  // m_simulation_tree->Branch("std_CL", &m_sim_dummy_double);
  // m_simulation_tree->Branch("std_d", &m_sim_dummy_vector_double);
  // m_simulation_tree->Branch("FRO_nb_hits", &m_sim_dummy_int);
  // m_simulation_tree->Branch("FRO_channel", &m_sim_dummy_vector_int);
  // m_simulation_tree->Branch("FRO_d", &m_sim_dummy_vector_double);

  m_p_tfile->cd();
  return;
}



//*****************************************************************************

//:::::::::::::::::::::::::::
//:: METHOD generate_event ::
//:::::::::::::::::::::::::::

MDTTriggerEvent MDTTriggerAnalysis::generate_event(\
                                const double & ang_res,
                                const double & t_resolution) {
  MDTTriggerEvent event;

  // track generation //
  double m_gen = 0;
  double b_gen = 0;

  // generate uniform distributions //
  m_gen = m_p_rnd->Uniform(m_m_gen_min, m_m_gen_max);
  b_gen = m_p_rnd->Uniform(m_b_gen_min, m_b_gen_max);

  // weight distributions with root histogram, if specified //
  if(m_use_b_weight){
    b_gen = m_b_distribution->GetRandom();
  }

  if(m_use_m_weight){
    m_gen = m_m_distribution->GetRandom();
  }

  double m_L0 = m_p_rnd->Gaus(m_gen, ang_res);
  m_sim_std_L0 = m_L0;
  event.muon_track = Straight_line(Hep3Vector(0.0, b_gen, 0.0),
                                   Hep3Vector(0.0, m_gen, 1.0));
  event.L0_track   = Straight_line(Hep3Vector(0.0, b_gen, 0.0),
                                   Hep3Vector(0.0, m_L0, 1.0));

  // MDT chamber event generation //
  event.hit_pattern=vector<BareMDTHit>(\
                      m_p_evnt_generator->generate(event.muon_track,
                                                   1,//with delta electrons
                                                   m_par.dead_time,//dead time
                                                   m_occupancy,
                                                   abs(m_b_gen_max -
                                                       m_b_gen_min)/2.,//60.0,
                                                   0.0,//time shift
                                                   t_resolution));

  if(m_store_simulation_output){
    m_sim_std_has_track = 1;
    m_sim_std_b = b_gen;
    m_sim_std_m = m_gen;
    m_sim_FRO_time.clear();
    m_sim_FRO_radius.clear();
    m_sim_FRO_wy.clear();
    m_sim_FRO_wz.clear();
    m_sim_tube.clear();
    m_sim_layer.clear();
    m_sim_multilayer.clear();
    for (std::vector<MDTTrigger::BareMDTHit>::iterator i = event.hit_pattern.begin();
         i != event.hit_pattern.end(); ++i) {
           m_sim_FRO_time.push_back((*i).getDriftTime());
           m_sim_FRO_radius.push_back(m_rt.r((*i).getDriftTime(), 0));
           m_sim_FRO_wy.push_back((*i).getWirePosition().y());
           m_sim_FRO_wz.push_back((*i).getWirePosition().z());
           m_sim_tube.push_back((*i).getTube());
           m_sim_layer.push_back((*i).getLayer());
           m_sim_multilayer.push_back((*i).getMultiLayer());
    }
    m_simulation_tree->Fill();
  }

  return event;
}


//*****************************************************************************

//:::::::::::::::::::::::
//:: METHOD read_event ::
//:::::::::::::::::::::::

MDTTriggerEvent MDTTriggerAnalysis::read_event(const unsigned int & entry,\
                                               const double & ang_res,
                                               const double & ROI_width){
  MDTTriggerEvent event;

  // check if TreeReader is initialised
  if(tr == NULL){
    return event;
  }

  // retrieve entry
  tr->GetEntry(entry);

  // check if track was found in event, if not, return empty event
  if(!tr->std_has_track){
    return event;
  }

  // read track reconstructed with standard readout //
  double b_std = tr->std_b;
  double m_std = tr->std_m;

  double m_L0 = m_p_rnd->Gaus(m_std, ang_res); // tr->std_L0;
  event.muon_track = Straight_line(Hep3Vector(0.0, b_std, 0.0),
                                   Hep3Vector(0.0, m_std, 1.0));
  event.L0_track   = Straight_line(Hep3Vector(0.0, b_std, 0.0),
                                   Hep3Vector(0.0, m_L0, 1.0));

  // create BareMDTHits //
  std::vector<BareMDTHit> hits;
  if(m_par.use_std == 1){
    for (unsigned int i = 0; i < tr->std_t->size(); i++) {
      double distance_to_muon = event.muon_track.dist_from_line(Hep3Vector(0.0, tr->std_wy->at(i), tr->std_wz->at(i)));
      if (distance_to_muon > ROI_width) {
        continue;
      }
      hits.push_back(BareMDTHit(Hep3Vector(0.0, tr->std_wy->at(i), tr->std_wz->at(i)),\
                                tr->std_t->at(i),\
                                tr->multilayer->at(i),
                                tr->layer->at(i),
                                tr->tube->at(i)
                                ));
    }
  }
  else{
    for (unsigned int i = 0; i < tr->FRO_t->size(); i++) {
      double distance_to_muon = event.muon_track.dist_from_line(Hep3Vector(0.0, tr->FRO_wy->at(i), tr->FRO_wz->at(i)));
      if (distance_to_muon > ROI_width) {
        continue;
      }
      hits.push_back(BareMDTHit(Hep3Vector(0.0, tr->FRO_wy->at(i), tr->FRO_wz->at(i)),\
                                tr->FRO_t->at(i),\
                                tr->multilayer->at(i),
                                tr->layer->at(i),
                                tr->tube->at(i)
                                ));
    }
  }
  event.hit_pattern=hits;

  return event;
}

//*****************************************************************************

//::::::::::::::::::::
//:: METHOD analyse ::
//::::::::::::::::::::

void MDTTriggerAnalysis::analyse(const unsigned int & nb_tracks,
                                 const double & ang_res,
                                 const double & t_resolution) {

///////////////
// VARIABLES //
///////////////

    vector<Straight_line> additional_seeds;
    vector<SegmentCandidate> temp;

    vector<SegmentCandidate> rec_tracks_alg_Hist;
    vector<SegmentCandidate> rec_tracks_alg_2DHough;
    vector<SegmentCandidate> rec_tracks_alg_Fast;
    vector<SegmentCandidate> rec_tracks_alg_VeryFast;



/////////////////
// EVENT  LOOP //
/////////////////
    unsigned int count = 0;
    for (unsigned int evt=0; count<nb_tracks; evt++) {

// event information //
        if (evt%1000==0) {
            cout << "Number of processed tracks/events: " << count
                 << "/"<< evt << endl;
        }

        MDTTriggerEvent event;

        if(m_tree == NULL){
          event = generate_event(ang_res, t_resolution);
        }
        else{
          event = read_event(evt, ang_res, m_par.ROI_width);
        }

        // check if event is empty
        if (event.hit_pattern.size() == 0) {
          continue;
        }
        ++count;
        m_gen = event.muon_track.my();
        b_gen = event.muon_track.by();
        m_L0  = event.L0_track.my();


        // additional seed tracks for algorithm [Binned 2D Hough transform]
        if (m_algorithm_2DHough) {
          additional_seeds.clear();
          int i_max = static_cast<int>(m_algorithm_2DHough_nb_binned_algs);
          additional_seeds.reserve(i_max);
          for (int i = -i_max/2; i < i_max/2+1; ++i) {
              if(i==0){
                continue;
              }
            double extra_slope = (2*i*m_algorithm_2DHough_distance_binned_algs*ang_res)/(static_cast<double>(i_max));
            additional_seeds.push_back(Straight_line(\
              Hep3Vector(0.0, b_gen, 0.0),\
              Hep3Vector(0.0, m_L0 + extra_slope, 1.0)));
          }
        }


        ///////////////////////////////////
        // RUN MDT L1 TRIGGER ALGORITHMS //
        ///////////////////////////////////

        // 0.) Histogram-based Pattern Recognition
        if (m_algorithm_Hist){
          // start clock //
          chrono::high_resolution_clock::time_point t1 = \
                                           chrono::high_resolution_clock::now();
          rec_tracks_alg_Hist = m_p_trigger_alg->getSegmentCandidates(
                                        event.hit_pattern,
                                        event.L0_track,
                                        t_resolution,
                                        m_rt);
          // stop clock and measure time in microseconds //
          chrono::high_resolution_clock::time_point t2 =\
                                           chrono::high_resolution_clock::now();
          m_algorithm_Hist_time =  \
          static_cast<double>(chrono::duration <double, nano> (t2 -t1).count());

        }

        // 1.) 2-D Binned Hough Transform
        if (m_algorithm_2DHough) {
          // start clock //
          chrono::high_resolution_clock::time_point t1 = \
                                          chrono::high_resolution_clock::now();
          rec_tracks_alg_2DHough = m_p_trigger_alg_2D->getSegmentCandidates(
                                      event.hit_pattern,
                                      event.L0_track,
                                      t_resolution,
                                      m_rt);
          for (unsigned int i = 0; i < additional_seeds.size(); ++i){
            temp.clear();
            temp = m_p_trigger_alg_2D->getSegmentCandidates(
                                          event.hit_pattern,
                                          additional_seeds[i],
                                          t_resolution,
                                          m_rt);
            rec_tracks_alg_2DHough.insert(rec_tracks_alg_2DHough.end(),\
                                          temp.begin(), temp.end());
          }
          if(rec_tracks_alg_2DHough.size()>1){
            sort(rec_tracks_alg_2DHough.begin(), rec_tracks_alg_2DHough.end());
            rec_tracks_alg_2DHough.erase(rec_tracks_alg_2DHough.begin(),
                                         rec_tracks_alg_2DHough.end()-1);
          }
          // stop clock and measure time in microseconds //
          chrono::high_resolution_clock::time_point t2 =\
                                         chrono::high_resolution_clock::now();
          m_algorithm_2DHough_time = \
          static_cast<double>(chrono::duration <double, nano> (t2 -t1).count());
        }

        // 2.) Fast Track Finder (Fast Algorithm)
        if (m_algorithm_Fast){
          // start clock //
          chrono::high_resolution_clock::time_point t1 = \
                                          chrono::high_resolution_clock::now();
          rec_tracks_alg_Fast = m_p_trigger_alg_semi->getSegmentCandidates(
                                      event.hit_pattern,
                                      event.L0_track,
                                      t_resolution,
                                      m_rt);
          // stop clock and measure time in microseconds //
          chrono::high_resolution_clock::time_point t2 =\
                                         chrono::high_resolution_clock::now();
          m_algorithm_Fast_time = \
          static_cast<double>(chrono::duration <double, nano> (t2 -t1).count());
        }

        // 3.) Very Fast Track Finder (Fast Algorithm without fitting)
        if (m_algorithm_VeryFast){
          // start clock //
          chrono::high_resolution_clock::time_point t1 = \
                                          chrono::high_resolution_clock::now();
          rec_tracks_alg_VeryFast = m_p_trigger_alg_fast_semi->getSegmentCandidates(
                                      event.hit_pattern,
                                      event.L0_track,
                                      t_resolution,
                                      m_rt);
          // stop clock and measure time in microseconds //
          chrono::high_resolution_clock::time_point t2 =\
                                         chrono::high_resolution_clock::now();
          m_algorithm_VeryFast_time = \
          static_cast<double>(chrono::duration <double, nano> (t2 -t1).count());
        }


        // store results //

        // 0.) Histogram-based Pattern Recognition
        a0_nb_candidates = rec_tracks_alg_Hist.size();
        a0_nb_hits = vector<int>(a0_nb_candidates);
        a0_chi2 = vector<double>(a0_nb_candidates);
        a0_m_rec = vector<double>(a0_nb_candidates);
        a0_b_rec = vector<double>(a0_nb_candidates);
        a0_m_rec_err = vector<double>(a0_nb_candidates);
        a0_reco_time = m_algorithm_Hist_time;
        for (unsigned int k=0; k<rec_tracks_alg_Hist.size(); k++) {
            a0_nb_hits[k] = rec_tracks_alg_Hist[k].getNumberOfHits();
            a0_chi2[k] = rec_tracks_alg_Hist[k].getChi2();
            a0_m_rec[k] = rec_tracks_alg_Hist[k].getSegment().my();
            a0_b_rec[k] = rec_tracks_alg_Hist[k].getSegment().by();
            a0_m_rec_err[k] = rec_tracks_alg_Hist[k].getSlopeError();
        }

        // 1.) 2-D Binned Hough Transform
        a1_nb_candidates = rec_tracks_alg_2DHough.size();
        a1_nb_hits = vector<int>(a1_nb_candidates);
        a1_chi2 = vector<double>(a1_nb_candidates);
        a1_m_rec = vector<double>(a1_nb_candidates);
        a1_b_rec = vector<double>(a1_nb_candidates);
        a1_m_rec_err = vector<double>(a1_nb_candidates);
        a1_reco_time = m_algorithm_2DHough_time;
        for (unsigned int k=0; k<rec_tracks_alg_2DHough.size(); k++) {
            a1_nb_hits[k] = rec_tracks_alg_2DHough[k].getNumberOfHits();
            a1_chi2[k] = rec_tracks_alg_2DHough[k].getChi2();
            a1_m_rec[k] = rec_tracks_alg_2DHough[k].getSegment().my();
            a1_b_rec[k] = rec_tracks_alg_2DHough[k].getSegment().by();
            a1_m_rec_err[k] = rec_tracks_alg_2DHough[k].getSlopeError();
        }

        // 2.) Fast Track Finder (Fast Algorithm)
        a2_nb_candidates = rec_tracks_alg_Fast.size();
        a2_nb_hits = vector<int>(a2_nb_candidates);
        a2_chi2 = vector<double>(a2_nb_candidates);
        a2_m_rec = vector<double>(a2_nb_candidates);
        a2_b_rec = vector<double>(a2_nb_candidates);
        a2_m_rec_err = vector<double>(a2_nb_candidates);
        a2_reco_time = m_algorithm_Fast_time;
        for (unsigned int k=0; k<rec_tracks_alg_Fast.size(); k++) {
            a2_nb_hits[k] = rec_tracks_alg_Fast[k].getNumberOfHits();
            a2_chi2[k] = rec_tracks_alg_Fast[k].getChi2();
            a2_m_rec[k] = rec_tracks_alg_Fast[k].getSegment().my();
            a2_b_rec[k] = rec_tracks_alg_Fast[k].getSegment().by();
            a2_m_rec_err[k] = rec_tracks_alg_Fast[k].getSlopeError();
        }

        // 3.) Very Fast Track Finder (Very Fast Algorithm)
        a3_nb_candidates = rec_tracks_alg_VeryFast.size();
        a3_nb_hits = vector<int>(a3_nb_candidates);
        a3_chi2 = vector<double>(a3_nb_candidates);
        a3_m_rec = vector<double>(a3_nb_candidates);
        a3_b_rec = vector<double>(a3_nb_candidates);
        a3_m_rec_err = vector<double>(a3_nb_candidates);
        a3_reco_time = m_algorithm_Fast_time;
        for (unsigned int k=0; k<rec_tracks_alg_VeryFast.size(); k++) {
            a3_nb_hits[k] = rec_tracks_alg_VeryFast[k].getNumberOfHits();
            a3_chi2[k] = rec_tracks_alg_VeryFast[k].getChi2();
            a3_m_rec[k] = rec_tracks_alg_VeryFast[k].getSegment().my();
            a3_b_rec[k] = rec_tracks_alg_VeryFast[k].getSegment().by();
            a3_m_rec_err[k] = rec_tracks_alg_VeryFast[k].getSlopeError();
        }

        if(m_par.store_input){
          m_data_std_m = tr->std_m;
          m_data_std_b = tr->std_b;
          m_data_std_has_track = tr->std_has_track;
          m_data_FRO_time = *(tr->FRO_t);
          m_data_FRO_wy = *(tr->FRO_wy);
          m_data_FRO_wz = *(tr->FRO_wz);
          m_data_tube = *(tr->tube);
          m_data_layer = *(tr->layer);
          m_data_multilayer = *(tr->multilayer);
          m_data_std_nb_hits = tr->std_nb_hits;
          m_data_std_channel = *(tr->std_channel);
          m_data_std_t = *(tr->std_t);
          m_data_std_r = *(tr->std_r);
          m_data_std_res = *(tr->std_res);
          m_data_std_wy = *(tr->std_wy);
          m_data_std_wz = *(tr->std_wz);
          m_data_std_nb_track_hits = tr->std_nb_track_hits;
          m_data_std_CL = tr->std_CL;
          m_data_std_d = *(tr->std_d);
          m_data_FRO_d = *(tr->FRO_d);
          m_data_FRO_r = *(tr->FRO_r);
        }

        // fill tree //
        m_p_tree->Fill();
    }

///////////////////////////////
// WRITE AND CLOSE ROOT FILE //
///////////////////////////////

    if(m_store_simulation_output){
      m_simulation_output->Write();
      m_simulation_output->Close();
    }

    m_p_tfile->Write();
    m_p_tfile->Close();

    return;
}


//*****************************************************************************

//:::::::::::::::::
//:: METHOD trim ::
//:::::::::::::::::

std::string MDTTriggerAnalysis::trim(std::string const& source,\
                                     char const* delims = " \t\r\n") {
  std::string result(source);
  std::string::size_type index = result.find_last_not_of(delims);
  if(index != std::string::npos)
    result.erase(++index);

  index = result.find_first_not_of(delims);
  if(index != std::string::npos)
    result.erase(0, index);
  else
    result.erase();
  return result;
}


//*****************************************************************************

//::::::::::::::::::::::
//:: METHOD read_file ::
//::::::::::::::::::::::

std::map<std::string,std::vector<std::string> > MDTTriggerAnalysis::read_file(\
                                                const std::string & file_name){

     std::map<std::string, std::vector<std::string> > file_content;

     std::ifstream file(file_name.c_str(), std::ios::in);

     unsigned int posEqual;

     std::string line;
     std::string key;
     std::string value;
     while (std::getline(file,line)) {

       if(! line.length()) continue;

       if (line[0] == '#') continue;

       posEqual=line.find('=');
       key  = trim(line.substr(0,posEqual));
       value = trim(line.substr(posEqual+1));

       // if key is not included in map, initialize empty vector
       if (file_content.find(key) == file_content.end()) {
          std::vector<std::string> empty_vector;
          file_content[key]=empty_vector;
       }

       file_content[key].push_back(value);
     }
     file.close();

     return file_content;
}

//*****************************************************************************

//:::::::::::::::::::::::
//:: METHOD access_map ::
//:::::::::::::::::::::::

std::vector<std::string> MDTTriggerAnalysis::access_map(\
                    std::map<std::string, std::vector<std::string> > m,\
                    std::string key){
    if(m.find(key) == m.end()){
      std::vector<std::string> empty_vector;
      empty_vector.push_back("0");
      return empty_vector;
    }

    return m[key];

}

//*****************************************************************************

//:::::::::::::::::::::::::::::::
//:: METHOD read_parameterfile ::
//:::::::::::::::::::::::::::::::

MDTChamberParameters MDTTriggerAnalysis::read_parameterfile(\
                                             const std::string & parameterfile){

  MDTChamberParameters par;
  std::map<std::string, std::vector<std::string> > parfile = \
                                                      read_file(parameterfile);
  par.tube_radius = atof(access_map(parfile,"tube_radius")[0].c_str());
  par.tube_wall = atof(access_map(parfile,"tube_wall")[0].c_str());
  par.tubes_per_layer = atoi(access_map(parfile,"tubes_per_layer")[0].c_str());
  par.layers_per_multilayer = atoi(access_map(parfile,"layers_per_multilayer")[0].c_str());
  par.multilayers = atoi(access_map(parfile,"multilayers")[0].c_str());
  par.y_offset = atof(access_map(parfile,"y_offset")[0].c_str());
  par.z_offset = atof(access_map(parfile,"z_offset")[0].c_str());
  par.spacer_thickness = atof(access_map(parfile,"spacer_thickness")[0].c_str());
  par.dead_time = atof(access_map(parfile,"dead_time")[0].c_str());
  par.ROI_width = atof(access_map(parfile,"ROI_width")[0].c_str());
  par.use_std = atoi(access_map(parfile,"use_std")[0].c_str());
  par.store_input = atoi(access_map(parfile,"store_input")[0].c_str());

  for (unsigned int wire = 0; wire < access_map(parfile,"dead_wire").size(); ++wire) {
        if(atoi(access_map(parfile,"dead_wire")[wire].c_str()) != 0){
          par.dead_wires.push_back( atoi(access_map(parfile,"dead_wire")[wire].c_str()) );
        }
  }

  std::cout << "Parameters read in from parameter file:" << std::endl
            << "Tube radius: " << par.tube_radius << std::endl
            << "Tube wall: " << par.tube_wall << std::endl
            << "Tubes per layer: " << par.tubes_per_layer << std::endl
            << "Layers per multilayer: " << par.layers_per_multilayer << std::endl
            << "Multilayers: " << par.multilayers << std::endl
            << "y-Offset: " << par.y_offset << std::endl
            << "z-Offset: " << par.z_offset << std::endl
            << "Spacer thickness: " << par.spacer_thickness << std::endl
            << "Dead time: " << par.dead_time << std::endl
            << "RoI width: " << par.ROI_width << std::endl
            << "Use standard readout?: " << par.use_std << std::endl
            << "Store input data?: " << par.store_input << std::endl
            << "Number of dead wires: " << par.dead_wires.size() << std::endl;
  for (unsigned int wire = 0; wire < par.dead_wires.size(); ++wire) {
    std::cout << "\tDead Wire: " << par.dead_wires[wire] << std::endl;
  }

  return par;
}
//*****************************************************************************

//::::::::::::::::::::::::::::::::
//:: METHOD read_algorithmfile ::
//::::::::::::::::::::::::::::::::

void MDTTriggerAnalysis::read_algorithmfile(const std::string& algorithmfile){

  std::map<std::string, std::vector<std::string> > algfile = \
                                                      read_file(algorithmfile);
  m_algorithm_Hist = atoi(access_map(algfile,"algorithm_Hist")[0].c_str());
  m_algorithm_Hist_bin_width = \
                atof(access_map(algfile,"algorithm_Hist_bin_width")[0].c_str());
  m_algorithm_Hist_single_track = \
             atoi(access_map(algfile,"algorithm_Hist_single_track")[0].c_str());

  if(m_algorithm_Hist_bin_width < 0.1){
    std::cout << "Specified bin width in alg0 " << m_algorithm_Hist_bin_width
              << " too small, now set to 0.1" << std::endl;
    m_algorithm_Hist_bin_width = 0.1;
  }


  m_algorithm_2DHough = atoi(access_map(algfile,"algorithm_2DHough")[0].c_str());
  m_algorithm_2DHough_bin_width = \
          atof(access_map(algfile,"algorithm_2DHough_bin_width")[0].c_str());
  m_algorithm_2DHough_nb_binned_algs = \
      atoi(access_map(algfile,"algorithm_2DHough_nb_binned_algs")[0].c_str());
  m_algorithm_2DHough_distance_binned_algs = \
    atof(access_map(algfile,"algorithm_2DHough_distance_binned_algs")[0].c_str());

  if(m_algorithm_2DHough_bin_width < 0.1){
    std::cout << "Specified bin width in alg1 " << m_algorithm_2DHough_bin_width
              << " too small, now set to 0.1" << std::endl;
    m_algorithm_2DHough_bin_width = 0.1;
  }

  m_algorithm_Fast = atoi(access_map(algfile,"algorithm_Fast")[0].c_str());
  m_algorithm_Fast_b_width = \
                atof(access_map(algfile,"algorithm_Fast_b_width")[0].c_str());
  m_algorithm_Fast_m_width = \
                atof(access_map(algfile,"algorithm_Fast_m_width")[0].c_str());
  m_algorithm_Fast_search_width = \
            atof(access_map(algfile,"algorithm_Fast_search_width")[0].c_str());


  m_algorithm_VeryFast = atoi(access_map(algfile,"algorithm_VeryFast")[0].c_str());
  m_algorithm_VeryFast_b_width = \
                atof(access_map(algfile,"algorithm_VeryFast_b_width")[0].c_str());
  m_algorithm_VeryFast_m_width = \
                atof(access_map(algfile,"algorithm_VeryFast_m_width")[0].c_str());



  std::cout << "Read in algorithm parameters: " << std::endl
            << "Algorithm Configuration (on[1]/off[0]): " << std::endl
            << "\t[algorithm_Hist]: " << m_algorithm_Hist << std::endl
            << "\t\tBin width: " << m_algorithm_Hist_bin_width << std::endl

            << "\t[algorithm_2DHough]: " << m_algorithm_2DHough << std::endl
            << "\t\tBin width: " << m_algorithm_2DHough_bin_width << std::endl
            << "\t\tNumber of Algorithms: " \
                << m_algorithm_2DHough_nb_binned_algs+1 << std::endl
            << "\t\tDistance between algorithm starting values: "\
                << m_algorithm_2DHough_distance_binned_algs << std::endl

            << "\t[algorithm_Fast]: " << m_algorithm_Fast << std::endl
            << "\t\tAcceptance in b: " << m_algorithm_Fast_b_width << std::endl
            << "\t\tAcceptance in m: " << m_algorithm_Fast_m_width << std::endl
            << "\t\tSearch Width: " << m_algorithm_Fast_search_width<<std::endl

            << "\t[algorithm_VeryFast]: " << m_algorithm_VeryFast << std::endl
            << "\t\tAcceptance b: " << m_algorithm_VeryFast_b_width << std::endl
            << "\t\tAcceptance m: " << m_algorithm_VeryFast_m_width << std::endl;

      return;
}
//*****************************************************************************

//::::::::::::::::::::::::::::::::
//:: METHOD read_simulationfile ::
//::::::::::::::::::::::::::::::::

void MDTTriggerAnalysis::read_simulationfile(const std::string& simulationfile){


    std::map<std::string, std::vector<std::string> > simfile = \
                                                      read_file(simulationfile);

    m_b_gen_min = atof(access_map(simfile,"b_gen_min")[0].c_str());
    m_b_gen_max = atof(access_map(simfile,"b_gen_max")[0].c_str());
    m_m_gen_min = atof(access_map(simfile,"m_gen_min")[0].c_str());
    m_m_gen_max = atof(access_map(simfile,"m_gen_max")[0].c_str());
    m_occupancy = atof(access_map(simfile,"occupancy")[0].c_str());

    std::cout << "Read in simulation parameters:" << std::endl
                << " Occupancy: " << m_occupancy << std::endl;

    m_rootfile_name = access_map(simfile,"rootfile")[0];
    m_hist_b_name = access_map(simfile,"hist_b")[0];
    m_hist_m_name = access_map(simfile,"hist_m")[0];

    if(m_hist_b_name.length() == 1){
      m_use_b_weight = false;
      std::cout << "Uniform distribution in b with" << std::endl
                  << "\tb_min: " << m_b_gen_min << std::endl
                  << "\tb_max: " << m_b_gen_max << std::endl;
    }
    else{
      m_use_b_weight = true;
      TFile* file = TFile::Open(m_rootfile_name.c_str());
      m_b_distribution = (TH1F*) file->Get(m_hist_b_name.c_str());

      m_b_gen_min = m_b_distribution->GetXaxis()->GetBinLowEdge(\
                                      m_b_distribution->GetXaxis()->GetFirst());
      m_b_gen_max = m_b_distribution->GetXaxis()->GetBinUpEdge(\
                                      m_b_distribution->GetXaxis()->GetLast());

      std::cout << "Weighted distribution in b taken from " << std::endl
                << m_rootfile_name << "/" << m_hist_b_name << " with " << std::endl
                << "\tb_min: " << m_b_gen_min << std::endl
                << "\tb_max: " << m_b_gen_max << std::endl;
    }

    if(m_hist_m_name.length() == 1){
      m_use_m_weight = false;
      std::cout << "Uniform distribution in m with" << std::endl
                  << "\tm_min: " << m_m_gen_min << std::endl
                  << "\tm_max: " << m_m_gen_max << std::endl;
    }
    else{
      m_use_m_weight = true;
      TFile* file = TFile::Open(m_rootfile_name.c_str());
      m_m_distribution = (TH1F*) file->Get(m_hist_m_name.c_str());

      m_m_gen_min = m_m_distribution->GetXaxis()->GetBinLowEdge(\
                                      m_m_distribution->GetXaxis()->GetFirst());
      m_m_gen_max = m_m_distribution->GetXaxis()->GetBinUpEdge(\
                                      m_m_distribution->GetXaxis()->GetLast());

      std::cout << "Weighted distribution in m taken from " << std::endl
                << m_rootfile_name << "/" << m_hist_m_name << " with " << std::endl
                << "\tm_min: " << m_m_gen_min << std::endl
                << "\tm_max: " << m_m_gen_max << std::endl;
    }

  return;
}
