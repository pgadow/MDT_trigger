//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 22.12.1998, AUTHOR: OLIVER KORTNER
// Modified: 11.01.1999: METHOD sign_dist_from corrected.
//           03.02.1999, method dist_from_line for points added. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//////////////////////////////////////////////////////////////////////
// IMPLEMENTATION OF THE METHODS DEFINED IN THE CLASS Straight_line //
//////////////////////////////////////////////////////////////////////

//////////////////
// HEADER FILES //
//////////////////

#include "Straight_line.h"

////////////////////////
// NAMESPACE SETTINGS //
////////////////////////

using namespace std;

//*****************************************************************************

/////////////////////////////
// METHOD print_parameters //
/////////////////////////////

void Straight_line::print_parameters(void) const {

	cout << "position vector: " 
		<< "(" << position.X()
		<< "," << position.Y()
		<< "," << position.Z()
		<< ")" << endl;
	cout << "direction vector: " 
		<< "(" << direction.X()
		<< "," << direction.Y()
		<< "," << direction.Z()
		<< ")" << endl;

	return;

}

//*****************************************************************************

//////////////////////////
// METHOD point_on_line //
//////////////////////////

const TVector3 & Straight_line::point_on_line(const double lambda) const {

/////////////////////////
// AUXILIARY VARIABLES //
/////////////////////////

	TVector3 p;

/////////////
// PROGRAM //
/////////////

	p = position + lambda*direction;

	return p;

}

//*****************************************************************************

///////////////////////////
// METHOD sign_dist_from //
///////////////////////////

double Straight_line::sign_dist_from(const Straight_line & h) const {

/////////////////////////
// AUXILIARY VARIABLES //
/////////////////////////

	TVector3 a = position, u = direction;
					// position and direction vectors of g
	TVector3 b = h.position_vector(), v = h.direction_vector();
					// position and direction vectors of h
	TVector3 n; // normal vector of plane spanned by g and h
	TVector3 d; // distance vector

/////////////
// PROGRAM //
/////////////

////////////////////////
// collinearity check //
////////////////////////

	n = u.cross(v);
	d = a-b;
	if (n*n == 0.0) {
		// straight lines are parallel
		// no sign given
		return sqrt(d*d-(u.Unit()*d)*(u.Unit()*d));
	}
	
	return (d*(n.Unit()));

}

//*****************************************************************************

///////////////////////////
// METHOD dist_from_line //
///////////////////////////

double Straight_line::dist_from_line(const TVector3 & point) const {

/////////////////////////
// AUXILIARY VARIABLES //
/////////////////////////

	TVector3 u = direction;

////////////////////////
// CALCULATE DISTANCE //
////////////////////////

	return sqrt((point-position)*(point-position) -
		((point-position)*u.Unit())*((point-position)*u.Unit()));

}

//*****************************************************************************

///////////////
// METHOD mx //
///////////////

double Straight_line::mx(void) const {

	if (direction.Z() == 0.0) {
		return 0.0;
	}
	return direction.X()/direction.Z();

}

//*****************************************************************************

///////////////
// METHOD bx //
///////////////

double Straight_line::bx(void) const {

	if (direction.Z() == 0.0) {
		cerr << "b_x not uniquely defined." << endl;
		return position.X();
	}
	return (position.X() - position.Z()*direction.X()/direction.Z());
					
}

//*****************************************************************************

///////////////
// METHOD my //
///////////////

double Straight_line::my(void) const {

	if (direction.Z() == 0.0) {
		return 0.0;
	}
	return direction.Y()/direction.Z();

}

//*****************************************************************************

///////////////
// METHOD by //
///////////////

double Straight_line::by(void) const {

	if (direction.Z() == 0.0) {
		cerr << "b_y not uniquely defined." << endl;
		return position.Y();
	}
	return (position.Y() - position.Z()*direction.Y()/direction.Z());

}

//*****************************************************************************

/////////////////////////
// METHOD set_position //
/////////////////////////

void Straight_line::set_position(const TVector3 & new_pos) {

	position = new_pos;
	return;

}

//*****************************************************************************

//////////////////////////
// METHOD set_direction //
//////////////////////////

void Straight_line::set_direction(const TVector3 & new_dir) {

	direction = new_dir;
	return;

}

//*****************************************************************************

////////////////////////////////////
// METHOD intersection_with_plane //
////////////////////////////////////

bool Straight_line::intersection_with_plane(const TVector3 &norm, 
            const TVector3 &point,
			TVector3 & intersection) const {
            
// check if plane || line
	double nd;
	if((nd=norm*direction)==0) return false;

// calculate line parameter for intersection point
	double lambda=(norm*point-norm*position)/nd;

// get intersection Point
	intersection=position+lambda*direction;	
	
    return true;

}
