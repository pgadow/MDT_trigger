//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 24.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                           BareMDTHit                            ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>

// MDTTrigger //
#include "BareMDTHit.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;
using namespace CLHEP;

//*****************************************************************************

//:::::::::::::::::::::::::
//:: DEFAULT CONSTRUCTOR ::
//:::::::::::::::::::::::::

BareMDTHit::BareMDTHit(void) {
}


//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

BareMDTHit::BareMDTHit(const Hep3Vector & wire_pos, const double & t_drift) {

    m_w_pos = wire_pos;
    m_t_drift = t_drift;
    m_r_drift = -1;
    m_multilayer = -1;
    m_layer = -1;
    m_tube = -1;

}

//*****************************************************************************



//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

BareMDTHit::BareMDTHit(const Hep3Vector & wire_pos, const double & t_drift,\
                const int & multilayer, const int & layer, const int & tube) {

    m_w_pos = wire_pos;
    m_t_drift = t_drift;
    m_r_drift = -1;
    m_multilayer = multilayer;
    m_layer = layer;
    m_tube = tube;

}

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

BareMDTHit::BareMDTHit(const Hep3Vector & wire_pos, const double & t_drift,\
                       const Rt_relation & rt) {

    m_w_pos = wire_pos;
    m_t_drift = t_drift;
    m_r_drift = rt.r(t_drift,0);
    m_multilayer = -1;
    m_layer = -1;
    m_tube = -1;

}

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

BareMDTHit::BareMDTHit(const Hep3Vector & wire_pos, const double & t_drift,\
                       const Rt_relation & rt,\
                  const int & multilayer, const int & layer, const int & tube) {

    m_w_pos = wire_pos;
    m_t_drift = t_drift;
    m_r_drift = rt.r(t_drift,0);
    m_multilayer = multilayer;
    m_layer = layer;
    m_tube = tube;

}
//*****************************************************************************

//::::::::::::::::::::::::::::
//:: METHOD getWirePosition ::
//::::::::::::::::::::::::::::

const Hep3Vector & BareMDTHit::getWirePosition(void) const {

    return m_w_pos;

}

//*****************************************************************************

//:::::::::::::::::::::::::
//:: METHOD getDriftTime ::
//:::::::::::::::::::::::::

double BareMDTHit::getDriftTime(void) const {

    return m_t_drift;

}

//*****************************************************************************

//:::::::::::::::::::::::::::
//:: METHOD getDriftRadius ::
//:::::::::::::::::::::::::::

double BareMDTHit::getDriftRadius(void) const {

    return m_r_drift;

}

//*****************************************************************************

//:::::::::::::::::::::::::
//:: METHOD getMultiLayer::
//:::::::::::::::::::::::::

int BareMDTHit::getMultiLayer(void) const {

    return m_multilayer;

}

//*****************************************************************************

//::::::::::::::::::::
//:: METHOD getLayer::
//::::::::::::::::::::

int BareMDTHit::getLayer(void) const {

    return m_layer;

}

//*****************************************************************************

//::::::::::::::::::::
//:: METHOD getTube ::
//::::::::::::::::::::

int BareMDTHit::getTube(void) const {

    return m_tube;

}

//*****************************************************************************

//::::::::::::::::::::::::::
//:: METHOD setRtRelation ::
//::::::::::::::::::::::::::

void BareMDTHit::setRtRelation(const Rt_relation & rt) {

    m_r_drift = rt.r(m_t_drift,0);

    return;

}

//*****************************************************************************

//:::::::::::::::::::::::::
//:: METHOD setMultiLayer::
//:::::::::::::::::::::::::

void BareMDTHit::setMultiLayer(int multilayer) {

    m_multilayer = multilayer;
    return;

}

//*****************************************************************************

//::::::::::::::::::::
//:: METHOD setLayer::
//::::::::::::::::::::

void BareMDTHit::setLayer(int layer) {

    m_layer = layer;
    return;

}


//*****************************************************************************

//::::::::::::::::::::
//:: METHOD setTube ::
//::::::::::::::::::::

void BareMDTHit::setTube(int tube) {

    m_tube = tube;
    return;

}
