//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 25.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                     MDTChamberEventGenerator                    ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>

// MDTTrigger //
#include "MDTChamberEventGenerator.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;
using namespace CLHEP;

//*****************************************************************************

//:::::::::::::::::::::::::
//:: DEFAULT CONSTRUCTOR ::
//:::::::::::::::::::::::::

MDTChamberEventGenerator::MDTChamberEventGenerator(void) {

    init(15.0, 0.4, 36, 3, 2, 0., 0., 121.0, "share/default.rt");

}

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

MDTChamberEventGenerator::MDTChamberEventGenerator(
                            const double & R,
                            const double & w_wall,
                            const unsigned int & nb_tubes_per_layer,
                            const unsigned int & nb_layers_per_multilayer,
                            const unsigned int & nb_multilayers,
                            const double & y_offset,
                            const double & z_offset,
                            const double & spacer_thickness,
                            const string & rt_file_name) {

    init(R, w_wall, nb_tubes_per_layer, nb_layers_per_multilayer,
          nb_multilayers, y_offset, z_offset, spacer_thickness, rt_file_name);

}

//*****************************************************************************

//:::::::::::::::
//:: METHOD rt ::
//:::::::::::::::

const Rt_relation & MDTChamberEventGenerator::rt(void) const {

    return m_rt;

}

//*****************************************************************************

//:::::::::::::::::::::
//:: METHOD generate ::
//:::::::::::::::::::::

const std::vector<BareMDTHit> & MDTChamberEventGenerator::generate(
                                            const Straight_line & track,
                                            bool delta_electrons_on,
                                            const double & t_dead,
                                            const double & occupancy,
                                            const double & road_width,
                                            const double & t0_shift,
                                            const double & t_accuracy) {

///////////////
// VARIABLES //
///////////////

    double r_delta;
    double d;
    double t, t_digi;
    double t_min(m_rt.t(0.0, 0)); // minimum drift time
    double t_max(m_rt.t(m_R-m_w_wall, 0)); // maximum drift time
    BareMDTHit *bare_hit(0); // auxiliary bare hit
    BareMDTHit *bare_hit_2(0); // auxiliary bare hit

////////////
// RESETS //
////////////

    m_bare_hit.clear();

/////////////////////////
// LOOP OVER THE TUBES //
/////////////////////////
    for (unsigned int ml=0; ml<m_nb_ml; ml++){
    for (unsigned int ly=0; ly<m_nb_ly_per_ml; ly++) {
    for (unsigned int tb=0; tb<m_nb_tubes_per_ly; tb++) {

      // check if tube is dead
      if(m_dead_wires[ml*m_nb_ly_per_ml*m_nb_tubes_per_ly\
                 + ly*m_nb_tubes_per_ly + tb]){
          continue;
                 }


// check whether tube is within the road width around the track //
      d = fabs(track.sign_dist_from(m_wire_position[ly+ml*m_nb_ly_per_ml][tb]));
      if (d-road_width<m_R) {

    // create a background hit //
      if (m_p_rnd->Uniform(1.0)<occupancy*(t_max-t_min+t_dead)/
                                                (t_max-t_min)) {
                t = m_p_rnd->Uniform(t_min-t_dead, t_max);
                t_digi = t_accuracy*static_cast<int>(t/t_accuracy)
                         +0.5*t_accuracy;
                bare_hit = new BareMDTHit(
                    m_wire_position[ly+ml*m_nb_ly_per_ml][tb].position_vector(),
                    t_digi, ml, ly, tb);
            }

    // create a muon hit //
            if (d<m_R-m_w_wall) {
                t = m_p_rnd->Gaus(m_rt.t(d, 0), m_rt.t_error(m_rt.t(d, 0), 0));
                t_digi = t_accuracy*static_cast<int>(t/t_accuracy)
                         +0.5*t_accuracy;
                bare_hit_2 = new BareMDTHit(
                    m_wire_position[ly+ml*m_nb_ly_per_ml][tb].position_vector(),
                    t_digi, ml, ly, tb);

            }


    // create delta electron hit (highly simplified model) //
            if (bare_hit_2!=0 &&
                delta_electrons_on && m_p_rnd->Uniform(0.0, 1.0)<0.28) {
                r_delta = m_p_rnd->Uniform(0.0, 2.0*m_R);
                if (bare_hit!=0 && m_rt.t(r_delta, 0)<bare_hit->getDriftTime()){
                 if (r_delta<d &&
                    fabs(m_rt.t(r_delta, 0)-bare_hit->getDriftTime())>t_dead) {
                    delete bare_hit_2;
                    t_digi = t_accuracy*
                             static_cast<int>(m_rt.t(r_delta, 0)/t_accuracy)
                             +0.5*t_accuracy;

                    bare_hit_2 = new BareMDTHit(
                    m_wire_position[ly+ml*m_nb_ly_per_ml][tb].position_vector(),
                    t_digi, ml, ly, tb);
                 }
                }
                if (bare_hit==0 ||
                    (bare_hit!=0 &&
                     m_rt.t(r_delta, 0)>=bare_hit->getDriftTime())) {
                 if (r_delta<d) {
                    delete bare_hit_2;
                    t_digi = t_accuracy*
                             static_cast<int>(m_rt.t(r_delta, 0)/t_accuracy)
                             +0.5*t_accuracy;

                    bare_hit_2 = new BareMDTHit(
                    m_wire_position[ly+ml*m_nb_ly_per_ml][tb].position_vector(),
                    t_digi, ml, ly, tb);
                 }
                }
            }

    // check for dead time //
            if (bare_hit!=0 && bare_hit_2!=0) {
                if (fabs(bare_hit->getDriftTime()-bare_hit_2->getDriftTime())>
                                                                    t_dead) {
                    if (bare_hit->getDriftTime()>=t_min-t_dead) {
                        m_bare_hit.push_back(*bare_hit);
                    }
                    m_bare_hit.push_back(*bare_hit_2);
                } else {
                    if (bare_hit->getDriftTime()<bare_hit_2->getDriftTime()) {
                        if (bare_hit->getDriftTime()>t_min-t_dead) {
                            m_bare_hit.push_back(*bare_hit);
                            delete bare_hit_2;
                        } else {
                            delete bare_hit;
                            delete bare_hit_2;
                        }
                    } else {
                        m_bare_hit.push_back(*bare_hit_2);
                        delete bare_hit;
                    }
                }
            } else {
                if (bare_hit!=0) {
                    m_bare_hit.push_back(*bare_hit);
                }
                if (bare_hit_2!=0) {
                    m_bare_hit.push_back(*bare_hit_2);
                }
            }
        }

        bare_hit = 0;
        bare_hit_2 = 0;

    }
    }
    }
////////////////////////////////
// DISPLAY FILE FOR DEBUGGING //
////////////////////////////////

//     static ofstream outfile("hit_display.cxx");
//     outfile << "display();" << endl;
// //     outfile << "TH2F *hist = new TH2F(\"hist\", \"\", 100, "
// //             << -1.1*m_nb_tubes_per_ly*m_R << ", "
// //             << 1.1*m_nb_tubes_per_ly*m_R<< ", 100, "
// //             << -1.1*m_nb_tubes_per_ly*m_R << ", "
// //             << 1.1*m_nb_tubes_per_ly*m_R << ");"
// //             << endl;
// //     outfile << "hist->Draw();" << endl;
// //     outfile << "TArc *arc;" << endl;
//     for (unsigned int k=0; k<m_bare_hit.size(); k++) {
//         outfile << "arc = new TArc("
//                 << m_bare_hit[k].getWirePosition().y()
//                 << ", "
//                 << m_bare_hit[k].getWirePosition().z()
//                 << ", " << m_rt.r(m_bare_hit[k].getDriftTime(), 0)
//                 << "); arc->SetLineColor(2); "
//                 << "arc->Draw(\"SAME\");"
//                 << endl;
//     }
//
//     outfile << "TLine *line=new TLine("
//             << track.my()*(-1.1*m_nb_tubes_per_ly*m_R)+track.by() << ", "
//             << (-1.1*m_nb_tubes_per_ly*m_R) << ", "
//             << track.my()*(1.1*m_nb_tubes_per_ly*m_R)+track.by() << ", "
//             << (1.1*m_nb_tubes_per_ly*m_R) << "); line->Draw(\"SAME\");"
//             << endl;
//
//     outfile << "c1->Update(); double wait; cin >> wait;" << endl;

    static ofstream res_file("residuals.txt");
    for (unsigned int k=0; k<m_bare_hit.size(); k++) {
        Straight_line wire(m_bare_hit[k].getWirePosition(),
                           Hep3Vector(1.0, 0.0, 0.0));
        res_file << m_bare_hit[k].getWirePosition().y() << "\t"
                << track.sign_dist_from(wire) << "\t"
                << m_bare_hit[k].getDriftTime() << "\t"
                << m_rt.r(m_bare_hit[k].getDriftTime(), 0) << "\t"
                << m_rt.t_error(m_bare_hit[k].getDriftTime(), 0)
                << endl;
    }

    return m_bare_hit;

}

//*****************************************************************************

//:::::::::::::::::
//:: METHOD init ::
//:::::::::::::::::

void MDTChamberEventGenerator::init(const double & R,
              const double & w_wall,
              const unsigned int & nb_tubes_per_layer,
              const unsigned int & nb_layers_per_multilayer,
              const unsigned int & nb_multilayers,
              const double & y_offset,
              const double & z_offset,
              const double & spacer_thickness,
              const string & rt_file_name) {

///////////////
// VARIABLES //
///////////////

    double pos_y, pos_z;
    double h;
    Hep3Vector dir(1.0, 0.0, 0.0);
    ifstream rt_file(rt_file_name.c_str());
    if (rt_file.fail()) {
        cerr << "Class MDTChamberEventGenerator, method init: ERROR!\n"
             << "Could not open file " << rt_file_name << ".\n";
        exit(1);
    }

/////////////////////////////
// STORE PRIVATE VARIABLES //
/////////////////////////////

    m_R = R;
    h = sqrt(3.0)*m_R;
    m_w_wall = w_wall;
    m_nb_tubes_per_ly = nb_tubes_per_layer;
    m_nb_ly_per_ml = nb_layers_per_multilayer;
    m_nb_ml = nb_multilayers;
    m_rt.read_rt(rt_file);
    m_wire_position = vector< vector< Straight_line > >(m_nb_ml*m_nb_ly_per_ml);
    for (unsigned int k=0; k<m_wire_position.size(); k++) {
        m_wire_position[k] = vector<Straight_line>(m_nb_tubes_per_ly);
    }

/////////////////////////////////
// CREATE THE CHAMBER GEOMETRY //
/////////////////////////////////

    for (unsigned int ly=0; ly<m_nb_ml*m_nb_ly_per_ml; ly++) {
      for (unsigned int tb=0; tb<m_nb_tubes_per_ly; tb++) {

// 1st multilayer //
        if (ly<m_nb_ly_per_ml) {
            pos_z = -0.5*spacer_thickness-m_R+
                    static_cast<int>(ly-m_nb_ly_per_ml+1)*h;
            pos_y = (tb-0.5*m_nb_tubes_per_ly)*2.0*m_R-m_R*(ly%2);
        }

// 2nd multilayer //
        if (ly>=m_nb_ly_per_ml) {
            pos_z = 0.5*spacer_thickness+m_R+(ly-m_nb_ly_per_ml)*h;
            pos_y = (tb-0.5*m_nb_tubes_per_ly)*2.0*m_R-m_R*((ly-1)%2);
        }

// store wire position //
        m_wire_position[ly][tb] = Straight_line(Hep3Vector(0.0,
            pos_y + y_offset, pos_z + z_offset), dir);
      }
    }

////////////////////////////////
// DISPLAY FILE FOR DEBUGGING //
////////////////////////////////

    ofstream outfile("display.cxx");
    outfile << "void display(void) {" << endl;
    outfile << "TH2F *hist = new TH2F(\"hist\", \"\", 100, "
            << -1.1*m_nb_tubes_per_ly*m_R + y_offset<< ", "
            << 1.1*m_nb_tubes_per_ly*m_R + y_offset<< ", 100, "
            << -1.1*m_nb_tubes_per_ly*m_R + z_offset<< ", "
            << 1.1*m_nb_tubes_per_ly*m_R + z_offset<< ");"
            << endl;
    outfile << "hist->Draw();" << endl;
    outfile << "TArc *arc;"<< endl;
    for (unsigned int ly=0; ly<m_nb_ml*m_nb_ly_per_ml; ly++) {
    for (unsigned int tb=0; tb<m_nb_tubes_per_ly; tb++) {
        outfile << "arc = new TArc("
                << m_wire_position[ly][tb].position_vector().y()
                << ", "
                << m_wire_position[ly][tb].position_vector().z()
                << ", " << m_R << "); arc->Draw(\"SAME\");"
                << endl;
    }
    }
    outfile << "}" << endl;


///////////////////
// GEOMETRY FILE //
///////////////////

ofstream geofile("geometry.txt");
for (unsigned int ml=0; ml<m_nb_ml; ml++) {
for (unsigned int ly=0; ly<m_nb_ly_per_ml; ly++) {
for (unsigned int tb=0; tb<m_nb_tubes_per_ly; tb++) {
    geofile << tb << "\t"
            << ly << "\t"
            << ml << "\t"
            << m_wire_position[ly+ml*m_nb_ly_per_ml][tb].position_vector().y()
            << "\t"
            << m_wire_position[ly+ml*m_nb_ly_per_ml][tb].position_vector().z()
            << endl;
}
}
}

//////////////////////////
// TABLE FOR DEAD WIRES //
//////////////////////////

m_dead_wires.reserve(m_nb_ml*m_nb_ly_per_ml*m_nb_tubes_per_ly);
std::fill(m_dead_wires.begin(), m_dead_wires.end(), 0);

/////////////////////////////
// RANDOM NUMBER GENERATOR //
/////////////////////////////

    m_p_rnd = new TRandom3(time(0));

    return;

}


//*****************************************************************************

//::::::::::::::::::::::
//:: METHOD kill_wire ::
//::::::::::::::::::::::

void MDTChamberEventGenerator::kill_wire(unsigned int number){

  if(number < m_nb_ml * m_nb_ly_per_ml * m_nb_tubes_per_ly){
      m_dead_wires[number] = 1;
  }
  return;
}

//*****************************************************************************
