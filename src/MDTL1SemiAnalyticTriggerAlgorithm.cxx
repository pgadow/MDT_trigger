//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 28.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                       MDTL1SemiAnalyticTriggerAlgorithm         ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

#include "TFile.h"
#include "TCanvas.h"
#include "TImage.h"

// standard C++ //
#include <iostream>
#include <string>
#include <cmath>

// MDTTrigger //
#include "MDTL1SemiAnalyticTriggerAlgorithm.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;
using namespace CLHEP;

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

MDTL1SemiAnalyticTriggerAlgorithm::MDTL1SemiAnalyticTriggerAlgorithm(\
                          const double & m_min,
                          const double & m_max,
                          const double & m_bin_width,
                          const double & b_min,
                          const double & b_max,
                          const double & b_bin_width) {

    m_parameter = static_cast<double>(b_bin_width);
    // m_track_hist_m = new TH1D("track_hist_m", "",
    //                      static_cast<int>((m_max-m_min)/m_bin_width),
    //                      m_min, m_max);
    // m_track_hist_b = new TH1D("track_hist_b", "",
    //                      static_cast<int>((b_max-b_min)/b_bin_width),
    //                      b_min, b_max);
    // m_track_hist_mb = new TH2D("track_hist_mb", "",\
    //                     static_cast<int>((m_max-m_min)/m_bin_width),\
    //                     m_min, m_max,\
    //                     static_cast<int>((b_max-b_min)/b_bin_width),\
    //                     b_min, b_max);

}

//*****************************************************************************

//:::::::::::::::::::::::::::::::::
//:: METHOD getSegmentCandidates ::
//:::::::::::::::::::::::::::::::::

const std::vector<SegmentCandidate> &
                        MDTL1SemiAnalyticTriggerAlgorithm::getSegmentCandidates(
                                        const std::vector<BareMDTHit> & hits,
                                        const Straight_line & L0_track,
                                        const double & L0_m_accuracy,
                                        const double & L0_b_accuracy,
                                        const double & t_accuracy,
                                        const Rt_relation & rt) {

///////////
// RESET //
///////////

    // m_track_hist_m->Clear();
    // m_track_hist_b->Clear();
    // m_track_hist_mb->Clear();
    m_segment.clear();

///////////////
// VARIABLES //
///////////////

    double chi2(0.0); // chi^2 of the reconstructed straight line
    double m(0.0), b(0.0);  // slope and intercept of a straight line
    double g_1 = 0.0;
    double g_2 = 0.0;
    double Lambda_11 = 0.0;
    double Lambda_12 = 0.0;
    double Lambda_22 = 0.0;
    double D = 0.0;

    // vector<double> results_m; // results of slope
    // vector<double> results_b; // results of position
    vector<Hep3Vector> results_hits; // results of hit positions

    Hep3Vector y, z, y_prime; // coordinate axes

    double t_max=rt.t(rt.number_of_pairs()-1);
    double r_max=rt.r(rt.number_of_pairs()-1);
    double sigma(0.0);
    double m_err(0.0);

/////////////////////////////////////
// COMPUTE MEAN SPATIAL RESOLUTION //
/////////////////////////////////////

    for (int k=0; k<rt.number_of_pairs(); k++) {
        sigma += rt.error(k)*rt.error(k);
    }
    sigma = sqrt(sigma/static_cast<double>(rt.number_of_pairs())
                 +pow(t_accuracy*r_max/(t_max*sqrt(12.0)), 2));


///////////////////////////////////////////////////////////
// CREATE A VECTOR OF MDT HITS FOR THE TRIGGER ALGORITHM //
///////////////////////////////////////////////////////////

    // only process hits with drift time within trigger signal
    // (extended by time accuracy). Illustration:
    // t[ns]: ----(-time_accuracy)--------|trigger signal|----

    vector<BareMDTHit> passed_hits;
    for (unsigned int k=0; k<hits.size(); k++) {
        if (hits[k].getDriftTime()<-t_accuracy) {
            continue;
        }


        passed_hits.push_back(BareMDTHit(hits[k].getWirePosition(),\
                                         hits[k].getDriftTime(),\
                                         rt,\
                                         hits[k].getMultiLayer(),\
                                         hits[k].getLayer(),\
                                         hits[k].getTube() ));

    }

    // perform pattern recognition to reduce number of hits to be processed
    // in algorithm

    //passed_hits = patternRecognition(passed_hits);

///////////////////////////////////////////////
// CALCULATE THE TANGENTS TO THE DRIFT RADII //
///////////////////////////////////////////////

    vector<double> r;
    r.reserve(2);

    for (unsigned int j=0; j<passed_hits.size(); j++) {
      Hep3Vector wire_pos_1 = passed_hits[j].getWirePosition();
      double r1 = passed_hits[j].getDriftRadius();
      for (unsigned int k=j+1; k<passed_hits.size(); k++) {

        // only calculate tangents of hits in the same multilayer
        // and in different layers
        if (passed_hits[j].getMultiLayer() != passed_hits[k].getMultiLayer() ||\
            passed_hits[j].getLayer() == passed_hits[k].getLayer()) {
              continue;
        }

        Hep3Vector wire_pos_2 = passed_hits[k].getWirePosition();
        double r2 = passed_hits[k].getDriftRadius();
        double L2 = (wire_pos_2 - wire_pos_1).mag2();

        Hep3Vector frame = wire_pos_2 - wire_pos_1;
        double mp = frame.y()/frame.z();
        y = CLHEP::Hep3Vector(0, 1., -mp); y=y.unit();
        z = CLHEP::Hep3Vector(0, mp, 1.);  z=z.unit();

        // calculate tangents for 4 different combinations
        for (unsigned int i = 0; i < 4; i++) {
          r = calculateTangent(r1,r2,L2,i); // m=result[0], b=result[1]
          double d = 1. - (mp *r[0]);
          double m_candidate = (mp + r[0])/d;
          double b_candidate = (r[1]+r[0]*wire_pos_1.z()-wire_pos_1.y())/d;

          //check if result is compatible with TGC/RPC seed track
          if (fabs(L0_track.my()-m_candidate)>m_parameter /*||\
              fabs(L0_track.by()-b_candidate)>30*L0_b_accuracy*/){
              continue;
          }

          // cout << "r1: " << j <<", r2: "<<k<<", combination: "<<i<<endl;
          // cout << "m_candidate: " << m_candidate
          //      << "\tL0_track.my: " << L0_track.my()<< endl;
          // cout << "b_candidate: " << b_candidate
          //      << "\tL0_track.by: " << L0_track.by()<< endl;
          // cout << endl;

          y_prime = Hep3Vector(0,1,-m_candidate); y_prime=y_prime.unit();

          vector<Hep3Vector> hits = calculateHits(r1, r2, y_prime,\
                                                  wire_pos_1, wire_pos_2, i);
          // m_track_hist_m->Fill(m_candidate);
          // m_track_hist_b->Fill(b_candidate);
          //m_track_hist_mb->Fill(m_candidate, b_candidate);

          // results_m.push_back(m_candidate);
          // results_b.push_back(b_candidate);
          results_hits.insert(results_hits.end(), hits.begin(), hits.end());
        }
      }
   }

// DEBUG PLOTTING
// TCanvas *c = new TCanvas;
// std::string temp;
// m_track_hist_m->Draw();
// TImage *img = TImage::Create();
// img->FromPad(c);
// img->WriteImage("canvas.png");
// std::cin >> temp ;
// m_track_hist_b->Draw();
// img->FromPad(c);
// img->WriteImage("canvas.png");
// std::cin >> temp ;
// m_track_hist_mb->Draw();
// img->FromPad(c);
// img->WriteImage("canvas.png");
// std::cin >> temp ;

    for (unsigned int i = 0; i < results_hits.size(); i++) {
      g_1 = g_1+results_hits[i].y();
      g_2 = g_2+results_hits[i].y()*results_hits[i].z();
      Lambda_11 = Lambda_11+1.0;
      Lambda_12 = Lambda_12+results_hits[i].z();
      Lambda_22 = Lambda_22+results_hits[i].z()*results_hits[i].z();
    }

    D = Lambda_11*Lambda_22-Lambda_12*Lambda_12;
    b = (g_1*Lambda_22-g_2*Lambda_12)/D;
    m = (-g_1*Lambda_12+g_2*Lambda_11)/D;
    m_err = sigma*sqrt(Lambda_11*Lambda_11*Lambda_22
                 -2.0*Lambda_11*Lambda_12*Lambda_12
                 +Lambda_12*Lambda_12*Lambda_11)/D;

    chi2 = 0.0;
    for(unsigned int j=0; j<results_hits.size(); j++) {
        chi2 = chi2+pow((results_hits[j].y()-b-m*results_hits[j].z())/sigma, 2);
    }

    // create track segment candidate //
    Straight_line track_candidate(Hep3Vector(0.0, b, 0.0),\
                                        Hep3Vector(0.0, m, 1.0));
    m_segment.push_back(SegmentCandidate(track_candidate,\
                  static_cast<unsigned int>(results_hits.size()), chi2, m_err));
    return m_segment;
}

//*****************************************************************************

//:::::::::::::::::::::::::::::
//:: METHOD calculateTangent ::
//:::::::::::::::::::::::::::::

std::vector<double> MDTL1SemiAnalyticTriggerAlgorithm::calculateTangent(\
                                    double r1, double r2, double L2,\
                                    unsigned int configuration){
    std::vector<double> result;
    result.reserve(2);
    double m,b;
    double denominator = sqrt(L2 - r1*r1 - r2*r2 + 2*r1*r2);
    double nominator;
    switch (configuration) {
      case 0:
        nominator = r2 - r1;
        break;
      case 1:
        nominator = r1 - r2;
        break;
      case 2:
        nominator = -r1 - r2;
        break;
      case 3:
        nominator = r1 + r2;
        break;
      default:
        nominator = 0;
        break;
    }

    // check if denominator is zero
    if (denominator>0 ) {
      m = nominator/denominator;
      if(configuration%2 == 0){
        b = r1*sqrt(1+m*m);
      }
      else{
        b = -r1*sqrt(1+m*m);
      }
    }
    else{
      m = -99999;
      b = -99999;
    }
    result.push_back(m);
    result.push_back(b);

    return result;
}

//*****************************************************************************

//:::::::::::::::::::::::::::
//:: METHOD calculateHits  ::
//:::::::::::::::::::::::::::

std::vector<Hep3Vector> MDTL1SemiAnalyticTriggerAlgorithm::calculateHits(\
                                    double r1, double r2, Hep3Vector y_prime,\
                                    Hep3Vector wp1, Hep3Vector wp2,\
                                    unsigned int configuration){
    std::vector<Hep3Vector> result;
    result.reserve(2);

    switch (configuration) {
      case 0:
        result.push_back(wp1 + r1*y_prime);
        result.push_back(wp2 + r2*y_prime);
        break;
      case 1:
        result.push_back(wp1 + r1*y_prime);
        result.push_back(wp2 - r2*y_prime);
        break;
      case 2:
        result.push_back(wp1 - r1*y_prime);
        result.push_back(wp2 + r2*y_prime);
        break;
      case 3:
        result.push_back(wp1 - r1*y_prime);
        result.push_back(wp2 - r2*y_prime);
        break;
      default:
        break;
    }

    return result;
}

//*****************************************************************************

//:::::::::::::::::::::::::::::::::
//:: METHOD patternRecognition   ::
//:::::::::::::::::::::::::::::::::

std::vector<BareMDTHit> MDTL1SemiAnalyticTriggerAlgorithm::patternRecognition(\
                                      const std::vector<BareMDTHit> & hits){

    std::vector<BareMDTHit> survivors;

    for (unsigned int  i = 0; i < hits.size(); i++) {
      survivors.push_back(hits[i]);
    }

    return survivors;

}
