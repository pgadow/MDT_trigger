//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 28.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: IMPLEMENTATION OF CONSTRUCTORS AND METHODS DEFINED IN THE CLASS ::
//::                 MDTL1VeryFastTriggerAlgorithm                   ::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

#include "TFile.h"
#include "TCanvas.h"
#include "TImage.h"

// standard C++ //
#include <iostream>
#include <string>
#include <cmath>

// MDTTrigger //
#include "MDTL1VeryFastTriggerAlgorithm.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;
using namespace CLHEP;

//*****************************************************************************

//:::::::::::::::::
//:: CONSTRUCTOR ::
//:::::::::::::::::

MDTL1VeryFastTriggerAlgorithm::MDTL1VeryFastTriggerAlgorithm(\
                          const double & b_width,
                          const double & m_width) {

    m_b_width = static_cast<double>(b_width);
    m_m_width = static_cast<double>(m_width);

}

//*****************************************************************************

//:::::::::::::::::::::::::::::::::
//:: METHOD getSegmentCandidates ::
//:::::::::::::::::::::::::::::::::

const std::vector<SegmentCandidate> &
                        MDTL1VeryFastTriggerAlgorithm::getSegmentCandidates(
                                        const std::vector<BareMDTHit> & hits,
                                        const Straight_line & L0_track,
                                        const double & t_accuracy,
                                        const Rt_relation & rt) {

///////////
// RESET //
///////////

    m_segment.clear();

///////////////
// VARIABLES //
///////////////

    double chi2(0.0); // chi^2 of the reconstructed straight line

    double m_mean = 0; // arithmetic mean of tangent slopes
    double b_mean = 0; // arithmetic mean of tangent intercepts
    unsigned int n_tangents = 0; // helper variable for arithmetic mean

    vector<double> rs; // result of tangent calculation: [0]:slope [1]:intercept
    rs.reserve(2);

    vector<BareMDTHit> passed_hits; // hits passing pattern recognition
    passed_hits.reserve(hits.size());
    Hep3Vector wire_pos_1; // wire position of first hit
    Hep3Vector wire_pos_2; // wire position of second hit
    Hep3Vector L;          // vector between wire positions (wp2 - wp1)
    double r1;             // drift radius of first hit
    double r2;             // drift radius of second hit

    double t_max=rt.t(rt.number_of_pairs()-1);
    double r_max=rt.r(rt.number_of_pairs()-1);
    double sigma(0.0);
    double m_err(0.0);

/////////////////////////////////////
// COMPUTE MEAN SPATIAL RESOLUTION //
/////////////////////////////////////

    for (int k=0; k<rt.number_of_pairs(); k++) {
        sigma += rt.error(k)*rt.error(k);
    }
    sigma = sqrt(sigma/static_cast<double>(rt.number_of_pairs())
                 +pow(t_accuracy*r_max/(t_max*sqrt(12.0)), 2));


///////////////////////////////////////////////////////////
// CREATE A VECTOR OF MDT HITS FOR THE TRIGGER ALGORITHM //
///////////////////////////////////////////////////////////

    // only process hits with drift time within trigger signal
    // (extended by time accuracy). Illustration:
    // t[ns]: ----(-time_accuracy)--------|trigger signal|----

    passed_hits.clear();
    for (unsigned int k=0; k<hits.size(); k++) {
        if (hits[k].getDriftTime()<-t_accuracy) {
            continue;
        }

        passed_hits.push_back(BareMDTHit(hits[k].getWirePosition(),\
                                         hits[k].getDriftTime(),\
                                         rt,\
                                         hits[k].getMultiLayer(),\
                                         hits[k].getLayer(),\
                                         hits[k].getTube() ));
    }

    // perform pattern recognition to reduce number of hits to be processed
    // in algorithm

    // patternRecognition(passed_hits);


///////////////////////////////////////////////
// CALCULATE THE TANGENTS TO THE DRIFT RADII //
///////////////////////////////////////////////

    m_mean = 0;
    b_mean = 0;
    n_tangents = 0;

    for (unsigned int j=0; j<passed_hits.size(); j++) {
      wire_pos_1 = passed_hits[j].getWirePosition();
      r1 = passed_hits[j].getDriftRadius();
      for (unsigned int k=j+1; k<passed_hits.size(); k++) {

        // only calculate tangents of hits in the same multilayer
        // and in different layers
        if (passed_hits[j].getMultiLayer() != passed_hits[k].getMultiLayer() ||\
            passed_hits[j].getLayer() == passed_hits[k].getLayer()) {
              continue;
        }

        wire_pos_2 = passed_hits[k].getWirePosition();
        r2 = passed_hits[k].getDriftRadius();

        L = wire_pos_2 - wire_pos_1;
        double L2 = L.mag2();
        double alpha_0 = atan(L.y()/L.z());

        // calculate tangents for 4 different combinations
        for (unsigned int i = 0; i < 4; i++) {
          rs = calculateTangent(r1,r2,L2,i); // m=result[0], b=result[1]
          double alpha = atan(rs[0]);
          double m_candidate = tan(alpha+alpha_0);
          double b_candidate = wire_pos_1.y() + cos(alpha_0)*rs[1]\
                   -m_candidate*wire_pos_1.z() + m_candidate*sin(alpha_0)*rs[1];

          // cout << "r1: " << j <<", r2: "<<k<<", combination: "<<i<<endl;
          // cout << "wp1: " << wire_pos_1.z() << " " << wire_pos_1.y() << endl;
          // cout << "m: " << r[0] << " b: " << r[1] << endl;
          // cout << "m_candidate: " << m_candidate
          //          << "\tL0_track.my: " << L0_track.my()<< endl;
          // cout << "b_candidate: " << b_candidate
          //         << "\tL0_track.by: " << L0_track.by()<< endl;
          // cout << endl;

          //check if result is compatible with TGC/RPC seed track
          if (fabs(L0_track.my()-m_candidate)>m_m_width ||\
              fabs(L0_track.by()-b_candidate)>m_b_width){
              continue;
          }

          ++n_tangents;
          m_mean += m_candidate;
          b_mean += b_candidate;
        }
      }
    }

    // Calculate average slope and intercept, if not possible, take L0 path
    if (n_tangents > 0) {
        m_mean /= static_cast<double>(n_tangents);
        b_mean /= static_cast<double>(n_tangents);
    }
    else{
        m_mean = L0_track.my();
        b_mean = L0_track.by();
    }


    // create track segment candidate //
    Straight_line track_candidate(Hep3Vector(0.0, b_mean, 0.0),\
                                        Hep3Vector(0.0, m_mean, 1.0));
    m_segment.push_back(SegmentCandidate(track_candidate,\
                  n_tangents, chi2, m_err));

    // cout << "number of found hits " << results_hits.size() << " of " << passed_hits.size() << endl;
    // cout << "L0_track.my: " << L0_track.my() << "\tL0_track.by: " << L0_track.by()<< endl;
    // cout << "m_mean " << m_mean << ", b_mean " << b_mean <<endl;

    return m_segment;
}

//*****************************************************************************

//:::::::::::::::::::::::::::::
//:: METHOD calculateTangent ::
//:::::::::::::::::::::::::::::

std::vector<double> MDTL1VeryFastTriggerAlgorithm::calculateTangent(\
                                    double r1, double r2, double L2,\
                                    unsigned int configuration){
    std::vector<double> result;
    result.reserve(2);
    double m,b;
    double nominator;
    double denominator1 = sqrt(L2 - r1*r1 - r2*r2 + 2*r1*r2);
    double denominator2 = sqrt(L2 - r1*r1 - r2*r2 - 2*r1*r2);

    // check if denominator is zero
    if (denominator1 == 0 || denominator2 == 0) {
      result.push_back(-999999);
      result.push_back(-999999);
      return result;
    }

    switch (configuration) {
      case 0:
        nominator = r2 - r1;
        m = nominator/denominator1;
        b = r1*sqrt(1+m*m);
        break;
      case 1:
        nominator = r1 - r2;
        m = nominator/denominator1;
        b = -r1*sqrt(1+m*m);
        break;
      case 2:
        nominator = -r1 - r2;
        b = r1*sqrt(1+m*m);
        m = nominator/denominator2;
        break;
      case 3:
        nominator = r1 + r2;
        b = -r1*sqrt(1+m*m);
        m = nominator/denominator2;
        break;
      default:
        nominator = 0;
        break;
    }

    result.push_back(m);
    result.push_back(b);

    return result;
}

//*****************************************************************************

//:::::::::::::::::::::::::::::::::
//:: METHOD patternRecognition   ::
//:::::::::::::::::::::::::::::::::

void MDTL1VeryFastTriggerAlgorithm::patternRecognition(\
                                      std::vector<BareMDTHit> & hits){

    return;
}
