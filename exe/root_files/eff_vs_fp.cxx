#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TGraphErrors.h"
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

void eff_vs_fp(string & file_name, const int & n,
                                   const double & delta_m,
                                   const double & chi2pdof_cut) {

//////////////////////////////////////////////////////////
//    This file has been automatically generated 
//      (Wed Oct 30 14:28:20 2013 by ROOT version5.34/03)
//    from TTree tree/tree
//    found on file: tmp_ideal_spread_0.0_bw1.5.root
//////////////////////////////////////////////////////////


//Reset ROOT and connect tree file
    gROOT->Reset();
    TFile *f=new TFile(file_name.c_str());
    TTree *tree;
    f->GetObject("tree",tree);

//Declaration of leaves types
    Double_t       m_gen;
    Double_t       b_gen;
    Double_t       m_L0;
    Int_t          nb_candidates;
    vector<int>    *nb_hits(0);
    vector<double> *chi2(0);
    vector<double> *m_rec(0);
    vector<double> *b_rec(0);
    vector<double> *m_rec_err(0);

    // Set branch addresses.
    tree->SetBranchAddress("m_gen",&m_gen);
    tree->SetBranchAddress("b_gen",&b_gen);
    tree->SetBranchAddress("m_L0",&m_L0);
    tree->SetBranchAddress("nb_candidates",&nb_candidates);
    tree->SetBranchAddress("nb_hits",&nb_hits);
    tree->SetBranchAddress("chi2",&chi2);
    tree->SetBranchAddress("m_rec",&m_rec);
    tree->SetBranchAddress("b_rec",&b_rec);
    tree->SetBranchAddress("m_rec_err",&m_rec_err);

//      This is the loop skeleton
//         To read only selected branches, Insert statements like:
// tree->SetBranchStatus("*",0);  // disable all branches
// TTreePlayer->SetBranchStatus("branchname",1);  // activate branchname

///////////////////////////
// MY ANALYSIS VARIABLES //
///////////////////////////

    TFile outfile("outfile.root", "RECREATE");
    TH1F *hist_eff = new TH1F("hist_eff", "", 2, -0.5, 1.5);
    TH1F *hist_fp = new TH1F("hist_fp", "", 2, -0.5, 1.5);
    TH1F *hist_real_fp = new TH1F("hist_real_fp", "", 2, -0.5, 1.5);
    TNtuple *ntuple = new TNtuple("ntuple", "",
    "nb_candidates:efficient:fake:nb_hits_eff:nb_hits_fake:chi2_eff:chi2_fake");
    int nb_hits_eff, nb_hits_fake;
    double chi2_eff, chi2_fake;

    Long64_t nentries = tree->GetEntries();

    Long64_t nbytes = 0;
    for (Long64_t i=0; i<nentries;i++) {
        nbytes += tree->GetEntry(i);

// loop over the candidates //
        bool efficient(false);
        bool fake(false);
        nb_hits_eff = 0;
        nb_hits_fake = 0;
        chi2_eff = 1.0e99;
        chi2_fake = 1.0e99;
        for (int k=0; k<nb_candidates; k++) {
            if ((*nb_hits)[k]<n) {
                continue;
            }
            if ((*chi2)[k]/static_cast<double>((*nb_hits)[k])>chi2pdof_cut) {
                continue;
            }
            if ((*m_rec_err)[k]>1.0e-3) {
                continue;
            }
            if (fabs((*m_rec)[k]-m_gen)<=delta_m) {
                efficient = true;
                if ((*nb_hits)[k]>nb_hits_eff) {
                    nb_hits_eff = (*nb_hits)[k];
                }
                if ((*chi2)[k]<chi2_eff) {
                    chi2_eff = (*chi2)[k];
                }
            } else {
                fake = true;
                if ((*nb_hits)[k]>nb_hits_fake) {
                    nb_hits_fake = 
                    (*nb_hits)[k];
                }
                if ((*chi2)[k]<chi2_fake) {
                    chi2_fake = (*chi2)[k];
                }
            }
        }

// store the result //
        hist_eff->Fill(efficient, 1.0);
        hist_fp->Fill(fake, 1.0);
        hist_real_fp->Fill(fake*(!efficient), 1.0);
        ntuple->Fill(nb_candidates, efficient, fake, nb_hits_eff, nb_hits_fake,
                     chi2_eff, chi2_fake);

    }

    cout << "Efficiency: " << 100.0*hist_eff->GetMean() << "+/-";
    if (hist_eff->GetMean()>0.5) {
        cout << 100.0*sqrt(hist_eff->GetBinContent(1))/hist_eff->GetEntries() << endl;
    } else {
        cout << 100.0*sqrt(hist_eff->GetBinContent(2))/hist_eff->GetEntries() << endl;
    }

    cout << "Fake probability: " << 100.0*hist_fp->GetMean() << "+/-";
    if (hist_fp->GetMean()>0.5) {
        cout << 100.0*sqrt(hist_fp->GetBinContent(1))/hist_fp->GetEntries() << endl;
    } else {
        cout << 100.0*sqrt(hist_fp->GetBinContent(2))/hist_fp->GetEntries() << endl;
    }

    cout << "Real fake probability: " << 100.0*hist_real_fp->GetMean() << "+/-";
    if (hist_real_fp->GetMean()>0.5) {
        cout << 100.0*sqrt(hist_real_fp->GetBinContent(1))/hist_real_fp->GetEntries() << endl;
    } else {
        cout << 100.0*sqrt(hist_real_fp->GetBinContent(2))/hist_real_fp->GetEntries() << endl;
    }

    outfile.Write();
    outfile.Close();

    return;

}
