#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TGraphErrors.h"
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

void categories(string & file_name, const int & n,
                                   const double & delta_m_good,
                                   const double & delta_m_like_L0,
                                   const double & chi2pdof_cut,
                                   double m_err_cut=10000000.0) {

//////////////////////////////////////////////////////////
//    This file has been automatically generated 
//      (Wed Oct 30 14:28:20 2013 by ROOT version5.34/03)
//    from TTree tree/tree
//    found on file: tmp_ideal_spread_0.0_bw1.5.root
//////////////////////////////////////////////////////////


//Reset ROOT and connect tree file
    gROOT->Reset();
    TFile *f=new TFile(file_name.c_str());
    TTree *tree;
    f->GetObject("tree",tree);

//Declaration of leaves types
    Double_t       m_gen;
    Double_t       b_gen;
    Double_t       m_L0;
    Int_t          nb_candidates;
    vector<int>    *nb_hits(0);
    vector<double> *chi2(0);
    vector<double> *m_rec(0);
    vector<double> *b_rec(0);
    vector<double> *m_rec_err(0);

    // Set branch addresses.
    tree->SetBranchAddress("m_gen",&m_gen);
    tree->SetBranchAddress("b_gen",&b_gen);
    tree->SetBranchAddress("m_L0",&m_L0);
    tree->SetBranchAddress("nb_candidates",&nb_candidates);
    tree->SetBranchAddress("nb_hits",&nb_hits);
    tree->SetBranchAddress("chi2",&chi2);
    tree->SetBranchAddress("m_rec",&m_rec);
    tree->SetBranchAddress("b_rec",&b_rec);
    tree->SetBranchAddress("m_rec_err",&m_rec_err);

//      This is the loop skeleton
//         To read only selected branches, Insert statements like:
// tree->SetBranchStatus("*",0);  // disable all branches
// TTreePlayer->SetBranchStatus("branchname",1);  // activate branchname

///////////////////////////
// MY ANALYSIS VARIABLES //
///////////////////////////

    TFile outfile("outfile.root", "RECREATE");
    TH1F *hist_good = new TH1F("hist_good", "", 2, -0.5, 1.5);
    TH1F *hist_like_L0 = new TH1F("hist_like_L0", "", 2, -0.5, 1.5);
    TH1F *hist_poor = new TH1F("hist_poor", "", 2, -0.5, 1.5);
//     TNtuple *ntuple = new TNtuple("ntuple", "",
//     "nb_candidates:efficient:fake:nb_hits_eff:nb_hits_fake:chi2_eff:chi2_fake");
    int nb_hits_eff, nb_hits_fake;
    double chi2_eff, chi2_fake;

    Long64_t nentries = tree->GetEntries();

    Long64_t nbytes = 0;
    for (Long64_t i=0; i<nentries;i++) {
        nbytes += tree->GetEntry(i);

// loop over the candidates //
        bool good(false);
        bool like_L0(false);
        bool poor(false);
        for (int k=0; k<nb_candidates; k++) {
            if ((*nb_hits)[k]<n) {
                continue;
            }
            if ((*chi2)[k]/static_cast<double>((*nb_hits)[k])>chi2pdof_cut) {
                continue;
            }
            if ((*m_rec_err)[k]>m_err_cut) {
                continue;
            }
            if (fabs((*m_rec)[k]-m_gen)<delta_m_good) {
                good = true;
            }
            if (fabs((*m_rec)[k]-m_gen)>=delta_m_good &&
                fabs((*m_rec)[k]-m_gen)<delta_m_like_L0) {
                like_L0 = true;
            }
            if (fabs((*m_rec)[k]-m_gen)>=delta_m_like_L0) {
                poor = true;
            }
        }

// store the result //
        hist_good->Fill(good, 1.0); 
        hist_like_L0->Fill(like_L0, 1.0);
        hist_poor->Fill(poor, 1.0);
    }

    cout << "Good: " << 100.0*hist_good->GetMean() << "+/-";
    if (hist_good->GetMean()>0.5) {
        cout << 100.0*sqrt(hist_good->GetBinContent(1))/hist_good->GetEntries() << endl;
    } else {
        cout << 100.0*sqrt(hist_good->GetBinContent(2))/hist_good->GetEntries() << endl;
    }

    cout << "Like L0: " << 100.0*hist_like_L0->GetMean() << "+/-";
    if (hist_like_L0->GetMean()>0.5) {
        cout << 100.0*sqrt(hist_like_L0->GetBinContent(1))/hist_like_L0->GetEntries() << endl;
    } else {
        cout << 100.0*sqrt(hist_like_L0->GetBinContent(2))/hist_like_L0->GetEntries() << endl;
    }

    cout << "Poor: " << 100.0*hist_poor->GetMean() << "+/-";
    if (hist_poor->GetMean()>0.5) {
        cout << 100.0*sqrt(hist_poor->GetBinContent(1))/hist_poor->GetEntries() << endl;
    } else {
        cout << 100.0*sqrt(hist_poor->GetBinContent(2))/hist_poor->GetEntries() << endl;
    }

    outfile.Write();
    outfile.Close();

    return;

}
