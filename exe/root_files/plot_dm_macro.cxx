{
TH1F *h_dm_all=new TH1F("h_dm_all", "", 100, -17, 17); h_dm_all->SetXTitle("m_{rec}-m_{gen} [mrad]");
TH1F *h_dm_good=new TH1F("h_dm_good", "", 100, -17, 17); h_dm_all->SetXTitle("m_{rec}-m_{gen} [mrad]"); h_dm_good->SetFillColor(kGreen); h_dm_good->SetLineColor(kGreen);
TH1F *h_dm_like_L0=new TH1F("h_dm_like_L0", "", 100, -17, 17); h_dm_like_L0->SetXTitle("m_{rec}-m_{gen} [mrad]"); h_dm_like_L0->SetFillColor(kBlue); h_dm_like_L0->SetLineColor(kBlue);
TH1F *h_dm_bad=new TH1F("h_dm_bad", "", 100, -17, 17); h_dm_bad->SetXTitle("m_{rec}-m_{gen} [mrad]"); h_dm_bad->SetFillColor(kRed); h_dm_bad->SetLineColor(kRed);
tree->Draw("1000.0*(m_rec-m_gen)>>h_dm_all", "nb_hits>4");
tree->Draw("1000.0*(m_rec-m_gen)>>h_dm_good", "nb_hits>4 && abs(m_rec-m_gen)<0.003");
tree->Draw("1000*(m_rec-m_gen)>>h_dm_like_L0", "nb_hits>4 && abs(m_rec-m_gen)>=0.003 && abs(m_rec-m_gen)<0.09");
tree->Draw("1000*(m_rec-m_gen)>>h_dm_bad", "nb_hits>4 && abs(m_rec-m_gen)>0.009");

h_dm_all->Draw();
h_dm_good->Draw("SAME");
h_dm_like_L0->Draw("SAME");
h_dm_bad->Draw("SAME");
h_dm_all->Draw("SAME");
}
