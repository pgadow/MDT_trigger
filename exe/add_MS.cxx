#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

void add_MS(void) {

    unsigned int nb_points;
    double r, t, res;
    ifstream infile("../share/default.rt");

    infile >> nb_points;
    cout << nb_points << endl;
    for (unsigned int k=0; k<nb_points; k++) {
        infile >> r >> t >> res;
        cout << r << "\t" << t << "\t" << sqrt(res*res+0.1*0.1) << endl;
    }

    return;

}
