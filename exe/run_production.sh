#!/bin/sh

#######################################
### PARAMETERS FOR TRACK GENERATION ###
#######################################

# Specify the number of tracks to be generated here
  NTRACKS=10000
# Specify the minimum x_position here
  BMIN=-60
# Specify the maximum x_position here
  BMAX=60
# Specify the minimum incidence angle here
  MMIN=-0.12
# Specify the maximum incidence angle here
  MMAX=0.12
# Specify the time resolution here
  TRES=12.5

4
for (( h = 1; h <= 9; h++ )) # bin width h.i
do
  for (( i = 0; i <= 9; i++ )) # bin width h.i
  do
    for j in 0 3 5 # L0 resolution in mrad
    do
    	for k in 0 1 2 # occupancy
    	do
          #echo "./run_analysis.exe trigalgstudy_binwidth_${h}.${i}_bmin_${BMIN}_bmax_${BMAX}_mmin_${MMIN}_mmax_${MMAX}_occ_0.${k}_L0res_0.00${j}_tres_12.5.root"
          ./run_analysis.exe trigalgstudy_binwidth_${h}.${i}_bmin_${BMIN}_bmax_${BMAX}_mmin_${MMIN}_mmax_${MMAX}_occ_0.${k}_L0res_0.00${j}_tres_${TRES}.root ${h}.${i} ${NTRACKS} ${BMIN} ${BMAX} ${MMIN} ${MMAX} 0.${k} 0.00${j} ${TRES}
      done
    done
  done
done
