//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 29.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: SIMPLE PROGRAM TO STEAR THE MDT L1 TRIGGER STUDIES ::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>
#include <string>
#include <cstdlib>

// MDTTrigger //
#include "MDTTriggerAnalysisFRO.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;

//::::::::::
//:: MAIN ::
//::::::::::

int main(int argc, char *argv[]) {

//////////////////////
// CHECK INPUT LINE //
//////////////////////

    if (argc!=11) {
        cerr << "Incorrect usage!" << endl
             << "Correct usage:" << endl
             << "run_analysis_FRO.exe <ROOT output file name> <bin width> "
             << "<number of tracks> <b_min> <b_max> <m_min> <m_max> "
             << "<occupancy> <L0 angular resolution> <time resolution>"
             << endl;
        return 1;
    }

///////////////
// VARIABLES //
///////////////

    string ROOT_outfile_name(argv[1]);
    double bin_width(atof(argv[2]));
    unsigned int nb_tracks(atoi(argv[3]));
    double b_gen_min(atof(argv[4]));
    double b_gen_max(atof(argv[5]));
    double m_gen_min(atof(argv[6]));
    double m_gen_max(atof(argv[7]));
    double occupancy(atof(argv[8]));
    double ang_res(atof(argv[9]));
    double t_resolution(atof(argv[10]));

    MDTTriggerAnalysisFRO trigger_analysis(ROOT_outfile_name, bin_width);

//////////////////////////
// PERFORM THE ANALYSIS //
//////////////////////////

    cout << "Running MDT L1 trigger analysis with the following settings"
         << endl
         << " Bin width: " << bin_width << endl
         << " Number of tracks: " << nb_tracks << endl
         << " b_min, b_max: " << b_gen_min << ", " << b_gen_max << endl
         << " m_min, m_max: " << m_gen_min << ", " << m_gen_max << endl
         << " Occupancy: " << occupancy << endl
         << " L0 angular resolution: " << ang_res << endl
         << " Time resolution: " << t_resolution << endl;

    trigger_analysis.analyse(nb_tracks,
                             b_gen_min,
                             b_gen_max,
                             m_gen_min,
                             m_gen_max,
                             occupancy,
                             ang_res,
                             t_resolution);

    return 0;

}
