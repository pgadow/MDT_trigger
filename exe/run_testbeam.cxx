//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 29.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: SIMPLE PROGRAM TO STEAR THE MDT L1 TRIGGER STUDIES ::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>

// MDTTrigger //
#include "MDTTriggerAnalysis.h"

// ROOT //
#include "TFile.h"
#include "TTree.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;

//::::::::::
//:: MAIN ::
//::::::::::

int main(int argc, char *argv[]) {

//////////////////////
// CHECK INPUT LINE //
//////////////////////

    if (argc!=8) {
        cerr << "Incorrect usage!" << endl
             << "Correct usage:" << endl
             << "run_testbeam.exe <ROOT output file name> <ROOT input file> "
             << "<number of events> <angular resolution> "
             << "<time resolution> <algorithm file> <parameter file>"
             << endl;

        return 1;
    }

///////////////
// VARIABLES //
///////////////

    string ROOT_outfile_name(argv[1]);
    string ROOT_infile_name(argv[2]);
    unsigned int nb_tracks(atoi(argv[3]));
    double ang_res(atof(argv[4]));
    double t_resolution(atof(argv[5]));
    string algorithmfile(argv[6]);
    string parameterfile(argv[7]);


    // obtain tree from ROOT data file
    TFile* file = TFile::Open(ROOT_infile_name.c_str());
    TTree* tree = (TTree*) file->Get("FRO_analysis_tree");

    MDTTriggerAnalysis trigger_analysis(ROOT_outfile_name,
                                        tree,
                                        algorithmfile,
                                        parameterfile);

    if(nb_tracks > tree->GetEntriesFast()){
      cout << "Warning: Number of specified tracks()" << nb_tracks
           << ") is greater than entries in file(" << tree->GetEntriesFast()
           << "). Use entries in file instead." << endl;
      nb_tracks = tree->GetEntries("std_has_track == 1");
    }

//////////////////////////
// PERFORM THE ANALYSIS //
//////////////////////////

    cout << "Running MDT L1 trigger analysis with the following settings"
         << endl
         << " Number of tracks: " << nb_tracks << endl
         << " L0 angular resolution: " << ang_res << endl
         << " Time resolution: " << t_resolution << endl
         << " Algorithmfile : " << algorithmfile << endl
         << " Parameter file: " << parameterfile << endl;


    trigger_analysis.analyse(nb_tracks,
                             ang_res,
                             t_resolution);

    return 0;

}
