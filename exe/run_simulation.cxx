//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 29.10.2013, AUTHOR: OLIVER KORTNER
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:: SIMPLE PROGRAM TO STEAR THE MDT L1 TRIGGER STUDIES ::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//::::::::::::::::::
//:: HEADER FILES ::
//::::::::::::::::::

// standard C++ //
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>

// MDTTrigger //
#include "MDTTriggerAnalysis.h"

//::::::::::::::::::::::::
//:: NAMESPACE SETTINGS ::
//::::::::::::::::::::::::

using namespace std;
using namespace MDTTrigger;

//::::::::::
//:: MAIN ::
//::::::::::

int main(int argc, char *argv[]) {

//////////////////////
// CHECK INPUT LINE //
//////////////////////

    if (argc<8) {
        cerr << "Incorrect usage!" << endl
             << "Correct usage:" << endl
             << "run_analysis.exe <ROOT output file name> <simulation file> "
             << "<number of tracks> <L0 angular resolution> <time resolution> "
             << "<algorithm file> <parameter file>"
             << endl;

        return 1;
    }

///////////////
// VARIABLES //
///////////////

    string ROOT_outfile_name(argv[1]);
    string simulationfile(argv[2]);
    unsigned int nb_tracks(atoi(argv[3]));
    double ang_res(atof(argv[4]));
    double t_resolution(atof(argv[5]));
    string algorithmfile(argv[6]);
    string parameterfile(argv[7]);



    MDTTriggerAnalysis trigger_analysis(ROOT_outfile_name, \
                                        simulationfile,
                                        algorithmfile,
                                        parameterfile);
    if (argc==9){
      string simulation_output(argv[8]);
      cout << "Simulation results are written to " << simulation_output << endl;
      trigger_analysis.store_simulation(simulation_output);
    }

//////////////////////////
// PERFORM THE ANALYSIS //
//////////////////////////

    cout << "Running MDT L1 trigger analysis with the following settings"
         << endl
         << " Number of tracks: " << nb_tracks << endl
         << " L0 angular resolution: " << ang_res << endl
         << " Time resolution: " << t_resolution << endl
         << " Simulation file: " << simulationfile << endl
         << " Algorithmfile : " << algorithmfile << endl
         << " Parameter file: " << parameterfile << endl;

    trigger_analysis.analyse(nb_tracks, \
                             ang_res, \
                             t_resolution);
    return 0;

}
